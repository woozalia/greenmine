<?php namespace ferreteria\classloader;
/*
PURPOSE: defines locations for libraries using modloader.php
*/
/*
$fp = dirname( __FILE__ );

fcCodeModule::BasePath($fp.'/');
fcCodeLibrary::BasePath($fp.'/');
*/
cLoader::SetBasePath(__DIR__);
$oLib = $this;

$om = $oLib->MakeModule('app.php');
  $om->AddClass('greenmine\\cApp');
$om = $oLib->MakeModule('global.php');
  $om->AddClass('greenmine\\cGlobalsBase');
$om = $oLib->MakeModule('page.php');
  $om->AddClass('gcGreenminePage');

<?php
/*
  PURPOSE: classes for handling client charges and payments, so we can generate statements
  HISTORY:
    2016-10-18 started
    2017-02-08 revamping for Greenmine
*/

class wftcCharges extends ferreteria\data\cStandardTableSingleKey implements fiLinkableTable, fiEventAware {
    use ftLinkableTable;
    use \ferreteria\field\tFieldCollective_forTable;

    // ++ SETUP ++ //

    protected function SingleRowClass() : string { return 'wfrcCharge'; }
    protected function GetTableName() : string { return 'wf_charge'; }
    public function GetActionKey() : string { return KS_ACTION_WORKFERRET_CHARGE; }
    
    protected function FieldCollectiveClass() : string { return 'wfcCharge_fields'; }
    protected function StorageCollectiveClass() : string { return 'ferreteria\field\cStorageFieldCollective'; }
    protected function DisplayCollectiveClass() : string { return 'wfcCharge_display'; }

    // -- SETUP -- //
    // ++ EVENTS ++ //
  
    public function DoEvent($nEvent) {}	// no action needed
    public function Render() : string { return $this->AdminPage(); }

    // -- EVENTS -- //
    // ++ RECORDS ++ //

    // PUBLIC so Invoice object can use it
    public function Record_forInvoice($idInvc) {
        $rc = $this->SelectRecords('ID_Invc='.$idInvc);
        $rc->NextRow();
        return $rc;
    }

    // -- RECORDS -- //
    // ++ FIELD CALCULATIONS ++ //
    
    // PUBLIC so Invoice record can access it
    public function HasInvoice($idInvc) {
        $rc = $this->Record_forInvoice($idInvc);
        return ($rc->RowCount() > 0);
    }

    // -- FIELD CALCULATIONS -- //
    // ++ INTERNAL STATES ++ //
    
    private $olViewVoid;
    protected function SetMenuOption_ViewVoid(fcMenuOptionLink $ol) { $this->olViewVoid = $ol; }
    // PUBLIC so recordset can access
    public function GetMenuOption_ViewVoid() { return $this->olViewVoid; }

    private $olDoPay;
    protected function SetMenuOption_DoPay(fcMenuOptionLink $ol) { $this->olDoPay = $ol; }
    // PUBLIC so recordset can access
    public function GetMenuOption_DoPay() { return $this->olDoPay; }
    
    // -- INTERNAL STATES -- //
    // ++ ACTIONS ++ //
    
    public function DoCharge_forInvoice(wfrcInvoice $rcInvc) {
        $idInvc = $rcInvc->GetKeyValue();
        if ($this->HasInvoice($idInvc)) {
            // no action needed
        } else {
            $arChg = array(
            'ID_Proj'	=> $rcInvc->ProjectID(),
            'WhenCreated'	=> 'NOW()',
            'WhenEffect'	=> $rcInvc->WhenSent_asSQL(),
            'Amount'	=> $rcInvc->Amount_toBill(),
            'Code'		=> '"INVC-'.$rcInvc->InvoiceSequence().'"',
            'ID_Invc'	=> $idInvc,
            'Descr'		=> '"Invoice #'.$rcInvc->InvoiceNumber().'"'
              );
            $this->Insert($arChg);
        }
    }

    // -- ACTIONS -- //
    // ++ WEB UI ++ //
    
    protected function AdminPage() {
        $this->CreateRowsMenuItems();
        $rs = $this->SelectRecords();
        fcApp::Me()->GetKioskObject()->SetPagePath($this->SelfURL());
        return $rs->AdminRows();
    }
    public function AdminRows_forProject($idProj) {
        $this->CreateRowsMenuItems();
        $doShowVoid = $this->GetMenuOption_ViewVoid()->GetIsSelected();
        
        $sqlFilt = 'ID_Proj='.$idProj;
        if ($doShowVoid) {
            // leave unaltered
        } else {
            $sqlFilt = "($sqlFilt) AND (WhenVoided IS NULL)";
        }
        $rs = $this->SelectRecords($sqlFilt,'WhenEffect,Sort');
        $rs->SetProjectID($idProj);
        $rs->AdminRows_settings_option('no.rows.html','<div class=content>No billing charges found.</div>');
        return $rs->AdminRows();
    }
    protected function CreateRowsMenuItems() {
        $sSfx = '.'.$this->GetActionKey();
        
        // ($sGroupKey,$sKeyValue=TRUE,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
        $ol = new fcMenuOptionLink('view'.$sSfx,'void','voided',NULL,'view voided charges');
          $this->SetMenuOption_ViewVoid($ol);
        $ol = new fcMenuOptionLink('do'.$sSfx,'pay','payment',NULL,'add a payment');
          $this->SetMenuOption_DoPay($ol);
    }

    // -- WEB UI -- //
}
class wfrcCharge extends ferreteria\data\cRecordDeluxe implements fiLinkableRecord, fiEventAware /*, fiEditableRecord */ {
    #use ftSaveableRecord;
    use ftLinkableRecord;
    use ftExecutableTwig;
    #use ftShowableRecord { AdminField as AdminField_parent; }
    
    // 2020-03-11 this may cause conflict
    protected function AdminRows_settings_columns() { return $this->LineForm()->HeaderArray(); }
    
    const KSF_BTN_ADD_CHARGE = 'btnAddCharge';
  
    // ++ EVENTS ++ //
  
    protected function OnCreateElements() {}
    protected function OnRunCalculations() {
        $rcProj = $this->ProjectRecord();
        $sTitle = $rcProj->GetInvoicePrefix()
          .' chg '
          .$this->GetKeyValue()
          ;
        $htTitle = $rcProj->GetNameString()
          .' charge #'
          .$this->GetKeyValue()
          ;
        
        $oPage = fcApp::Me()->GetPageObject();
        //$oPage->SetPageTitle($sTitle);
        $oPage->SetBrowserTitle($sTitle);
        $oPage->SetContentTitle($htTitle);
    }
    public function Render() { return $this->AdminPage(); }

    // -- EVENTS -- //
    // ++ TRAIT HELPERS ++ //

    public function SelfLink_date() {
        $sDate = $this->WhenEffective();
        if ($this->IsConfirmed()) {
            $htDate = $sDate;
            $sPopup = 'charge is confirmed';
        } elseif ($this->IsVoided()) {
            $htDate = "<s>$sDate</s>";
            $sPopup = 'charge was voided';
        } else {
            $htDate = "($sDate)";
            $sPopup = 'charge has not been confirmed';
        }
        $out = $this->SelfLink($htDate,$sPopup);
        return $out;
    }

    // -- TRAIT HELPERS -- //
    // ++ FIELD VALUES ++ //

    protected function ProjectID() : int { return $this->GetFieldValue('ID_Proj'); }
    // PUBLIC so Project record can access it
    public function SetProjectID(int $id) { $this->SetFieldValue('ID_Proj',$id); }
    protected function InvoiceID() : int { return $this->GetFieldValue('ID_Invc'); }
    protected function WhenEffective() { return $this->GetFieldValue('WhenEffect'); }
    protected function WhenConfirmed() { return $this->GetFieldValue('WhenConfirm'); }
    protected function WhenVoided() { return $this->GetFieldValue('WhenVoided'); }
    protected function Amount() { return $this->GetFieldValue('Amount'); }

    // -- FIELD VALUES -- //
    // ++ FIELD CALCULATIONS ++ //

    // PUBLIC so form object can access it
    public function NzProjectID() {
        if ($this->HasRow()) {
            return $this->ProjectID();
        } else {
            return NULL;
        }
    }
    protected function HasInvoice() { return !is_null($this->InvoiceID()); }
    protected function InvoiceLink() {
        if ($this->HasInvoice()) {
            return $this->InvoiceRecord()->SelfLink_name();
        } else {
            return NULL;
        }
    }
    protected function IsConfirmed() { return !is_null($this->WhenConfirmed()); }
    protected function IsVoided() { return !is_null($this->WhenVoided()); }
    protected function Amount_asHTML() { return fcMoney::Format_withSymbol_asHTML($this->Amount()); }
    protected function WhenEffective_DateOnly() { return fcDate::NzDate($this->WhenEffective()); }

    // -- FIELD CALCULATIONS -- //
    // ++ VALUE CALCULATIONS ++ //

    private $dlrBalance;
    protected function ClearBalance() { $this->dlrBalance = 0; }
    protected function AddToBalance() { $this->dlrBalance += $this->Amount(); }
    protected function GetBalance() { return $this->dlrBalance; }
    protected function GetBalance_asHTML() { return fcMoney::Format_withSymbol_asHTML($this->GetBalance()); }

    // -- VALUE CALCULATIONS -- //
    // ++ TABLES ++ //

    // PUBLIC so form can access it (TODO: eventually move it there, somehow)
    public function ProjectTable($id=NULL) {
        return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_PROJECTS,$id);
    }
    protected function InvoiceTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_INVOICES,$id);
    }

    // -- TABLES -- //
    // ++ RECORDS ++ //

    protected function ProjectRecord() {
        $idProj = $this->ProjectID();
        if (is_null($idProj)) {
            throw new exception('Ferreteria internal error: Trying to retrieve project record when project ID is not set.');
        }
        return $this->ProjectTable($idProj);
    }
    protected function InvoiceRecord() {
        $idInvc = $this->InvoiceID();
        if (is_null($idInvc)) {
            throw new exception('Ferreteria internal error: Trying to retrieve invoice record when invoice ID is not set.');
        }
        return $this->InvoiceTable($idInvc);
    }

    // -- RECORDS -- //
    // ++ DB WRITE ++ //
    
    // CALLBACK
    protected function UpdateArray($ar=NULL) {
        $ar = $this->ChangeArray($ar);
        $ar['WhenEdited'] = 'NOW()';
        return $ar;
    }

    // -- DB WRITE -- //
    // ++ WEB UI ++ //

      //++page++//

    private $frmPage;
    public function PageForm() {
        if (empty($this->frmPage)) {
            $oForm = new wfcCharge_fields($this);
            $this->frmPage = $oForm;
        }
        return $this->frmPage;
    }
    public function AdminPage() {
        $oPathIn = fcApp::Me()->GetKioskObject()->GetInputObject();
        $oFormIn = fcHTTP::Request();

        $doSave = $oFormIn->GetBool('btnSave');
        $sAction = $oPathIn->GetString('do');

        if ($doSave) {
            $this->PageForm()->Save();
            $this->SelfRedirect();
        /* 2017-02-22 old code
            $oForm = $this->PageForm();
            $oForm->ClearValues();
            $out = $oForm->Save();

            $vgOut->AddText($out);
            $this->SelfRedirect();
        */
        }
        switch ($sAction) {
          case 'void':
            $arUpd = array(
              'WhenVoided'	=> 'NOW()',
              'WhenConfirm'	=> 'NULL',
              );
            $this->Update($arUpd);
            $this->SelfRedirect();
            die();
          case 'conf':
            $arUpd = array(
              'WhenVoided'	=> 'NULL',
              'WhenConfirm'	=> 'NOW()',
              );
            $this->Update($arUpd);
            $this->SelfRedirect();
            die();
          case 'edit':
            $doEdit = TRUE;
            break;
          default:
            $doEdit = FALSE;
        }
        $doForm = $doEdit;

        //$id = $this->GetKeyValue();
        //$sTitle = 'Charge #'.$id;
        
        // v3
        //$oApp = fcApp::Me();
        //$oApp->GetPageObject()->SetPageTitle($sTitle);
        $oMenu = fcApp::Me()->GetHeaderMenu();
            // ($sGroupKey,$sKeyValue=TRUE,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
          $oMenu->SetNode(new fcMenuOptionLink('do','edit'));

        // calculate form customizations

        // ++ RENDER THE FORM

        $out = NULL;
        if ($doForm) {
            $out .= "\n<form method=post>";
        }

        $frmEdit = $this->PageForm();
        if ($this->IsNew()) {
            $frmEdit->ClearValues();
        } else {
            $frmEdit->LoadRecord();
        }

        $oTplt = $frmEdit->Template();
        $arCtrls = $frmEdit->RenderControls($doEdit);
          // custom vars
          $arCtrls['ID'] = $this->SelfLink();
          $arCtrls['ID_Proj'] = $this->ProjectRecord()->SelfLink_name();

        if (!$doEdit) {
            if (!$this->IsVoided()) {
            $sPopup = 'VOID this charge';
            $arLink['do'] = 'void';
            $arCtrls['WhenVoided'] = ' ['.$this->SelfLink('void now',$sPopup,$arLink).']';
            }
            if (!$this->IsConfirmed()) {
            $sPopup = 'CONFIRM this charge';
            $arLink['do'] = 'conf';
            $arCtrls['WhenConfirm'] = ' ['.$this->SelfLink('confirm now',$sPopup,$arLink).']';
            }
        }

        $oTplt->SetVariableValues($arCtrls);
        $out .= $oTplt->Render();

        // -- RENDER THE FORM
        // FORM FOOTER

        if ($doForm) {
            $out .= '<input type=submit name=btnSave value="Save">';
            $out .= '</form>';
        }
        return $out;
    }

      //--page--//
      //++line++//

    private $frmLine;
    public function LineForm() {
        if (empty($this->frmLine)) {
            $oForm = new wfcForm_Charge_line($this);
            $this->frmLine = $oForm;
        }
        return $this->frmLine;
    }
    protected function AdminRows_start() {
        $this->ClearBalance();

        //++menu

        // decide where to put the menu based on what kind of page we're on
        $idProj = $this->NzProjectID();
        if (is_null($idProj)) {
            $oMenu = fcApp::Me()->GetHeaderMenu();
        } else {
            $oMenu = new fcHeaderMenu();
            $oHdr = new fcSectionHeader('Charges',$oMenu);
        }

        $t = $this->GetTableWrapper();
        // ($sKeyValue,$sGroupKey=NULL,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
        $oMenu->SetNode($t->GetMenuOption_ViewVoid());
        $oMenu->SetNode($t->GetMenuOption_DoPay());
            
        if (!is_null($idProj)) {
            $out = $oHdr->Render();
        } else {
            $out = NULL;
        }
        $out .= $this->HandleNewChargeForm($idProj);

        //--menu

        $out .= "\n<table class=listing>";
        return $out;
    }
    // ACTION: Render the new charges form (if requested) and handle any user input
    protected function HandleNewChargeForm($idProj) {
        //$oPathIn = fcApp::Me()->GetKioskObject()->GetInputObject();
        $oFormIn = fcHTTP::Request();
        
        // Has the form been requested (via URL)?
        $isReq = $this->GetTableWrapper()->GetMenuOption_DoPay()->GetIsSelected();
        $doShow = $isReq;	// by default, show the form if requested
        // Has the form been submitted (via POST)?
        $isSub = $oFormIn->GetBool(self::KSF_BTN_ADD_CHARGE);
        if ($isSub) {
            $idProj = $oFormIn->GetInt('proj');
            $nAmt = $oFormIn->GetFloat('amount');
            $sCode = $oFormIn->GetString('code');
            $sDescr = $oFormIn->GetString('descr');
            $sNotes = $oFormIn->GetString('notes');
            $odtEffect = $oFormIn->GetDateTime('when-effect');
            //$sdtEffect = $oFormIn->GetDateTime_asString('when-effect','YY "-" MM "-" DD " " HH ":" II ":" SS');
            $sdtEffect = $odtEffect->format('Y/m/d H:i:s');
            $oPage = fcApp::Me()->GetPageObject();
            if (is_null($idProj) || is_null($nAmt) || is_null($sdtEffect) || (is_null($sCode) && is_null($sDescr))) {
            // a required field is NULL, so can't save; re-display the form
            $oPage->AddErrorMessage('A required field is blank.');	// TODO: be more specific
            $doShow = FALSE;
            } else {
            $db = $this->GetConnection();
            $ar = array(
              'ID_Proj'	=> $idProj,
              'Amount'	=> -$nAmt,
              'Code'	=> $db->SanitizeValue($sCode),
              'Descr'	=> $db->SanitizeValue($sDescr),
              'Notes'	=> $db->SanitizeValue($sNotes),
              'WhenEffect'	=> "'$sdtEffect'",
              'WhenCreated'	=> 'NOW()'
              );
            $this->GetTableWrapper()->Insert($ar);
            if ($db->IsOkay()) {
                $oPage->AddSuccessMessage('New charge saved.');
                // remove form submission and request:
                if (is_null($idProj)) {
                $this->SelfRedirect();
                } else {
                $rcProj = $this->ProjectRecord();
                $rcProj->SelfRedirect();
                }
            } else {
                $oPage->AddErrorMessage('Database error when saving: '.$db->ErrorString());
            }
            }
        } else {
            $nAmt = NULL;
            $sCode = NULL;
            $sDescr = NULL;
            $sNotes = NULL;
            $sdtEffect = NULL;
        }
        if ($isReq) {
            if (is_null($idProj)) {
            $htProj = $this->ProjectTable()->DropDown();
            } else {
            $htProj = $this->ProjectRecord()->SelfLink_name()
              ."<input type=hidden name=proj value=$idProj>"
              ;
            }
            $htBtn = self::KSF_BTN_ADD_CHARGE;
            $htCode = fcHTML::FormatString_forTag($sCode);
            $htDescr = fcHTML::FormatString_forTag($sDescr);
            $htNotes = fcHTML::FormatString_forTag($sNotes);
            $out = <<<__END__

<form method=post>
  <table class=form-record>
    <tr><th>Project</th><th>$ Amount</th><th>When</th><th>Code</th><th>Description</th></tr>
    <tr>
      <td>$htProj</td>
      <td>$<input name=amount size=6 value="$nAmt"></td>
      <td><input name=when-effect size=10 value="$sdtEffect"></td>
      <td><input name=code size=10 value="$htCode"></td>
      <td><input name=descr size=30 value="$htDescr"></td>
    </tr>
    <tr>
      <td colspan=4>
	<b>Notes</b>:<input size=50 name=notes value="$htNotes">
	<input type=submit name=$htBtn value="Add Payment">
      </td>
    </tr>
  </table>
</form>
__END__;
        } else {
            $out = NULL;
        }
        return $out;
    }
    // NOTE: This will have to be reworked when we have separate balances for confirmed and unconfirmed
    protected function AdminRows_finish() {
        $out = '<tr><td colspan=4 align=right>Balance:</td><td align=right class=cell-total>'
          .$this->GetBalance_asHTML()
          .'</td></tr>'
          ."\n</table>"
          ;
        return $out;
    }
    // OVERRIDE/REPLACEMENT
    protected function AdminField($sField) : string {
        $sShow = NULL;
        switch($sField) {
          case 'ID':
            $out = '<td align=right>'.$this->SelfLink().'</td>';
            break;
          case 'WhenEffect':
            $out = '<td>'.$this->WhenEffective_DateOnly().'</td>';
            break;
          case 'Amount':
            $this->AddToBalance();
            $out = '<td align=right>'.$this->Amount_asHTML().'</td>';
            break;
          case '!Bal':
            $dlrBal = $this->GetBalance_asHTML();
            $out = "<td align=right>$dlrBal</td>";
            break;
          case 'ID_Proj':
            $out = '<td>'.$this->ProjectRecord()->SelfLink_name().'</td>';
            break;
          case 'ID_Invc':
            $out = '<td>'.$this->InvoiceLink().'</td>';
            break;
          default:
            $out = parent::AdminField_parent($sField);
        }
        return $out;
    }

    // -- WEB UI -- //

}

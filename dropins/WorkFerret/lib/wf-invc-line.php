<?php
/*
  PURPOSE: classes for managing Invoice Line-items in WorkFerret
  HISTORY:
    2013-11-08 split off from WorkFerret.main
    2014-03-26 moved session-to-invoice process here, completely rewritten
      This fixes the bug where rates weren't being set.
*/
class wfctInvcLines extends fcTable_keyed_single_standard implements fiLinkableTable, fiEventAware {
    use ftLinkableTable;
    
    // ++ SETUP ++ //
    
    protected function SingularName() {
	return 'wfcrInvcLine';
    }
    protected function TableName() {
	return 'wf_invc_line';
    }
    public function GetActionKey() {
	return KS_ACTION_WORKFERRET_INVOICE_LINE;
    }
    
    // -- SETUP -- //
    // ++ EVENTS ++ //
  
    public function DoEvent($nEvent) {}	// no action needed
    public function Render() {
	return $this->AdminPage();
    }

    // -- EVENTS -- //
}
class wfcrInvcLine extends fcRecord_keyed_single_integer implements fiLinkableRecord, fiEventAware, fiEditableRecord {
    use ftLinkableRecord;
    use ftSaveableRecord;
    use ftExecutableTwig;

    // ++ LOGGING ++ //

    /*----
      PURPOSE: Event-logging stubbed off until later. TODO: implement it.
      OVERRIDE
    */
    public function CreateEvent(array $arArgs) {
	return NULL;
    }

    // -- LOGGING -- //
    // ++ EVENTS ++ //
  
    protected function OnCreateElements() {}
    protected function OnRunCalculations() {
	$sName = $this->InvoiceRecord()->InvoiceNumber().'.'.$this->GetSeq();
	$htTitle = 'Invoice Line '.$sName;
	$sTitle = 'ivl '.$sName;
	
	$oPage = fcApp::Me()->GetPageObject();
	//$oPage->SetPageTitle($sTitle);
	$oPage->SetBrowserTitle($sTitle);
	$oPage->SetContentTitle($htTitle);
    }
    public function Render() {
	return $this->AdminPage();
    }

    // -- EVENTS -- //
    // ++ FIELD VALUES ++ //

    /*----
      PUBLIC so that Invoice object can set it in a new line
    */
    public function SetInvoiceID($id) {
	$this->SetFieldValue('ID_Invc',$id);
    }
    public function GetInvoiceID() {
	return $this->GetFieldValue('ID_Invc');
    }
    public function SetSeq($n) {
	return $this->SetFieldValue('Seq',$n);
    }
    public function GetSeq() {
	return $this->GetFieldValue('Seq');
    }
    protected function LineDate() {
	return $this->GetFieldValue('LineDate');
    }
    protected function WhenVoid() {
	return $this->GetFieldValue('WhenVoid');
    }
    protected function GetDescription() {
	return $this->GetFieldValue('What');
    }
    protected function GetUnitString() {
	return $this->GetFieldValue('Unit');
    }
    protected function GetRateCost() {
	return $this->GetFieldValue('Rate');
    }
    protected function GetLineCost() {
	return $this->GetFieldValue('CostLine');
    }
    protected function Balance_GetStored() {
	return $this->GetFieldValue('CostBal');
    }
    
    // -- FIELD VALUES -- //
    // ++ FIELD CALCULATIONS ++ //

    // CALLBACK
    public function ListItem_Link() {
	return $this->SelfLink($this->ShortDesc());
    }
    // CALLBACK
    public function ListItem_Text() {
	return $this->ShortDesc();
    }
    // TODO: This should probably be renamed ShortTitle() OSLT
    public function ShortDesc() {
	$rcInvc = $this->InvoiceRecord();
	$sDate = $this->LineDate_forDisplay();
	$out = $rcInvc->InvoiceNumber().'.'.$this->GetSeq().' '.$sDate;
	return $out;
    }
    protected function LineDate_forDisplay() {
	$utDate = strtotime($this->LineDate());
	$sDate = date('Y/n/j',$utDate);
	return $sDate;
    }
    protected function LineDate_forDisplay_noYear() {
	$utDate = strtotime($this->LineDate());
	$sDate = date('n/d',$utDate);
	return $sDate;
    }
    /*----
      PURPOSE: callback for drop-down list
    */
    public function Text_forList() {
	return $this->ShortDesc();
    }
    protected function IsVoid() {
	return !is_null($this->WhenVoid());
    }
    protected function GetQuantity_Tidy() {
	return sprintf('%0.2f',$this->GetFieldValue('Qty'));
    }
    protected function BalanceSaved_inCents() {
	$dlrBalSaved = $this->Balance_GetStored();
	$ctsBalSaved = round($dlrBalSaved*100);
	return $ctsBalSaved;
    }

    // -- FIELD CALCULATIONS -- //
    // ++ MULTIROW CALCULATIONS ++ //
    
    private $ctsBal;
    protected function BalanceReset() {
	$this->ctsBal = 0;
    }
    protected function Balance_AddCurrent() {
	$dlrCostLine = $this->GetLineCost();
	$ctsCostLine = round($dlrCostLine*100);
	if (!$this->IsVoid()) {
	    $this->ctsBal += $ctsCostLine;
	}
    }
    protected function Balance_GetFigured() {
	return (round($this->ctsBal))/100;
    }
    protected function Balance_Matches() {
	return ($this->BalanceSaved_inCents() == $this->ctsBal);
    }
    
    // -- MULTIROW CALCULATIONS -- //
    // ++ SQL ++ //

    /*----
      RETURNS: SQL filter for retrieving sessions assigned to this invoice line
      TODO: this should probably call the Sessions table
    */
    protected function SQL_Filter_Sessions() {
	return 'ID_InvcLine='.$this->GetKeyValue();
    }

    // -- SQL -- //
    // ++ TABLES ++ //

    protected function ProjectTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_PROJECTS,$id);
    }
    protected function SessionTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_SESSIONS,$id);
    }
    protected function InvoiceTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_INVOICES,$id);
    }

    // -- TABLES -- //
    // ++ RECORDS ++ //

    /*----
      RETURNS: recordset of sessions assigned to this invoice line
      HISTORY:
	2011-12-04 written
	2017-02-24 renamed from SessionsRc() to SessionRecords()
	2017-03-11 calling Session table to get recordset instead of calculating locally
    */
    public function SessionRecords() {
	/* 2017-03-11 works, but redundant now
	$tblSess = $this->SessionTable();
	$sql = $this->SQL_Filter_Sessions();
	$rcSess = $tblSess->SelectRecords($sql);
	return $rcSess;
	*/
	return $this->SessionTable()->Records_forinvoiceLine($this->GetKeyValue());
    }
    private $rcInvc;
    public function InvoiceRecord() {
	$doLoad = FALSE;
	$idInvc = $this->GetInvoiceID();
	if (empty($this->rcInvc)) {
	    $doLoad = TRUE;
	} else {
	    if ($this->rcInvc->GetKeyValue() != $idInvc) {
		$doLoad = TRUE;
	    }
	}
	if ($doLoad) {
	    $this->rcInvc = $this->InvoiceTable($idInvc);
	}
	return $this->rcInvc;
    }
    /*----
      RETURNS: recordset of invoices for this project
    */
    protected function InvoiceRecords() {
	return $this->ProjectRecord()->InvoiceRecords();
    }
    protected function ProjectRecord() {
	return $this->InvoiceRecord()->ProjectRecord();
    }

    // -- RECORDS -- //
    // ++ WRITE DATA ++ //

      //++calculations++//
      //--calculations--//
    
    /*----
      ACTION: Creates a new ILine record from $this, which is assumed
	to be a fully-populated ILine recordset object.
    */
    public function Create() {
	//$arIns = $this->Values();	// 2016-03-30 What was this for? Gets completely overwritten...
	$db = $this->GetConnection();
	$arIns = array(
	    'ID_Invc'	=> $this->GetInvoiceID(),
	    'Seq'	=> $this->GetSeq(),
	    'LineDate'	=> $db->SanitizeValue($this->LineDate()),
	    'WhenVoid'	=> 'NULL',
	    'What'	=> $db->SanitizeValue($this->GetDescription()),
	    'Qty'	=> $this->GetFieldValue('Qty'),
	    'Unit'	=> $db->SanitizeValue($this->GetUnitString()),
	    'Rate'	=> $db->SanitizeValue($this->GetRateCost()),
	    'CostLine'	=> $this->GetLineCost(),
	    'CostBal'	=> $this->Balance_GetStored(),
	    //'Notes`      VARCHAR(255) DEFAULT NULL COMMENT "human-entered notes",
	  );
	$id = $this->GetTableWrapper()->Insert($arIns);
	if ($id === FALSE) {
	    return NULL;
	} else {
	    $this->SetKeyValue($id);
	    return $id;
	}
    }

    /*----
      ACTION: voids the current line and returns its sessions to the un-invoiced pool
      HISTORY:
	2011-12-03 started
	2018-06-01 return status (NOTE: does not seem to be TRUE=ok; maybe # of rows affected?)
    */
    public function DoVoid() {
	$tblSess = $this->SessionTable();
	$ar = array(
	  'ID_Invc'	=> 'NULL',
	  'ID_InvcLine'	=> 'NULL',
	  );
	$sql = $this->SQL_Filter_Sessions();
	$tblSess->Update($ar,$sql);

	$ar = array('WhenVoid' => 'NOW()');
	return $this->Update($ar);
    }
    protected function AdminUpdateInvoiceBalance() {
	$dlrBalSaved = $this->Balance_GetStored();
	$out = "\n<span class=success-message><b>Note</b>: Recorded total of <b>$dlrBalSaved</b> matches calculated balance.</span>";
	if ($this->IsInvoiceRequested()) {
	    // automatically update invoice balance
	    $idInvcReq = $this->GetRequestInvoiceID();
	    $rcInvc = $this->InvoiceTable($idInvcReq);
	    $dlrBalInvc = $rcInvc->Amount_toBill();
	    if ($dlrBalInvc == $this->Balance_GetFigured()) {
		$out .= "\n<span class=success-message><b>Note</b>: Invoice total of <b>$dlrBalInvc</b> matches calculated balance.</span>";
	    } else {
		$dlrBalCalc = $this->Balance_GetFigured();
		if ($rcInvc->WasSent()) {
		    $out .= "\n<span class=warning-message><b>Warning</b>: Invoice total of <b>$dlrBalInvc</b> does not match calculated balance of $dlrBalCalc, but invoice has already been sent.</span>";
		} else {
		    $rcInvc->UpdateBalance($dlrBalCalc);
		    $out .= "\n<span class=success-message><b>Note</b>: Invoice total of <b>$dlrBalInvc</b> has been updated to calculated balance of $dlrBalCalc.</span>";
		}
	    }
	}
	return $out;
    }

    // -- WRITE DATA -- //
    // ++ SESSION ACCUMULATION ++ //

    // ++ new, one-session-per-line session conversion

    /*----
      ACTION: Populate the current Invoice Line record with data adapted from the given Session array
    */
    public function CopySession(wfcSession $rcSess) {

	$this->SetFieldValue('CostLine'	,$rcSess->CostLine_stored());
	$this->SetFieldValue('LineDate'	,$rcSess->WhenStart_dbase());
	$this->SetFieldValue('What'	,$rcSess->Description());
	$this->SetFieldValue('Qty'	,$rcSess->TimeFinal_hours_rounded());
	if ($rcSess->IsTimeBased()) {
	    $this->SetFieldValue('Unit'      ,'hours');
	    $this->SetFieldValue('Rate'      ,$rcSess->RateUsed());
	} else {
	    $this->SetFieldValue('Unit',NULL);
	    $this->SetFieldValue('Rate',NULL);
	}
	$this->SetFieldValue('CostBal'	,$rcSess->CostBalance_stored());
    }

    // ++ old, complicated session conversion begins

    private $arSess,$rcSess;
    private $sWhat,$sWhatLast;
    private $idRateLast,$sDateLast;
    private $nSeq;
    private $nQtyMin;	// number of minutes
    private $dlrLine,$dlrBal;
    public function InvoiceStart() {
	$this->nSeq = $this->InvoiceRecord()->LineCount();
	$this->idRateLast = NULL;
	$this->sDateLast = NULL;
	$this->dlrBal = NULL;
//	$this->ClearLine();
    }
    public function ClearLine() {
	$this->arSess = NULL;
	$this->sWhat = NULL;
	$this->sWhatLast = NULL;
	$this->nQtyMin = NULL;
	$this->dlrLine = NULL;
	$this->nSeq++;
    }
    public function AddSessions(array $arSess) {
	$rcSess = $this->SessionTable()->SpawnRecordset();
	foreach ($arSess as $strSort => $rowSess) {
	    $rcSess->SetFieldValues($rowSess);
	    $this->AddSession($rcSess);
	}
	$this->CloseLine();	// close out any remaining sessions
    }
    public function AddSession(wfcSession $rcSess) {
	// if session needs a new line, write the old line and start a new one
	if (!$this->SameLine($rcSess)) {
	    $this->CloseLine();
	    $this->ClearLine();
	}

	$sWhatNow = $rcSess->Value('Descr');
	if ($sWhatNow != $this->sWhatLast) {
	    if (($sWhatNow != '') && ($this->sWhat != '')) {
		$this->sWhat .= ' / ';
	    }
	    $this->sWhat .= $sWhatNow;
	}
	$this->arSess[$rcSess->KeyValue()] = $rcSess->GetFieldValues();
	$this->nQtyMin += $rcSess->GetFieldValue('TimeTotal');
	$this->dlrLine += $rcSess->GetFieldValue('CostLine');
	// TODO: this seems suboptimal; figure out what it's for, and improve it
	$this->rcSess = clone $rcSess;	// save latest session for reference
    }
    /*----
      ACTION: If the line has anything to write, write it and update the sessions
    */
    protected function CloseLine() {
	if (count($this->arSess) > 0) {
	    $this->WriteRecord();
	    $this->UpdateSessions();
	}
    }
    /*----
      TODO:
	* Clarify why this is in the recordset type rather than the table.
	* This needs to handle unit conversion properly -- when there is proper support for units
	  (i.e when it can handle units other than time)
    */
    protected function WriteRecord() {
	$this->dlrBal += $this->dlrLine;
	$nQtyMin = $this->nQtyMin;
	$nQtyHrs = round(100*$nQtyMin/60)/100;
	
	echo "MINS=[$nQtyMin] HRS=[$nQtyHrs]<br>";
	
          /*
	
	$db = $this->GetConnection();
        $arIns = array(
          'ID_Invc'	=> $this->GetInvoiceID(),
          'Seq'		=> $this->nSeq,
          'LineDate'	=> $db->SanitizeValue($this->sDateLast),
          'What'	=> $db->SanitizeValue($this->sWhat),
          'Qty'		=> $nQtyHrs,
          'Unit'	=> 'NULL',	// not really supported yet
          'Rate'	=> $this->rcSess->Value('BillRate'),
          'CostLine'	=> $this->dlrLine,
          'CostBal'	=> $this->dlrBal
          );

        // add the condensed sessions as invoice lines
        $id = $this->GetWrapperTable()->Insert($arIns);
        $this->SetFieldValue('ID',$id); */
    }
    protected function UpdateSessions() {
	$rcSess = clone $this->rcSess;	// just to use as a blank session record
	foreach ($this->arSess as $idSess => $arRec) {
	    $rcSess->SetFieldValues($arRec);
	    $this->UpdateSession($rcSess);
	}
    }
    protected function UpdateSession(wfcSession $rcSess) {
	$arUpd = array(
	    'ID_Invc'		=> $this->GetInvoiceID(),
	    'ID_InvcLine'	=> $this->GetKeyValue()
	    );
	$rcSess->Update($arUpd);
    }
    protected function SameLine(wfcSession $rcSess) {
	  $idRateThis = $rcSess->GetFieldValue('ID_Rate');
	  $sDateThis = $rcSess->WhenStart_text();

	  $isSame =
	    ($idRateThis == $this->idRateLast) &&
	    ($sDateThis == $this->sDateLast);
	  if (!$isSame) {
	      $this->idRateLast = $idRateThis;
	      $this->sDateLast = $sDateThis;
	  }
	  return $isSame;
    }

    // -- SESSION ACCUMULATION -- //
    // ++ WEB UI ELEMENTS ++ //

    public function DropDown(array $iarArgs=NULL) {
	throw new exception('2017-03-10 Is anything still calling this? Will need some updating.');
	if ($this->hasRows()) {
//	    $strName = nz($iarArgs['ctrl.name'],'invcline');
	    if (array_key_exists('ctrl.name',$iarArgs)) {
		$strName = $iarArgs['ctrl.name'];
	    } else {
		throw new exception('Control name must be specified in input array.');
	    }
	    $idSel = nz($iarArgs['cur.id']);
	    $doNone = nz($iarArgs['none.use'],FALSE);		// include 'none' as an option
	    $strNone = nz($iarArgs['none.text'],'NONE');	// text to show for 'none'
	    $sqlNone = nz($iarArgs['none.sql'],'NULL');	// SQL to use for 'none'
	    $out = "\n".'<select name="'.$strName.'">';
	    if ($doNone) {
		// "NONE"
		if (empty($idSel)) {
		    $htSelect = " selected";
		} else {
		    $htSelect = '';
		}
		$out .= "\n  ".'<option'.$htSelect.' value="'.$sqlNone.'">'.$strNone.'</option>';
	    }
	    // actual invc lines
	    while ($this->NextRow()) {
		if ($this->ID == $idSel) {
		    $htSelect = " selected";
		} else {
		    $htSelect = '';
		}
		$out .= "\n  ".'<option'.$htSelect.' value="'.$this->ID.'">'.$this->ShortDesc().'</option>';
	    }
	    $out .= "\n</select>";

	    return $out;
	} else {
	    return 'N/A';
	}
    }
    private $olDoRenumber;
    protected function SetMenuOption_DoRenumber(fcMenuOptionLink $ol) {
	$this->olDoRenumber = $ol;
    }
    protected function GetMenuOption_DoRenumber() {
	return $this->olDoRenumber;
    }
    private $olDoBalance;
    protected function SetMenuOption_DoBalance($ol) {
	$this->olDoBalance = $ol;
    }
    protected function GetMenuOption_DoBalance() {
	return $this->olDoBalance;
    }
    private $olViewSendable;
    protected function SetMenuOption_ViewSendable($ol) {
	$this->olViewSendable = $ol;
    }
    protected function GetMenuOption_ViewSendable() {
	return $this->olViewSendable;
    }
    private $olViewSessions;
    protected function SetMenuOption_ViewSessions($ol) {
	$this->olViewSessions = $ol;
    }
    protected function GetMenuOption_ViewSessions() {
	return $this->olViewSessions;
    }
    
    // TODO 2017-02-16: need to define all the SetMenuOption_* functions
    protected function CreateMenuItems_forRows() {
                  // ($sGroupKey,$sKeyValue=TRUE,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
	$ol = new fcMenuOptionLink('do','ren','renumber',NULL,'re-number the invoice lines');
	  $this->SetMenuOption_DoRenumber($ol);
	$ol = new fcMenuOptionLink('do','bal','balance',NULL,'recalculate the invoice balance');
	  $this->SetMenuOption_DoBalance($ol);
	$ol = new fcMenuOptionLink('vsend',TRUE,'sendable',NULL,'view invoice lines formatted for sending');
	  $this->SetMenuOption_ViewSendable($ol);
	$ol = new fcMenuOptionLink('vsess',TRUE,'sessions',NULL,'view individual sessions for each line');
	  $this->SetMenuOption_ViewSessions($ol);
    }
    protected function RenderHeader_forRows() {
	$oMenu = new fcHeaderMenu();
	$oHdr = new fcSectionHeader('Lines',$oMenu);
	//$urlBase = $oKiosk->GetBasePath();
	$oMenu->SetNode($oGrp = new fcHeaderMenuGroup('do'));
	  // ($sGroupKey,$sKeyValue=TRUE,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
	  $oGrp->SetNode($this->GetMenuOption_DoRenumber());
	  $oGrp->SetNode($this->GetMenuOption_DoBalance());
	$oMenu->SetNode($oGrp = new fcHeaderMenuGroup('view'));
	  $oGrp->SetNode($this->GetMenuOption_ViewSendable());
	  $oGrp->SetNode($this->GetMenuOption_ViewSessions());
	$out = $oHdr->Render();
	return $out;
    }
    private $idInvcReq;
    protected function GetRequestInvoiceID() {
	// 2017-02-17 There should be a better way to do this...
	if (!isset($this->idInvcReq)) {
	    $oKiosk = fcApp::Me()->GetKioskObject();
	    $oPathIn = $oKiosk->GetInputObject();
	    if ($oPathIn->GetBool(KS_PATHARG_NAME_TABLE)) {
		$sTableReq = $oPathIn->GetString(KS_PATHARG_NAME_TABLE);
		if ($sTableReq == $this->InvoiceTable()->GetActionKey()) {
		    $this->idInvcReq = $oPathIn->GetString(KS_PATHARG_NAME_INDEX);
		}
	    }
	}
	return $this->idInvcReq;
    }
    protected function IsInvoiceRequested() {
	return !is_null($this->GetRequestInvoiceID());
    }

    // -- WEB UI ELEMENTS -- //
    // ++ WEB UI PAGES/SECTIONS ++ //

    protected function ReadRecords_asSortableArray() {
	$ar = NULL;
	while ($this->NextRow()) {
	    $sSortKey = $this->LineDate().'.'.$this->GetKeyValue();	// make sure every sort key is unique
	    $ar[$sSortKey] = $this->GetFieldValues();
	}
	if (is_null($ar)) {
	    echo 'SQL: '.$this->sql;
	    throw new exception('There are no lines for this invoice.');	// 2017-09-29 TODO: handle this more gracefully
	}
	return $ar;
    }
    protected function ReadRecords_asSortedArray() {
	$ar = $this->ReadRecords_asSortableArray();
	$ok = ksort($ar);
	if (!$ok) {
	    throw new exception('Ferreteria internal error: could not sort invoice lines.');
	}
	return $ar;
    }
    /*----
      ACTION: Reads the current recordset into an array, sorted, with field keys named
	appropriately for a report template
      PUBLIC so Invoice object can access it
      VERSION: "Year" header is subreport of row
    */
    public function ReadRecords_asReportArray_v2() {	// goes with template v2
	$ar = $this->ReadRecords_asSortedArray();
	
	// copy data to new array with appropriate keys, massaging as needed
	$nLine = 0;
	$nYrLast = NULL;
	foreach ($ar as $sSort => $row) {
	    $this->SetFieldValues($row);
	    if (!$this->IsVoid()) {
		$nLine++;
		
		$ftQty = $this->GetQuantity_Tidy();
		if ($ftQty == 0) {
		    $ftQtyUnit = '';
		} else {
		    $ftUnit = $this->GetUnitString();
		    $ftQtyUnit = $ftQty.' '.$ftUnit;
		}

		$rowOut = array(
		    'line #'	=> $nLine,
		    'line date'	=> $this->LineDate_forDisplay(),
		    'line description'	=> $this->GetDescription(),
		    'line qty'	=> $ftQtyUnit,
		    'line rate'	=> $this->GetRateCost(),
		    'line total'	=> $this->GetLineCost(),
		    'line balance'	=> $this->Balance_GetStored(),
		  );
		$utLine = strtotime($this->LineDate());
		$nYrCurr = date('Y',$utLine);
		if ($nYrLast != $nYrCurr) {
		    $nYrLast = $nYrCurr;
		    // add year header (subrecord)
		    $rowOut['year'] = array(
		      0 => array(
			'year'	=> $nYrCurr
			)
		      );
		}
		$arOut[$sSort] = $rowOut;
	    }
	}
	return $arOut;
    }
    /*----
      ACTION: Reads the current recordset into an array, sorted, with field keys named
	appropriately for a report template
      PUBLIC so Invoice object can access it
      VERSION: rows are subreports of Year (TODO)
      DATA STRUCTURE:
	[ID] = record array
	[ID][field name] = [field value]
	[ID][report name] = subreport array
	[ID][report name][ID] = record array
	[ID][report name][ID][field name] = [field value]
	etc.
    */
    public function ReadRecords_asReportArray() {
	$ar = $this->ReadRecords_asSortedArray();
	
	// copy data to new array with appropriate keys, massaging as needed
	$nLine = 0;
	$nYrLast = NULL;
	foreach ($ar as $sSort => $row) {
	    $this->SetFieldValues($row);
	    if (!$this->IsVoid()) {
		$nLine++;

		// year header calculations
		
		$utLine = strtotime($this->LineDate());
		$nYrCurr = date('Y',$utLine);
		if ($nYrLast != $nYrCurr) {
		    $nYrLast = $nYrCurr;
		    // add year header (subrecord)
		    $rowYears[$nYrCurr]['year'] = $nYrCurr;
		}
		
		// row calculations

		$ftQty = $this->GetQuantity_Tidy();
		if ($ftQty == 0) {
		    $ftQtyUnit = '';
		} else {
		    $ftUnit = $this->GetUnitString();
		    $ftQtyUnit = $ftQty.' '.$ftUnit;
		}
		
		$rowLine = array(
		    'line #'	=> $nLine,
		    'line date'	=> $this->LineDate_forDisplay_noYear(),
		    'line description'	=> $this->GetDescription(),
		    'line qty'	=> $ftQtyUnit,
		    'line rate'	=> $this->GetRateCost(),
		    'line total'	=> $this->GetLineCost(),
		    'line balance'	=> $this->Balance_GetStored(),
		  );
		$rowYears[$nYrCurr]['lines'][$sSort] = $rowLine;
	    }
	}
	//die('DATA:'.fcArray::Render($rowYears));
	return $rowYears;
    }
    /*----
      NOTE: 2017-02-17 This automatically updates the invoice balance *if* line balances are already correct
	*and* a re-balance has been requested *and* the invoice has not been sent.
	I can imagine additional circumstances where we might not want to automatically update,
	but we can only design that bridge-crossing when we come to it.
    */
    public function AdminList() {
	if ($this->hasRows()) {
	    $oKiosk = fcApp::Me()->GetKioskObject();
	    $oPathIn = $oKiosk->GetInputObject();
	    $oFormIn = fcHTTP::Request();
	    
	    $this->CreateMenuItems_forRows();	// create menu item objects so we can get their values
	    $doBal = $this->GetMenuOption_DoBalance()->GetIsSelected();
	    $doRen = $this->GetMenuOption_DoRenumber()->GetIsSelected();
	    $doSendable = $this->GetMenuOption_ViewSendable()->GetIsSelected();
	    $doSessions = $this->GetMenuOption_ViewSessions()->GetIsSelected();
	    
	    // STAGE 1: read data into an array
	    // ...and now sort if needed:
	    if ($doBal || $doRen) {
		$arIL = $this->ReadRecords_asSortedArray();
	    } else {
		$arIL = $this->ReadRecords_asSortableArray();
	    }
	    
	    // STAGE 2: display headers

	    // - section header:
	    $out = $this->RenderHeader_forRows();
	    
	    $sData = NULL;
	    // - table header:
	    if ($doSendable) {
		$sData .= 'Sendable format:';
		$sData .= "\n<table class=listing><tr><th>#</th><th>Date</th><th>Description</th><th>Qty & Units</th><th>Rate</th><th>$ Line</th><th>$ Bal</th></tr>";
		$qCols = 7;
	    } else {
		if ($doSessions) {
		    $sData .= 'Showing individual sessions:';
		}
		$sData .= "\n<table class=listing><tr><th>ID</th><th>Seq</th><th>Date</th><th>Qty</th><th>Unit</th><th>Rate</th><th>$ Line</th><th>$ Bal</th><th>Description</th></tr>";
		$qCols = 9;
	    }
	    
	    // STAGE 3: iterate through data (possibly sorted now)
	    $isOdd = TRUE;
	    $yrLast = 0;
	    $ctsBalCalc = 0;
	    $dlrBalSaved = NULL;
	    $nSeqCalc = 0;
	    if ($doSessions) {
		// get a Session recordset for displaying Session data
		$rcSess = $this->SessionTable()->SpawnRecordset();
	    }
	    $this->BalanceReset();
	    foreach ($arIL as $sSort => $rowIL) {
		$this->SetFieldValues($rowIL);	// stuff row data back into $this object
		
		$isVoid = $this->IsVoid();
		$doShow = !($doSendable && $isVoid);

		if ($doShow) {
		    $css = $isOdd?'odd':'even';
		    $isOdd = !$isOdd;
		    
		    if ($doSessions) {
			// 2017-02-17 This can be optimized later if needed.
			$rsSess = $this->SessionRecords();
			if ($rsSess->hasRows()) {
			    //$nSessFound++;
			    while ($rsSess->NextRow()) {
				$idSess = $rsSess->GetKeyValue();
				$idLine = $rsSess->InvoiceLineID();
				
				$rowSess = $rsSess->GetFieldValues();
				
				// collate sessions by invoice-line:
				$arLines[$idLine][$idSess] = $rowSess;
			    }
			}
		    }
		    if ($isVoid) {
			$css .= ' voided';
		    } else {
			$nSeqCalc++;
		    }
		    
		    // check to see if we need a year header:
		    $utLine = strtotime($this->LineDate());
		    $yrCur = date('Y',$utLine);
		    if ($yrLast != $yrCur) {
			// display header with year
			$yrLast = $yrCur;
			$sData .= "\n<tr class='section-header-year'><td colspan=$qCols>$yrCur</td></tr>";
		    }
		    $ftDate = date('m/d',$utLine);

		    $ftID = $this->SelfLink();
		    $nSeqThis = $this->GetSeq();
		    if ($doRen) {
			if ($nSeqThis == $nSeqCalc) {
			    $ftSeq = $nSeqThis.'&radic;';
			} else {
			    $arUpd = array('Seq'=>$nSeqCalc);
			    $this->Update($arUpd);
			    $this->SetSeq($nSeqCalc);
			    $ftSeq = "<b>$nSeqCalc</b>(<s>$nSeqThis</s>)";	// TODO: proper HTML5 markup
			}
		    } else {
			if ($isVoid) {
			    $ftSeq = "<s>$nSeqThis</s>";	// TODO: proper HTML5 markup
			} else {
			    $ftSeq = $nSeqThis;
			}
		    }
		    
		    $ftWhat = htmlspecialchars($this->GetDescription());
		    $ftQty = $this->GetQuantity_Tidy();
		    $ftUnit = $this->GetUnitString();
		    $ftRate = $this->GetRateCost();
		    $ftCost = $this->GetLineCost();
		    
		    // check calculated balance against stored balance
		    
		    $this->Balance_AddCurrent();	// replaces next chunk
		    /*
		    $dlrCostLine = $this->GetLineCost();
		    $ctsCostLine = round($dlrCostLine*100);
		    if (!$isVoid) {
			$ctsBalCalc += $ctsCostLine;
		    }
		    */
		    
		    //$dlrBalCalc = (round($ctsBalCalc))/100;
		    $dlrBalSaved = $this->Balance_GetStored();
		    //$ctsBalSaved = round($dlrBalSaved*100);
//		    if ($ctsBalCalc == $ctsBalSaved) {
		    if ($this->Balance_Matches()) {
			$ftBal = $dlrBalSaved;
		    } else {
			// create string showing the change:
			$ftBalWas = sprintf('%0.2f',$dlrBalSaved);
			$dlrBalCalc = $this->Balance_GetFigured();
			if ($isVoid) {
			    $ftBalCalc = '';
			} else {
			    $ftBalCalc = sprintf('%0.2f',$dlrBalCalc);
			}
			//$ftBalDiff = 'was:'.$ftBalWas.' now:'.$ctsBalCalc.' diff:'.($ctsBalCalc-((int)($dlrBalSaved*100)));
			if ($doBal) {
			  // update the balance
			    $arUpd['CostBal'] = $dlrBalCalc;
			    $this->Update($arUpd);
			    $ftBal = "<small><s>$ftBalWas</s> &rarr; <b>$ftBalCalc</b></small>";	// TODO: proper HTML5
			} else {
			    $ftBal = "<small><font color=red><i>$dlrBalSaved</i></font><br><font color=green><b>$ftBalCalc</b></font></small>";
			}
		    }
		    
		    $sData .= "\n<tr class='$css'>";
		    if ($doSendable) {
			if ($ftQty == 0) {
			    $ftQtyUnit = '';
			} else {
			    $ftQtyUnit = $ftQty.' '.$ftUnit;
			}
			$sData .=
			  "\n  <td>$ftSeq</td><td>$ftDate</td><td>$ftWhat</td><td>$ftQtyUnit</td><td>$ftRate"
			  ."</td><td align=right>$ftCost</td><td align=right>$ftBal</td>";
		    } else {
			$sData .=
			  "\n  <td>$ftID</td><td>$ftSeq</td><td>$ftDate</td><td>$ftQty</td><td>$ftUnit</td><td>$ftRate"
			  ."</td><td align=right>$ftCost</td><td align=right>$ftBal</td><td>$ftWhat</td>";
		    }
		    $sData .= "\n</tr>";

		    if ($doSessions) {
			$idLine = $this->GetKeyValue();

			$qColsSess = $qCols-1;
			if (array_key_exists($idLine,$arLines)) {
			    $arLineSess = $arLines[$idLine];
			    foreach ($arLineSess as $key => $row) {
				$rcSess->SetFieldValues($row);
				$idSess = $rcSess->GetKeyValue();
				$nSeq = $rcSess->GetSequence();
				$htSeq = ($nSeq > 0)?($nSeq.'. '):'';
				$htLine = $htSeq
				  .' ('.$rcSess->SelfLink().') '
				  .$rcSess->TimeTotal_stored().'m/'
				  .$rcSess->CostLine_stored().': '
				  .$rcSess->Description()
				  ;

				$sData .= "\n<tr class='$css'>"
				  ."\n<td bgcolor=blue></td><td colspan=$qColsSess>$htLine</td></tr>";
			    }
			} else {
			    $sData .= "\n<tr class='$css'>"
			      ."\n<td bgcolor=red></td><td colspan=$qColsSLess>no sessions</td></tr>";
			}
		    }
		}
	    }
	    if ($doSendable) {
		$sData .= "\n<tr><td colspan=3 align=right><b>TOTAL</b>: </td><td></td><td></td><td></td><td><b>$ftBal</b></td></tr>";
	    }
	    $sData .= "\n</table>";
	    // TODO: all of these should be proper messages and the page should be redirected if anything changes.
	    //if ($dlrBalSaved != $dlrBalCalc) {
	    if ($this->Balance_Matches()) {
		if ($doBal) {
		    $out .= $this->AdminUpdateInvoiceBalance();
		}
	    } else {
		if ($doBal) {
		    $out .= "\n<span class=success-message><b>Note</b>: Recorded total of <b>$dlrBalSaved</b> has now been updated to <b>$dlrBalCalc</b>.</span>";
		} else {
		    $out .= "\n<span class=warning-message><b>Warning</b>: Recorded total of <b>$dlrBalSaved</b> does not match calculated total of <b>$dlrBalCalc</b>!</span>";
		}
	    }
	    $out .= $sData;
	    return $out;
	} else {
	    return 'No invoice lines found.';
	}
    }
    // 2017-02-17 This was the previous displayable version (some fx() not working).
    public function AdminList_OLD($iDoRenum) {
	if ($this->hasRows()) {
	    $oKiosk = fcApp::Me()->GetKioskObject();
	    $oPathIn = $oKiosk->GetInputObject();
	    $oFormIn = fcHTTP::Request();
	    
	    $doBal = $this->GetMenuOption_DoBalance();
	    $doRen = $this->GetMenuOption_DoRenumber();
	    $doSendable = $this->GetMenuOption_ViewSendable();
	    $doSessions = $this->GetMenuOption_ViewSessions(); 

	    /* 2017-02-17 old code for picking up menu options
	    $doSessionsReq = $oPathIn->GetBool('vsess');
	    $doSendableReq = $oPathIn->GetBool('vsend');

	    $doSessions = $doSessionsReq;
	    $doSendable = $doSendableReq;

	    $strAction = $oPathIn->GetString('do');
	    $doBal = ($strAction == 'bal');
//	    $doRen = ($strAction == 'ren');
	    $doRen = $iDoRenum;
	    */
	    
	    // if we're displaying inside an Invoice, just show current invoice's data
	    // this is a bit of a kluge
	    $strType = $oPathIn->GetString('page');
	    if ($strType == 'invc') {
		$idInvc = $$oPathIn->GetInt('id');
		// get list of sessions for this invoice
		$rcInvc = $this->InvoiceTable($idInvc);
		$rsSess = $rcInvc->SessionRecords();
		if ($rsSess->hasRows()) {
		    while ($rsSess->NextRow()) {
			$idSess = $rsSess->KeyValue();
			$idLine = $rsSess->GetFieldValue('ID_InvcLine');
			$rcSess->SetFieldValues($rsSess->GetFieldValues());
			$arSess[$idSess] = $rcSess;
			$arLines[$idLine][] = $rcSess;
		    }
		} else {
		    // can't list sessions because none are assigned
		    $doSessions = FALSE;
		    $sSessMsg = 'No sessions are assigned to this invoice.';
		}
	    } else {
		// can't list sessions because we don't know the invoice ID
		$doSessions = FALSE;
		$sSessMsg = 'Invoice ID is not known.';
	    }

	    /* 2017-02-16 splitting into functions
	    $oMenu = new fcHeaderMenu();
	    $oHdr = new fcSectionHeader('Lines',$oMenu);
	    //$urlBase = $oKiosk->GetBasePath();
	    $oMenu->SetNode($oGrp = new fcHeaderMenuGroup('do'));
	      // ($sGroupKey,$sKeyValue=TRUE,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
	      $oGrp->SetNode($ol = new fcMenuOptionLink('do','ren','renumber',NULL,'re-number the invoice lines'));
		//$ol->SetBasePath($urlBase);
	      $oGrp->SetNode($ol = new fcMenuOptionLink('do','bal','balance',NULL,'recalculate the invoice balance'));
		//$ol->SetBasePath($urlBase);
	    $oMenu->SetNode($oGrp = new fcHeaderMenuGroup('view'));
	      $oGrp->SetNode($ol = new fcMenuOptionLink('vsend',TRUE,'sendable',NULL,'view invoice lines formatted for sending'));
		//$ol->SetBasePath($urlBase);
	      $oGrp->SetNode($ol = new fcMenuOptionLink('vsess',TRUE,'sessions',NULL,'view individual sessions for each line'));
		//$ol->SetBasePath($urlBase);
	    $out = $oHdr->Render();
	    */
	    // If we ever display invoice-lines in some other context, we could move this:
	    $this->CreateMenuItems_forRows();
	    $out = $this->RenderHeader_forRows();
	    
	    /* 2017-02-12 MW code
	    $objPage = new clsWikiFormatter($vgPage);

	    $objSection = new clsWikiSection_std_page($objPage,'Lines',2);

	    $objLink = $objSection->AddLink(new clsWikiSection_section('do'));
	    $objLink = $objSection->AddLink_local(new clsWikiSectionLink_option(array(),'ren','do','renumber'));
	      $objLink->Popup('re-number the invoice lines');
	    $objLink = $objSection->AddLink_local(new clsWikiSectionLink_option(array(),'bal','do','balance'));
	      $objLink->Popup('recalculate the invoice balance');
	    $objLink = $objSection->AddLink(new clsWikiSection_section('view'));
	    $objLink = $objSection->AddLink_local(new clsWikiSectionLink_keyed(array(),'sendable','vsend'));
	      $objLink->Popup('view invoice lines formatted for sending');
	    $objLink = $objSection->AddLink_local(new clsWikiSectionLink_keyed(array(),'sessions','vsess'));
	      $objLink->Popup('view individual sessions for each line');

	    $out = $objSection->Render();
	    */

	    if ($doSendable) {
		$out .= 'Sendable format:';

		$out .= "\n<table><tr><th>#</th><th>Date</th><th>Description</th><th>Qty & Units</th><th>Rate</th><th>$ Line</th><th>$ Bal</th></tr>";
		$qCols = 7;
	    } else {
		if ($doSessionsReq) {
		    if ($doSessions) {
			$out .= 'Showing individual sessions:';
		    } else {
			$out .= 'Cannot show individual sessions: '.$sSessMsg;
		    }
		}

		$out .= "\n<table><tr><th>ID</th><th>Seq</th><th>Date</th><th>Qty</th><th>Unit</th><th>Rate</th><th>$ Line</th><th>$ Bal</th><th>Description</th></tr>";
		$qCols = 9;
	    }
	    $isOdd = TRUE;
	    $yrLast = 0;
	    $ctsBalCalc = 0;
	    $dlrBalSaved = NULL;
	    $intSeq = 0;
	    while ($this->NextRow()) {
		$isVoid = $this->IsVoid();
		$doHide = ($doSendable && $isVoid);

		if (!$doHide) {
		    $wtStyle = $isOdd?'background:#ffffff;':'background:#eeeeee;';
		    $isOdd = !$isOdd;

		    if ($isVoid) {
			$wtStyle .= ' text-decoration: line-through;';
		    } else {
			$intSeq++;
		    }

		    $utLine = strtotime($this->LineDate());

		    $yrCur = date('Y',$utLine);
		    if ($yrLast != $yrCur) {
			// display header with year
			$yrLast = $yrCur;
			$out .= "\n<tr style=\"background: #444466; color: #ffffff;\"\n><td colspan=$qCols><b>$yrCur</b></td></tr>";
		    }
		    $ftDate = date('m/d',$utLine);

		    $ar = $this->GetFieldValues();

		    $ftID = $this->SelfLink();
		    $txtSeq = $ar['Seq'];
		    if ($doRen) {
			if ($txtSeq == $intSeq) {
			    $ftSeq = $txtSeq.'&radic;';
			} else {
			    $arUpd = array('Seq'=>$intSeq);
			    $this->Update($arUpd);
			    $this->SetSeq($intSeq);
			    $ftSeq = '<i>'.$intSeq.'</i>';
			}
		    } else {
			if ($isVoid) {
			    $ftSeq = '';
			} else {
			    $ftSeq = $txtSeq;
			}
		    }
		    $ftWhat = htmlspecialchars($ar['What']);
		    $ftQty = sprintf('%0.2f',$ar['Qty']);
		    $ftUnit = $ar['Unit'];
		    $ftRate = $ar['Rate'];
		    $ftCost = $ar['CostLine'];
		    $ctsCostLine = round($ar['CostLine']*100);
		    if (!$isVoid) {
			$ctsBalCalc += $ctsCostLine;
		    }
		    $dlrBalCalc = (round($ctsBalCalc))/100;
		    $dlrBalSaved = $ar['CostBal'];
		    $ctsBalSaved = round($dlrBalSaved*100);
		    //$ftBalCalc = sprintf('%0.2f',$dlrBalSaved);
		    if ($ctsBalCalc == $ctsBalSaved) {
			$ftBal = $ar['CostBal'];
		    } else {
			// create string showing the change:
			//$ctsBalSaved = ((int)($dlrBalSaved*100));
			$ftBalWas = sprintf('%0.2f',$dlrBalSaved);
			if ($isVoid) {
			    $ftBalCalc = '';
			} else {
			    $ftBalCalc = sprintf('%0.2f',$dlrBalCalc);
			}
			//$ftBalDiff = 'was:'.$ftBalWas.' now:'.$ctsBalCalc.' diff:'.($ctsBalCalc-((int)($dlrBalSaved*100)));
			if ($doBal) {
			  // update the balance
			    $arUpd['CostBal'] = $dlrBalCalc;
			    $this->Update($arUpd);
			    $ftBal = '<small><s>'.$ftBalWas.'</s> &rarr; <b>'.$ftBalCalc.'</b></small>';
			} else {
			    $ftBal = '<small><font color=red><i>'.$ar['CostBal'].'</i></font><br><font color=green><b>'.$ftBalCalc.'</b></font></small>';
			}
		    }

		    $out .= "\n<tr style=\"$wtStyle\">";
		    if ($doSendable) {
			if ($ftQty == 0) {
			    $ftQtyUnit = '';
			} else {
			    $ftQtyUnit = $ftQty.' '.$ftUnit;
			}
			$out .=
			  "<td>$ftSeq</td><td>$ftDate</td><td>$ftWhat</td><td>$ftQtyUnit</td><td>$ftRate"
			  ."</td><td align=right>$ftCost</td><td align=right>$ftBal</td>";
		    } else {
			$out .=
			  "<td>$ftID</td><td>$ftSeq</td><td>$ftDate</td><td>$ftQty</td><td>$ftUnit</td><td>$ftRate"
			  ."</td><td align=right>$ftCost</td><td align=right>$ftBal</td><td>$ftWhat</td>";
		    }
		    $out .= "\n</tr>";
		    if ($doSessions) {
			$idLine = $this->GetKeyValue();

			if (array_key_exists($idLine,$arLines)) {
			    $arLineSess = $arLines[$idLine];
			    foreach ($arLineSess as $key => $rc) {
				$idSess = $rc->GetKeyValue();
				unset($arSess[$idSess]);
				$intSeq = $rc->GetSeq();
				$htSeq = ($intSeq > 0)?($intSeq.'. '):'';
				$htLine = $htSeq
				  .' ('.$rc->SelfLink().') '
				  .$rc->TimeTotal().'m/'
				  .$rc->CostLine().': '
				  .$rc->Description()
				  ;

				$out .= "\n<tr style=\"$wtStyle\">"
				  ."\n<td bgcolor=blue></td><td colspan=".($qCols-1).">$htLine</td></tr>";
			    }
			} else {
			    $out .= "\n<tr style=\"$wtStyle\">"
			      ."\n<td bgcolor=red></td><td colspan=".($qCols-1).">no sessions</td></tr>";
			}
		    }
		}
	    }
	    if ($doSessions && (count($arSess) > 0)) {
		$out .= "\n<tr style=\"background: #888888; color: #ffffff;\"><td colspan=$qCols><b>non line-specific</b></td>";
		foreach ($arSess as $key => $rc) {
		    $htSeq = ($rc->GetSeq() > 0)?($obj->Seq.'. '):'';
		    $htLine = $htSeq
		      .$rc->TimeTotal().'m/'
		      .$rc->CostLine().': '
		      .$rc->Description()
		      ;
		    $out .= "\n<tr style=\"$wtStyle\"><td bgcolor=blue></td><td colspan=".($qCols-1)."</td><td>$htLine</td></tr>";
		}
	    }
	    if ($doSendable) {
		$out .= "\n<tr><td colspan=3 align=right><b>TOTAL</b>: </td><td></td><td></td><td></td><td><b>$ftBal</b></td></tr>";
	    }
	    $out .= "\n</table>";
	    if ($dlrBalSaved != $dlrBalCalc) {
		if ($doBal) {
		    $out .= "\n<b>Note</b>: Recorded total of <b>$dlrBalSaved</b> has now been updated to <b>$dlrBalCalc</b>.";
		} else {
		    $out .= "\n<b>Warning</b>: Recorded total of <b>$dlrBalSaved</b> does not match calculated total of <b>$dlrBalCalc</b>!";
		}
	    }
	    return $out;
	} else {
	    return 'No lines found in this invoice.';
	}
    }
    /*-----
      TODO: When an invoice line is moved to a different invoice, both the original invoice
	and the target invoice need to be renumbered / recalculated.
    */
    public function AdminPage() {
	$oPathIn = fcApp::Me()->GetKioskObject()->GetInputObject();
	$oFormIn = fcHTTP::Request();
	
	$doSave = $oFormIn->GetBool('btnSave');
	$doEdit = $oPathIn->GetBool('edit');
	$sAction = $oPathIn->GetString('do');

	$doVoid = FALSE;
	switch ($sAction) {
/*	  case 'edit':
	    $doEdit = TRUE;
	    break;
*/
	  case 'void':
	    $doVoid = TRUE;
	    break;
	}

	if ($doVoid) {
	    $this->DoVoid();
	    $this->Reload('Line has been VOIDed');
	}

	if ($doSave) {
	    $this->PageForm()->Save();
	    $this->SelfRedirect();
	}

	$doForm = $doEdit;

	$id = $this->GetKeyValue();
	$oMenu = fcApp::Me()->GetHeaderMenu();
	  // ($sGroupKey,$sKeyValue=TRUE,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
	  $oMenu->SetNode($ol = new fcMenuOptionLink('edit',TRUE,NULL,NULL,'edit this invoice line'));
	    //$ol->SetBasePath($this->SelfURL());

	/* MW code
	$objPage = new clsWikiFormatter($vgPage);

	$objSection = new clsWikiSection_std_page($objPage,$strName,2);
	$objLink = $objSection->AddLink_local(new clsWikiSectionLink_option(array(),'edit',NULL,NULL,'view'));
	  $objLink->Popup('edit this invoice line');
	$out = $objSection->Render();
	*/

	$out = NULL;
	if ($doForm) {
	    $out .= "\n<form method=post>";
	}

	// calculate form add-ons

	if ($this->IsVoid() || $doEdit) {	// don't show VOID link when editing
	    $htVoidNow = NULL;
	} else {
	    $sPopup = 'immediately VOID the invoice line and mark its sessions as not invoiced';
	    $arLink['do'] = 'void';
	    $htVoidNow = ' ['.$this->SelfLink('void now',$sPopup,$arLink).']';
	}

	// render the form

	$frmEdit = $this->PageForm();
	if ($this->IsNew()) {
	    $frmEdit->ClearValues();
	} else {
	    $frmEdit->LoadRecord();
	}
	$oTplt = $this->PageTemplate();
	$arCtrls = $frmEdit->RenderControls($doEdit);
	  // custom vars
	  $arCtrls['ID'] = $this->SelfLink();
	  $arCtrls['!DoVoid'] = $htVoidNow;

	if (!$doEdit) {
	    $rcInvc = $this->InvoiceRecord();
	    $arCtrls['ID_Invc'] = $rcInvc->SelfLink_name();
	}

	$oTplt->SetVariableValues($arCtrls);
	$out .= $oTplt->Render();
	// TODO: working here

/* 2015-05-04 old version
	// get values in various formats
	$utDate = strtotime($this->Value('LineDate'));
	$fvVoid = $this->Value('WhenVoid');
	if (empty($fvVoid)) {
	    $htvVoid = '<i>n/a</i>';
	    $arLink = $vgPage->Args(array('page','id'));
	    $arLink['do'] = 'void';
	    $htVoidNow = ' ['.$vgOut->SelfLink($arLink,'void now', 'VOID the invoice line and mark its sessions as not invoiced (without saving edits)').']';
	} else {
	    $utVoid = strtotime($fvVoid);
	    $htvVoid = date('Y-m-d H:i',$utVoid);
	    $htVoidNow = NULL;
	}

	$strDate = date('Y-m-d',$utDate);
	$fltQty = $this->Value('Qty');
	$fltRate = $this->Value('Rate');
	$fltCost = $this->Value('CostLine');
	if (empty($fltRate)) {
	    $doShowCalc = FALSE;
	} else {
	    $dlrCostCalc = ((int)($fltRate * $fltQty * 100))/100;
	    $doShowCalc = ($dlrCostCalc != $fltCost);
	}

	// get HTML-safe values
	$htvSeq = htmlspecialchars($this->Value('Seq'));
	$htvWhat = htmlspecialchars($this->Value('What'));
	$htvQty = $this->Value('Qty');
	$htvUnit = htmlspecialchars($this->Value('Unit'));
	$htvCostLine = $fltCost;	// LATER: show extra digits, if any, in red or something
	$htvCostBal = $this->Value('CostBal');
	$htvNotes = htmlspecialchars($this->Value('Notes'));

	$idInvc = $this->Value('ID_Invc');

	if ($doEdit) {
	    $objForm = $this->objForm;
	    $ar = array(
	      'default'	=> $idInvc,
	      'name'	=> 'ID_Invc',
	      );
	    $htInvc	= $this->Engine()->Invoices()->DropDown($ar);
	    $htSeq	= $objForm->RenderControl('Seq');
	    $htDate	= $objForm->RenderControl('LineDate');
	    $htVoid	= $objForm->RenderControl('WhenVoid');
	    $htWhat	= $objForm->RenderControl('What');
	    $htQty	= $objForm->RenderControl('Qty');
	    $htUnit	= $objForm->RenderControl('Unit');
	    $htRate	= $objForm->RenderControl('Rate');
	    $htCost	= $objForm->RenderControl('CostLine');
	    $htBal	= $objForm->RenderControl('CostBal');
	    $htNotes	= $objForm->RenderControl('Notes');
	} else {
	    $objInvc = $this->InvoiceRecord();
	    $htInvc = $objInvc->SelfLink($objInvc->Value('InvcNum'));
	    $htSeq = $htvSeq;
	    $htDate = $strDate;
	    $htVoid = $htvVoid;
	    $htWhat = $htvWhat;
	    $htQty = $htvQty;
	    $htUnit = $htvUnit;
	    $htRate = $fltRate;
	    $htCost = '$'.$htvCostLine;
	    $htBal = '$'.$htvCostBal;
	    $htNotes = $htvNotes;
	}

	$htVoid .= $htVoidNow;

	$htID = $this->SelfLink();

//	$rcRate = $this->RateRecord();	// TO BE WRITTEN
//	if (!$rcRate->isLineItem()) {
	    // only show this if total is based on a rate calculation
	    if ($doShowCalc) {
		$htCost .= ' <font color=red><b>!</b></font> should be '.$dlrCostCalc;
	    } else {
		$htCost .= ' <font color=green>ok</font>';
	    }
//	}

	$out .= '<table>';
	$out .= "\n<tr><td align=right><b>ID</b>:</td><td>$htID</td></tr>";
	$out .= "\n<tr><td align=right><b>Invoice</b>:</td><td>$htInvc</td></tr>";
	$out .= "\n<tr><td align=right><b>Sequence</b>:</td><td>$htSeq</td></tr>";
	$out .= "\n<tr><td align=right><b>Date</b>:</td><td>$htDate</td></tr>";
	$out .= "\n<tr><td align=right><b>Void</b>:</td><td>$htVoid</td></tr>";
	$out .= "\n<tr><td align=right><b>What</b>:</td><td>$htWhat</td></tr>";
	$out .= "\n<tr><td align=right><b>Quantity</b>:</td><td>$htQty</td></tr>";
	$out .= "\n<tr><td align=right><b>Unit</b>:</td><td>$htUnit</td></tr>";
	$out .= "\n<tr><td align=right><b>Rate</b>:</td><td>$htRate</td></tr>";
	$out .= "\n<tr><td align=right><b>Cost</b>:</td><td>$htCost</td></tr>";
	$out .= "\n<tr><td align=right><b>Balance</b>:</td><td>$htBal</td></tr>";
	$out .= "\n<tr><td align=right><b>Notes</b>:</td><td>$htNotes</td></tr>";
	$out .= '</table>'; */

	if ($doForm) {
	    $out .= '<input type=submit name=btnSave value="Save">';
	    $out .= '</form>';
	}

	/* 2017-03-11 Works, but untidy
	$out .= '<h3>Sessions</h3>';
	$rcSess = $this->SessionRecords();
	$out .= $this->SessionTable()->CreateMenuItems_forRows();	// this is a kluge
	$out .= $rcSess->AdminList(array());
	*/
	$out .= $this->SessionTable()->AdminRows_forInvoiceLine($this->GetKeyValue());
	return $out;
    }

    // -- WEB UI PAGES/SECTIONS -- //
    // ++ WEB UI PAGE CONSTRUCTION ++ //

    /*----
      HISTORY:
	2010-11-06 adapted from VbzStockBin for VbzAdminTitle
	2011-01-26 adapted from VbzAdminTitle
	2011-12-03 adapted for WorkFerret
    */
    private $frmPage;
    private function PageForm() {
	if (is_null($this->frmPage)) {
	    $oForm = new fcForm_DB($this);

	      $oField = new fcFormField_Num($oForm,'ID_Invc');
		$oCtrl = new fcFormControl_HTML_DropDown($oField,array());
                $oCtrl->SetRecords($this->InvoiceRecords());	// ideally: all open invoices for the same project

	      $oField = new fcFormField_Text($oForm,'Seq');
		$oCtrl = new fcFormControl_HTML($oField,array('size'=>2));

	      $oField = new fcFormField_Time($oForm,'LineDate');
		$oCtrl = new fcFormControl_HTML_Timestamp($oField,array('size'=>10));

	      $oField = new fcFormField_Time($oForm,'WhenVoid');
		$oCtrl = new fcFormControl_HTML_Timestamp($oField,array('size'=>15));	// allow room for time-of-day

	      $oField = new fcFormField_Text($oForm,'What');
		$oCtrl = new fcFormControl_HTML($oField,array('size'=>40));

	      $oField = new fcFormField_Num($oForm,'Qty');
		$oCtrl = new fcFormControl_HTML($oField,array('size'=>2));

	      $oField = new fcFormField_Text($oForm,'Unit');
		$oCtrl = new fcFormControl_HTML($oField,array('size'=>5));

	      $oField = new fcFormField_Num($oForm,'Rate');
		$oCtrl = new fcFormControl_HTML($oField,array('size'=>5));

	      $oField = new fcFormField_Num($oForm,'CostLine');
		$oCtrl = new fcFormControl_HTML($oField,array('size'=>7));

	      $oField = new fcFormField_Num($oForm,'CostBal');
		$oCtrl = new fcFormControl_HTML($oField,array('size'=>7));

	      $oField = new fcFormField_Text($oForm,'Notes');
		$oCtrl = new fcFormControl_HTML_TextArea($oField,array('rows'=>3,'cols'=>50));
/* Forms v1

	global $vgOut;

	if (is_null($this->objForm)) {
	    // create fields & controls
	    $oForm = new fcForm_DB($this->Table()->ActionKey(),$this);


	    $objForm = new clsForm_recs($this,$vgOut);
	    $objForm->AddField(new clsFieldNum	('ID_Invc'),	new clsCtrlHTML());
	    $objForm->AddField(new clsField	('Seq'),	new clsCtrlHTML(array('size'=>2)));
	    $objForm->AddField(new clsFieldTime	('LineDate'),	new clsCtrlHTML(array('size'=>10)));
	    $objForm->AddField(new clsFieldTime	('WhenVoid'),	new clsCtrlHTML(array('size'=>15)));
	    $objForm->AddField(new clsField	('What'),	new clsCtrlHTML(array('size'=>40)));
	    $objForm->AddField(new clsFieldNum	('Qty'),	new clsCtrlHTML(array('size'=>3)));
	    $objForm->AddField(new clsField	('Unit'),	new clsCtrlHTML(array('size'=>5)));
	    $objForm->AddField(new clsFieldNum	('Rate'),	new clsCtrlHTML(array('size'=>5)));
	    $objForm->AddField(new clsFieldNum	('CostLine'),	new clsCtrlHTML(array('size'=>7)));
	    $objForm->AddField(new clsFieldNum	('CostBal'),	new clsCtrlHTML(array('size'=>7)));
	    $objForm->AddField(new clsField	('Notes'),	new clsCtrlHTML_TextArea(array('height'=>3,'width'=>50)));

	    $this->objForm = $objForm; */
	    $this->frmPage = $oForm;
	}
	return $this->frmPage;
    }
    private $tpPage;
    protected function PageTemplate() {
	if (empty($this->tpPage)) {
	    $sTplt = <<<__END__
<table class=form-record>
  <tr><td align=right><b>ID</b>:</td><td>[#ID#]</td></tr>
  <tr><td align=right><b>Invoice</b>:</td><td>[#ID_Invc#]</td></tr>
  <tr><td align=right><b>Sequence</b>:</td><td>[#Seq#]</td></tr>
  <tr><td align=right><b>Date</b>:</td><td>[#LineDate#]</td></tr>
  <tr><td align=right><b>Void</b>:</td><td>[#WhenVoid#] [#!DoVoid#]</td></tr>
  <tr><td align=right><b>What</b>:</td><td>[#What#]</td></tr>
  <tr><td align=right><b>Quantity</b>:</td><td>[#Qty#]</td></tr>
  <tr><td align=right><b>Unit</b>:</td><td>[#Unit#]</td></tr>
  <tr><td align=right><b>Rate</b>:</td><td>[#Rate#]</td></tr>
  <tr><td align=right><b>Cost</b>:</td><td>[#CostLine#]</td></tr>
  <tr><td align=right><b>Balance</b>:</td><td>[#CostBal#]</td></tr>
  <tr><td align=right><b>Notes</b>:</td><td>[#Notes#]</td></tr>
</table>
__END__;
	    $this->tpPage = new fcTemplate_array('[#','#]',$sTplt);
	}
	return $this->tpPage;
    }

    // -- WEB UI PAGE CONSTRUCTION -- //
}

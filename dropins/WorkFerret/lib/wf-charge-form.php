<?php
/*
  PURPOSE: Forms for Change objects
  HISTORY:
    2016-10-19 started
    2020-08-01 revising for Ferreteria v0.4
*/
//class wfcForm_Charge_form extends fcForm_DB {
class wfcCharge_fields extends ferreteria\field\cFieldCollective {

//    public function __construct(wfrcCharge $rs) {
//	parent::__construct($rs);
    protected function MakeFields() {

	  $oField = new ferreteria\field\cNative_Time($this,'WhenCreated');
	  $oField = new ferreteria\field\cNative_Time($this,'WhenEdited');
	  $oField = new ferreteria\field\cNative_Time($this,'WhenEffect');
	    $oCtrl = new ferreteria\field\cDisplay_HTML_Timestamp($oField,array());
	      $oCtrl->Format('Y/m/d');
	  $oField = new ferreteria\field\cNative_Time($this,'WhenConfirm');
	    $oCtrl = new ferreteria\field\cDisplay_HTML_Timestamp($oField,array());
	  $oField = new ferreteria\field\cNative_Time($this,'WhenVoided');
	    $oCtrl = new ferreteria\field\cDisplay_HTML_Timestamp($oField,array());

	  $oField = new ferreteria\field\cNative_Num($this,'ID_Proj');
	    $oCtrl = new ferreteria\field\cDisplay_HTML_DropDown($oField,array());
	      $oCtrl->SetRecords($this->ProjectRecords());
      
	  $oField = new ferreteria\field\cNative_Num($this,'ID_Invc');
	    $oCtrl = new ferreteria\field\cDisplay_HTML_DropDown($oField,array());
	      $oCtrl->SetRecords($this->InvoiceRecords());
	      $oCtrl->NoDataString('(none)');

	  $oField = new ferreteria\field\cNative_Num($this,'Amount');
	    $oCtrl = new ferreteria\field\cDisplay_HTML($oField,array('size'=>5));

	  $oField = new ferreteria\field\cNative_Num($this,'Sort');
	    $oCtrl = new ferreteria\field\cDisplay_HTML($oField,array('size'=>2));

	  $oField = new ferreteria\field\cNative_Text($this,'Code');
	    $oCtrl = new ferreteria\field\cDisplay_HTML($oField,array('size'=>8));

	  $oField = new ferreteria\field\cNative_Text($this,'Descr');
	    $oCtrl = new ferreteria\field\cDisplay_HTML($oField,array('size'=>40));

	  $oField = new ferreteria\field\cNative_Text($this,'Notes');
	    $oCtrl = new ferreteria\field\cDisplay_HTML_TextArea($oField,array('rows'=>3,'cols'=>50));
    }
    protected function ProjectRecords() {
        $rs = $this->GetRecordsObject();
        $idProj = $rs->NzProjectID();
        return $rs->ProjectTable()->Records_forDropDown(array('current'=>$idProj));
    }
    protected function InvoiceRecords() {
        $rs = $this->GetRecordsObject();
        return $rs->ProjectTable()->Records_forDropDown(array());
    }
}
//class wfcForm_Charge_page extends wfcForm_Charge_form {
class wfcCharge_display extends ferreteria\field\cDisplayFieldCollective_POST {

    private $tp;
    public function GetContent_Page() : string {
	    return <<<__END__
<table class='form-record'>
  <tr><td align=right><b>ID</b>:</td><td>[#ID#]</td></tr>
  <tr><td align=right><b>Project</b>:</td><td>[#ID_Proj#]</td></tr>
  <tr><td align=right><b>Date Effective</b>:</td><td>[#WhenEffect#]</td></tr>
  <tr><td align=right><b>When Confirmed</b>:</td><td>[#WhenConfirm#]</td></tr>
  <tr><td align=right><b>When Voided</b>:</td><td>[#WhenVoided#]</td></tr>
  <tr><td align=right><b>Amount</b>: $</td><td>[#Amount#]</td></tr>
  <tr><td align=right title="sorting within date"><b>Sort</b>:</td><td>[#Sort#]</td></tr>
  <tr><td align=right><b>Code</b>:</td><td>[#Code#]</td></tr>

  <tr><td align=right><b>Description</b>:</td><td>[#Descr#]</td></tr>
  <tr><td align=right><b>Notes</b>:</td><td>[#Notes#]</td></tr>
  <tr><td align=right><b>When Created</b>:</td><td>[#WhenCreated#]</td></tr>
  <tr><td align=right><b>When Edited</b>:</td><td>[#WhenEdited#]</td></tr>
</table>
__END__;
	}
    public function GetContent_Line() : string {
        return <<<__END__

  <tr>
    <td>[#ID#]</td>
    <td>[#WhenEffect#]</td>
    <td align=right>[#Sort#]</td>
    <td align=right>$[#Amount#]</td>
    <td>[#ID_Proj#]</td>
    <td>[#ID_Invc#]</td>
    <td>[#Code#]</td>
    <td>[#Descr#]</td>
  </tr>
__END__;
	}
    public function AdminRows_head() : string {
        return <<<__END__
<tr>
  <th>ID</th>
  <th>Date</th>
  <th>Sort</th>
  <th>Amt</th>
  <th>Balance</th>
  <th>Project</th>
  <th>Invoice</th>
  <th>Code</th>
  <th>Description</th>
</tr>
__END__;
    }
}

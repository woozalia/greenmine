<?php
/*
  HISTORY:
    2016-04-12 Split off from wf-sess.php to make navigation easier.
*/
abstract class SessionCalc {
// results - single-line
    public $intTimeTot;
    public $ctsWorkCost,$dlrWorkCost;
    public $ctsWorkTot,$dlrCostTot;
    public $needCalc;
    public $ftStop;
// results - multi-line
    public $intSeq;
    public $ctsBalCalc,$dlrBalCalc;

    public function __construct() {
/*
	if (is_null($iSess)) {
	    $this->objSess = NULL;
	} else {
	    $this->objSess = $iSess;
	    $this->arRow = $iSess->Row;
	}
*/
	$this->ctsBalCalc = NULL;
	$this->dlrBalCalc = NULL;
	$this->intSeq = 0;
    }
    //abstract protected function Data($iName);
    abstract protected function Fetch_WhenStart();
    abstract protected function Fetch_WhenFinish();
    abstract protected function Fetch_TimeAdd();
    abstract protected function Fetch_TimeSub();
    abstract protected function Fetch_BillRate();
    abstract protected function Fetch_CostAdd();
    abstract protected function Fetch_CostLine();
    abstract protected function Fetch_CostBal();
    /*-----
      ACTION: Do calculations using row data passed as array
      USAGE: Call UseRow() first
      RETURNS: public fields
    */
    public function CalcRow() {
    /*
	$strWhenStart = $this->Data('WhenStart');
	$strWhenFinish = $this->Data('WhenFinish');
	$intTimeAdd = $this->Data('TimeAdd');
	$intTimeSub = $this->Data('TimeSub');
	$dlrBillRate = $this->Data('BillRate');
	$dlrCostAdd =  $this->Data('CostAdd');
    */
	$strWhenStart = $this->Fetch_WhenStart();
	$strWhenFinish = $this->Fetch_WhenFinish();
	$intTimeAdd = $this->Fetch_TimeAdd();
	$intTimeSub = $this->Fetch_TimeSub();
	$dlrBillRate = $this->Fetch_BillRate();
	// 2017-02-08 Leftover code indicates that this sometimes may be NULL, which causes an issue below. Fix when it happens.
	$dlrCostAdd =  $this->Fetch_CostAdd();
      // PART 1
	//clsModule::LoadFunc('Time_DefaultDate');
	$ftStart = fcTime::DefaultDate($strWhenStart,$strWhenStart);
	if (empty($strWhenFinish)) {
	    $ftStop = '--';
	    $intMinWorked = 0;
	    $rndMinWorked = 0;
	} else {
	    $ftStop = fcTime::DefaultDate($strWhenFinish,$strWhenStart);
	    // ++ these calculations are now duplicated in TimeSpan_min()
	    $intWhenStart = strtotime($strWhenStart);
	    $intWhenStop = strtotime($strWhenFinish);
	    $fltMinWorked = ($intWhenStop - $intWhenStart) / 60;
	    $rndMinWorked = (int)$fltMinWorked;
	    // --
	    $ftStop .= ' <small>('.$rndMinWorked.'m)</small>';
	}
	$this->ftStop = $ftStop;

      // PART 2
	$intTimeTot = $rndMinWorked + $intTimeAdd - $intTimeSub;
	$ctsWorkCost = round((($intTimeTot * $dlrBillRate)/60.0)*100);
	$ctsCostTot = round($ctsWorkCost + ($dlrCostAdd * 100));
	$dlrCostTot = $ctsCostTot/100;

	$needCalc = ($this->Fetch_CostLine() != $dlrCostTot);
/*
	$arOut['data']['time.tot'] = $intTimeTot;
	$arOut['data']['cost.time'] = $ctsWorkCost/100;
	$arOut['data']['cost.tot'] = $dlrCostTot;
	$arOut['data']['need.calc'] = $needCalc;
*/
      // OUTPUT
	$this->intTimeTot = $intTimeTot;
	$this->ctsWorkCost = $ctsWorkCost;
	$this->dlrWorkCost = $ctsWorkCost/100;
	$this->ctsCostTot = $ctsCostTot;
	$this->dlrCostTot = $dlrCostTot;
	$this->needCalc = $needCalc;
    }
    /*-----
      USAGE: Call CalcRow() first
    */
    public function SumRow() {
	$ctsBalCalc = $this->ctsBalCalc;
	$ctsCostTot = $this->ctsCostTot;
	$ctsBalCalc += $ctsCostTot;
	$dlrBalCalc = $ctsBalCalc/100;
	$this->ctsBalCalc = $ctsBalCalc;
	$this->dlrBalCalc = $dlrBalCalc;

	if ($this->Fetch_CostBal() != $dlrBalCalc) {
	    $this->needCalc = TRUE;
	}

	$this->intSeq++;
    }
}
// TODO: rename to SessionRecordCalc
class SessionRecordCalc extends SessionCalc {

    // ++ SETUP ++ //

    public function __construct(wfcSession $rcSess) {
	$this->Record($rcSess);
	parent::__construct();
    }
    private $rcSess;
    protected function Record($rc=NULL) {
	if (!is_null($rc)) {
	    $this->rcSess = $rc;
	}
	return $this->rcSess;
    }

    // -- SETUP -- //
    // ++ CEMENTING ++ //

//    protected function Data($sName) {
//	return $this->Record()->Value($sName);
//    }
    protected function Fetch_WhenStart() {
	return $this->Record()->WhenStart_dbase();
    }
    protected function Fetch_WhenFinish() {
	return $this->Record()->WhenFinish_dbase();
    }
    protected function Fetch_TimeAdd() {
	return $this->Record()->TimePlus();
    }
    protected function Fetch_TimeSub() {
	return $this->Record()->TimeMinus();
    }
    protected function Fetch_BillRate() {
	return $this->Record()->RateUsed();	// calculated
    }
    protected function Fetch_CostAdd() {
	return $this->Record()->CostPlus();
    }
    protected function Fetch_CostLine() {
	return $this->Record()->CostLine_stored();
    }
    protected function Fetch_CostBal() {
	return $this->Record()->CostBalance_stored();
    }
    /*-----
      USAGE: First call CalcRow() and, if needed, SumRow()
	Updates balance if calculated balance is not NULL
    */
    public function Update() {
	$arUpd = array(
	  'TimeTotal'	=> $this->intTimeTot,
	  'CostLine'	=> $this->dlrCostTot
	);
	if (!is_null($this->dlrBalCalc)) {
	    $arUpd['CostBal']	= $this->dlrBalCalc;
	    $arUpd['Seq']	= $this->intSeq;
	}
	$this->Record()->Update($arUpd);
    }
}
/* 2016-04-12 Fix implementation when needed. Nothing seems to be using it at this point.
class SessionArrCalc extends SessionCalc {
    private $arRow;

    public function __construct(array $iRow) {
	$this->arRow = $iRow;
	parent::__construct();
    }
    protected function Data($iName) {
	return $this->arRow[$iName];
    }
}
//*/
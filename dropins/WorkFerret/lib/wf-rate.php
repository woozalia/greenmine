<?php
/*
  PURPOSE: classes for managing Rates in WorkFerret
  HISTORY:
    2013-10-31 split off from WorkFerret.main
    2017-02-07 revamping as Ferreteria dropin
*/
class cwftRates extends fcTable_keyed_single_standard implements fiLinkableTable, fiEventAware {
    use ftLinkableTable;
    
    // ++ SETUP ++ //
    
    protected function SingularName() {
	return 'cwfrRate';
    }
    protected function TableName() {
	return 'wf_rate';
    }
    public function GetActionKey() {
	return KS_ACTION_WORKFERRET_RATE;
    }

    // -- SETUP -- //
    // ++ EVENTS ++ //
  
    public function DoEvent($nEvent) {}	// no action needed
    public function Render() {
	return $this->AdminPage();
    }

    // -- EVENTS -- //
    // ++ RECORDS ++ //

    /*----
      RETURNS: clsRate recordset (or descendant)
    */
    public function Rates_forProj($idProj) {
	$sqlThis = $this->TableName_Cooked();
	$sqlXProj = 'wf_rate_proj';
	$sql = "SELECT r.* FROM $sqlXProj AS rp LEFT JOIN $sqlThis AS r ON rp.ID_Rate=r.ID WHERE rp.ID_Proj=$idProj";
	$rs = $this->FetchRecords($sql);
	return $rs;
    }
    
    // -- RECORDS -- //
    // ++ WEB UI ++ //
    
    /*----
      HISTORY:
	2011-04-21 renamed from ListPage() -> AdminPage()
    */
    public function AdminPage() {
	$this->AdminSave();	// apply any user edits
	$rs = $this->SelectRecords();
	return $rs->AdminList();
    }
    // 2017-09-09 It's not clear if this is still the right way to do it...
    public function AdminSave() {
	$oFormIn = fcHTTP::Request();

	if ($oFormIn->GetBool('btnSave')) {
	    $id = $oFormIn->GetInt('ID');
	    $doNew = (empty($id));

	    $isActv = $oFormIn->GetBool('isActive');
	    $isLItm = $oFormIn->GetBool('isLineItem');
	    //$idProj = $wgRequest->GetIntOrNull('ID_Proj');
	    $strName = $oFormIn->GetString('Name');
	    $dlrHourly = $oFormIn->GetString('PerHour');

	    $db = $this->Engine();
	    $arRow = array(
	      'isActive'	=> BoolToBit($isActv),
	      'isLineItem'	=> BoolToBit($isLItm),
	      'Name'	=> $db->SanitizeAndQuote($strName),
	      'PerHour'	=> $db->SanitizeAndQuote($dlrHourly)
	      );
	    if ($doNew) {
		$this->Insert($arRow);
	    } else {
		$this->Update($arRow,'ID='.$id);
	    }
	}
    }

    // -- WEB UI -- //
}
class cwfrRate extends fcRecord_keyed_single_integer implements fiLinkableRecord, fiEventAware {
    use ftLinkableRecord;

    // ++ EVENTS ++ //
  
    public function DoEvent($nEvent) {}	// no action needed
    public function Render() {
	return $this->AdminPage();
    }

    // -- EVENTS -- //
    // ++ FIELD VALUES ++ //

    protected function GetNameString() {
	return $this->GetFieldValue('Name');
    }
    /*----
      PUBLIC so it can be used in Session lists
      TODO: Maybe this should be called GetPerUnitValue(), or else the class should be named something other than "Rate"
    */
    public function GetRateValue() {
	return $this->GetFieldValue('PerHour');
    }
    protected function GetRateString($sBlank='',$sPfx='',$sSfx='') {
	$dlr = $this->GetFieldValue('PerHour');
	if ($dlr == 0) {
	    $out = $sBlank;
	} else {
	    $sDlr = sprintf('%0.2f',$dlr);
	    $out = $sPfx.'$'.$dlr.$sSfx;
	}
	return $out;
    }
    /*----
      PUBLIC so Project can retrieve description of default rate
    */
    public function Descr() {
	$sName = $this->GetNameString();
	//$curPerHour = $this->GetFieldValue('PerHour');
	//$ftAmt = ($curPerHour == 0)?'':('($'.$curPerHour.') ');
	$ftAmt = $this->GetRateString('','(',') ');
	return $ftAmt.$sName;
    }
    protected function GetBool_IsActive() {
	return fcFieldStorage_Bit::FromStorage($this->GetFieldValue('isActive'));
    }
    protected function GetBool_IsLineItem() {
	return fcFieldStorage_Bit::FromStorage($this->GetFieldValue('isLineItem'));
    }

    // -- FIELD VALUES -- //
    // ++ FIELD CALCULATIONS ++ //

    public function SelfLink_name() {
	return $this->SelfLink($this->Text_forList());
    }
    /*----
      DONE AS alias of Descr(), but that could change
      HISTORY:
	2015-10-08 Was public, but now only used internally.
    */
    protected function Text_forList() {
        return $this->Descr();
    }
    /*----
      CALLBACK for dropdown control
    */
    public function ListItem_Text() {
        return $this->Descr();
    }
    public function ListItem_Link() {
	return $this->SelfLink_name();
    }
    /*
    public function isLineItem() {
	$bit = $this->Value('isLineItem');
	//return ($bit == chr(1));
	return BitToBool($bit);
    } */
    protected function GetChars_IsActive() {
	return $this->GetBool_IsActive()?'A':'';
    }
    protected function GetText_IsActive() {
	return $this->GetBool_IsActive()?' checked':'';
    }
    protected function GetChars_IsLineItem() {
	return $this->GetBool_IsLineItem()?'LI':'';
    }
    protected function GetText_IsLineItem() {
	return $this->GetBool_IsLineItem()?' checked':'';
    }
    /*----
      RETURNS: TRUE if the rate is quantity-based.
	If not, session cost will need to be entered manually.
    */
    public function IsQtyBased() {
	return !empty($this->GetRateValue());
    }

    // -- FIELD CALCULATIONS -- //
    // ++ TABLES ++ //

    protected function XProjTable() {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_PROJECTS_X_RATES);
    }
    
    private $tProjs;
    protected function XProjs() {
	if (empty($this->tProjs)) {
	    $this->tProjs = $this->XProjTable();
	    $this->tProjs->Load();
	}
	return $this->tProjs;
    }

    // -- TABLES -- //
    // ++ RECORDS ++ //
/*
    public function Records_forDropDown(array $arArgs=NULL) {
    }
*/
    // -- RECORDS -- //
    // ++ WEB INTERFACE ++ //

    public function DropDown(array $iArArgs=NULL) {
	$strName = fcArray::Nz($iArArgs,'name','rate');
	$strNone = fcArray::Nz($iArArgs,'none','none found');
	$intDeflt = fcArray::Nz($iArArgs,'default',0);

	if ($this->hasRows()) {
	    $out = "\n".'<select name="'.$strName.'">';
	    while ($this->NextRow()) {
		$id = $this->KeyValue();
		if ($id == $intDeflt) {
		    $htSelect = " selected";
		} else {
		    $htSelect = '';
		}
		$out .= "\n".'  <option'.$htSelect.' value="'.$id.'">'.$this->Descr().'</option>';
	    }
	    $out .= "\n</select>\n";
	    return $out;
	} else {
	    return $strNone;
	}
    }
    public function AdminPage() {
	$rs = $this->GetTableWrapper()->SelectRecords();
	return $rs->AdminList();
    }
    /*----
      HISTORY:
	2011-12-28 uncommented edit-saving code
	2017-09-10 Mostly fixed to work with Ferreteria
      TODO: "assign" menu option not fixed yet; need dev environment
    */
    public function AdminList() {
	$oPathIn = fcApp::Me()->GetKioskObject()->GetInputObject();
	$oFormIn = fcHTTP::Request();

	// build/render menu
	$sSection = 'Rates';
	$oMenu = new fcHeaderMenu();
	$oHdr = new fcSectionHeader($sSection,$oMenu);
	
	      // ($sGroupKey,$sKeyValue=TRUE,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
          //$oMenu->SetNode($ol = new fcMenuOptionLink('do','edit'));
          $oMenu->SetNode($ol = new fcMenuOptionLink('do','assign',NULL,NULL,'assign rates to projects'));
	    $doAssg = $ol->GetIsSelected();
          $oMenu->SetNode($ol = new fcMenuOptionLink('id',KS_NEW_REC,NULL,NULL,'create a new rate'));
          
	$out = $oHdr->Render();
	
//	$strDo = $oPathIn->GetString('do');
	$idToEdit = $oPathIn->GetInt('id');
	$doEdit = !is_null($idToEdit);
//	$doNew = ($strDo == 'add') || ($idToEdit == 'new');
	$doNew = ($idToEdit == KS_NEW_REC);
	//$doAssg = $oPathIn->GetBool('assign');
	$doForm = $doNew || $doAssg || $doEdit;
	
	$doSave = $oFormIn->getBool('btnSave');
	$doSaveAssign = $oFormIn->getBool('btnAssign');

	// save variables to pass to DisplayRow()

	if ($doSave) {
	    $intChg = $this->AdminSave();	// save edit
	    $this->GetTableWrapper()->SelfRedirect();		// display non-edit version
	}

	if ($doSaveAssign) {
	    $tXProjs = $this->XProjs();
	    $arX = $oFormIn->getArray('rxp');
	    $tXProjs->Update_fromArray($arX);
	    $this->GetTableWrapper()->SelfRedirect();
	}

	if ($doForm) {
	    $out .= "\n<form method=post>";
	    if ($doAssg) {
		$out .= '<table><tr><td>';
	    }
	}

	if ($this->hasRows() || $doNew) {
	    $out .= "\n<table>".$this->XProjs()->RenderRate_hdr();

	    if ($doNew) {
		$out .= $this->DoEditRow();
	    }

	    // DISPLAY ROWS
	    
	    $isOdd = FALSE;
	    
	    // -- display existing rows
	    while ($this->NextRow()) {
		$id = $this->GetKeyValue();
		$doEditThis = ($idToEdit == $id);
		if ($doEditThis) {
		    $out .= $this->DoEditRow();
		} else {
		    $out .= $this->DisplayRow($doAssg,$isOdd);
		}
		$isOdd = !$isOdd;
	    }

	    /* why do we do this even when we're not adding it?
	    // -- display new row
	    $arNew = array(
	      'ID'		=> 'new',
	      //'ID_Proj'		=> NULL,
	      'Name'		=> NULL,
	      'PerHour'		=> NULL,
	      'isActive'	=> TRUE,
	      'isLineItem'	=> TRUE,
	      );
	    $this->SetFieldValues($arNew);
	    if ($doNew) {
		$out .= $this->DoEditRow();
	    } else {
		$out .= $this->DisplayRow(FALSE,NULL,$isOdd);
	    }*/
	    
	    // /DISPLAY ROWS

	    $out .= "\n</table>";
	} else {
	    $out = 'No rates defined.';
	}
	if ($doForm) {
	    if ($doAssg) {
		$out .= '<div align=right><input type=submit name=btnAssign value="Save Assignments"></div></td></tr></table>';
	    }
	    $out .= "\n</form>";
	}
	return $out;
    }
    public function DisplayRow($doAssign,$isOdd) {

	$id = $this->GetKeyValue();
	$css = $isOdd?'odd':'even';
	$htTR = "\n<tr class=\"$css\">";

	//$objProj = $this->ProjectObject();

	$ftID = $this->SelfLink();
//	$ftActv = BitToBool($this->GetFieldValue('isActive'))?'A':'';
//	$ftLItm = BitToBool($this->GetFieldValue('isLineItem'))?'LI':'';
	$ftActv = $this->GetChars_IsActive();
	$ftLItm = $this->GetChars_IsLineItem();
/*
	if ($objProj->IsNew()) {
	    $ftProj = 'ALL';
	} else {
	    $ftProj = $objProj->SelfLink($objProj->Value('Name'));
	}
*/
	$ftName = $this->GetNameString();
	//$ftPerHr = empty($this->Row['PerHour'])?'-':sprintf('%0.2f',$this->GetFieldValue('PerHour'));
	$ftPerHr = $this->GetRateString('-');

	//$id = $this->GetFieldValue('ID');
	$ftProjs = $this->XProjs()->RenderRate($id,$doAssign);

	$out = NULL;
//	if ($id == $idEdit) {
//	    $out .= $this->DoEditRow();
//	} else {
	    $out .= $htTR;
	    $out .= "\n<td>$ftID</td>"
	      ."<td align=center>$ftActv</td>"
	      ."<td align=center>$ftLItm</td>"
	      //."<td>$ftProj</td>"
	      ."<td>$ftName</td>"
	      ."<td align=right>$ftPerHr</td>"
	      .'<td></td>'	// empty cell under "Projects"
	      .$ftProjs
	      .'</tr>';
//	}
	return $out;
    }
    public function DoEditRow() {
	//$idRate = nz($iarArgs['rate']);	// currently selected

//	if(empty($this->Row['ID'])) {
	if ($this->IsNew()) {
	    $htID = '<i>new</i>';
	    $htActv = ' checked';
	    $htLItm = '';
	    //$arProj = NULL;
	    $htName = '';
	    $htSort = NULL;
	    $htPerHour = NULL;
	} else {
	    $id = $this->GetKeyValue();
	    $htID = '<b>'.$id.'</b><input type=hidden name=ID value='.$id.'>';
	    $htActv = $this->GetText_IsActive();
	    $htLItm = $this->GetText_IsLineItem();

	    //$arProj['default'] = $this->Value('ID_Proj');

	    $htName = htmlspecialchars($this->GetNameString());
	    //$htPerHour = sprintf('%0.02f',$this->PerUnit());
	    $htPerHour = $this->GetRateString();
	}
	//$arProj['incl none'] = TRUE;
	//$htProj = $this->objDB->Projects()->DropDown($arProj);
	$out = "\n<tr>"
	  .'<td>'.$htID.'</td>'
	  .'<td align=center>A<br><input type=checkbox name=isActive'.$htActv.'></td>'
	  .'<td align=center>LI<br><input type=checkbox name=isLineItem'.$htLItm.'></td>'
	//  .'<td>'.$htProj.'</td>'
	  .'<td><input name=Name value="'.$htName.'" size=20></td>'
	  .'<td><input name=PerHour value="'.$htPerHour.'" size=5></td>'
	  .'<td><input type=submit name=btnSave value="Save"></td>'
	  .'</tr>';
	return $out;
    }
    // TODO: don't get stuff directly from $_POST
    public function AdminSave() {
	$id = $_POST['ID'];

	$isActive = (fcArray::Nz($_POST,'isActive',NULL) == 'on');

	$db = $this->GetConnection();
	$arEdit = array(
	  'isActive'	=> ($isActive?'TRUE':'FALSE'),
	  'Name'	=> $db->SanitizeValue($_POST['Name']),
	  'PerHour'	=> $db->SanitizeValue($_POST['PerHour']),
	  );

	$t = $this->GetTableWrapper();
	if (is_numeric($id)) {
	    // save existing record
	    $rc = $t->GetRecord_forKey($id);
	    $rc->Update($arEdit);
	} else {
	    // create new record
	    $t->Insert($arEdit);
	}
    }

    // -- WEB INTERFACE -- //
}

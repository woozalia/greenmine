<?php
/*
  PURPOSE: classes for managing Invoices in WorkFerret
  HISTORY:
    2013-11-08 split off from WorkFerret.main
    2017-02-07 revamping as Ferreteria dropin
    2018-05-18 lots of manual reconciliation between cloud4 (dev) and cloud5 (live)
*/


class wftcInvoices extends fcTable_keyed_single_standard implements fiLinkableTable, fiEventAware {
    use ftLinkableTable;

    // ++ SETUP ++ //

    protected function SingularName() {
	return 'wfrcInvoice';
    }
    protected function TableName() {
	return 'wf_invoice';
    }
    public function GetActionKey() {
	return KS_ACTION_WORKFERRET_INVOICE;
    }

    // -- SETUP -- //
    // ++ EVENTS ++ //
  
    public function DoEvent($nEvent) {}	// no action needed
    public function Render() {
	return $this->AdminPage();
    }

    // -- EVENTS -- //
    // ++ TABLES ++ //

    protected function ProjectTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_PROJECTS,$id);
    }

    // -- TABLES -- //
    // ++ RECORDS ++ //

    /*-----
      RETURNS: Dataset of all unsent invoices. Only unsent invoices can be modified; sent invoices should be voided and replaced
	with a new invoice.
    */
    public function GetUnsent($idProj=NULL) {
	$sqlFilt = '(WhenSent IS NULL) AND (WhenVoid IS NULL)';
	if (!is_null($idProj)) {
	    $sqlFilt .= " AND (ID_Proj=$idProj)";
	}
	return $this->SelectRecords($sqlFilt);
    }

    // -- RECORDS -- //
    // ++ CALCULATIONS ++ //

    /*-----
      RETURNS: The next available sequence number for the given project
      PUBLIC so Project page can show it
      NOTES:
	* Sorting by "InvcSeq+0" instead of just "InvcSeq" because otherwise InvcSeq is treated as a string.
	  * Perhaps the InvcSeq field should be changed to INTEGER. It was VARCHAR because I thought I might
	    use some alpha characters at some point, like "12A". It's not clear if this is even sortable in SQL.
	    If the ability to insert between integers is needed, then maybe it should be a FLOAT and we could
	    do things like "12.1" rather than "12A"
    */
    public function NextInvoiceSeq($idProj) {
	$rs = $this->SelectRecords('ID_Proj='.$idProj,'(InvcSeq+0) DESC');
	if ($rs->HasRows()) {
	    $rs->NextRow();
	    $nSeq = $rs->InvoiceSequence();
	    return $nSeq+1;
	} else {
	    return 1;
	}
    }

    // -- CALCULATIONS -- //
    // ++ ACTIONS ++ //

    /*-----
      ACTION: Creates a new, empty invoice
      INPUT:
	iProj: ID of project to which this invoice will belong
	iArSess: array of sessions to be included on the invoice (as a list of IDs)
	  Sessions may not be sorted correctly in this array, so Create should sort them
	    before generating invoice lines.
      HISTORY:
	2017-03-10 Removing record ID from the invoice number.
	  * Requires anticipating the next ID.
	  * Makes invoice number longer.
	  * We don't need the invoice number to be proof of anything; it's a pointer.
    */
    public function Create($idProj,$dlrTotal, $sNotes) {
	// get invoice sequencing information and generate the invoice number:
	if (empty($idProj)) {
	    throw new exception('Internal Error: Trying to create invoice for unspecified project.');
	}
	$nSeq = $this->NextInvoiceSeq($idProj);
	$rcProj = $this->ProjectTable($idProj);
	$strPfx = $rcProj->GetFieldValue('InvcPfx');
	$strInvcNum = sprintf('%1$s-%2$04u',$strPfx,$nSeq);

	// create the new invoice record
	$db = $this->GetConnection();
	$arIns = array(
	  'ID_Proj'	=> $idProj,
	  'InvcSeq'	=> $nSeq,
	  'InvcNum'	=> $db->SanitizeValue($strInvcNum),
	  'WhenCreated'	=> 'NOW()',
	  'TotalAmt'	=> $dlrTotal,
	  'Notes'	=> $db->SanitizeValue($sNotes)
	  );
	$id = $this->Insert($arIns);
	return $id;
    }

    // -- ACTIONS -- //
    // ++ WEB UI ++ //

    public function AdminPage() {
	$rs = $this->SelectRecords();
	return $rs->AdminList();
    }
    public function AdminRows_forProject($idProj) {
	$sqlFilt = 'ID_Proj='.$idProj;
	$rs = $this->SelectRecords($sqlFilt,'InvcNum DESC');
	return $rs->AdminList();
    }

    // -- WEB UI -- //
}
class wfrcInvoice extends fcRecord_keyed_single_integer implements fiLinkableRecord, fiEventAware, fiEditableRecord {
    use ftLinkableRecord;
    use ftSaveableRecord;
    use ftExecutableTwig;

    private $idCache;           // ID of invoice for which we have cached data
    private $arLineStats;       // cached invoice data

    // ++ SETUP ++ //

    protected function InitVars() {
	parent::InitVars();
	$this->arLineStats = NULL;
	$this->idCache = NULL;
    }

    // -- SETUP -- //
    // ++ EVENTS ++ //
  
    protected function OnCreateElements() {}
    protected function OnRunCalculations() {
	$sTitle = 'invc '.$this->InvoiceNumber();
	$htTitle = 'Invoice '.$this->InvoiceNumber();

	$oPage = fcApp::Me()->GetPageObject();
	//$oPage->SetPageTitle($sTitle);
	$oPage->SetBrowserTitle($sTitle);
	$oPage->SetContentTitle($htTitle);
    }
    public function Render() {
	return $this->AdminPage();
    }

    // -- EVENTS -- //
    // ++ FIELD VALUES ++ //

    // PUBLIC because Charges table-wrapper uses it to create a new Charge record
    public function ProjectID() {
	return $this->GetFieldValue('ID_Proj');
    }
    // PUBLIC because Charges table-wrapper uses it to create a new Charge record
    public function InvoiceNumber() {
	return $this->GetFieldValue('InvcNum');
    }
    // PUBLIC because Charges table-wrapper uses it to create a new Charge record
    public function InvoiceSequence() {
	return $this->GetFieldValue('InvcSeq');
    }
    /*----
      PUBLIC because:
	* Charges table-wrapper uses it to create a new Charge record
	* Invoice Lines check it against the calculated total
    */
    public function Amount_toBill() {
	return $this->GetFieldValue('TotalAmt');
    }
    protected function WhenSent() {
	return $this->GetFieldValue('WhenSent');
    }

    // -- FIELD VALUES -- //
    // ++ FIELD CALCULATIONS ++ //

    // TRAIT HELPER

    public function SelfLink_name() {
	return $this->SelfLink($this->InvoiceNumber());
    }
    // CALLBACK
    public function ListItem_Link() {
	return $this->SelfLink_name();
    }
    // CALLBACK
    public function ListItem_Text() {
	return $this->InvoiceNumber();
    }
    public function IsVoid() {
	$dt = $this->GetFieldValue('WhenVoid');
	return (!is_null($dt));
    }
    // PUBLIC because Invoice Lines need to know when deciding whether to update balance
    public function WasSent() {
	$dt = $this->WhenSent();
	return (!is_null($dt));
    }
    /*----
      TODO: This should be changed to use a simple SQL statement which returns the highest existing Seq.
    */
    protected function NextILineSeq() {
	$nSeqMax = 0;
	$rs = $this->InvoiceLineRecords(FALSE);
	if ($rs->HasRows()) {
	    while ($rs->NextRow()) {
		$nSeqCur = $rs->GetFieldValue('Seq');
		if ($nSeqCur > $nSeqMax) {
		    $nSeqMax = $nSeqCur;
		}
	    }
	}
	return $nSeqMax + 1;
    }
    protected function Amount_toBill_formatted() {
	return sprintf('%0.2f',$this->Amount_toBill());
    }

    // -- FIELD CALCULATIONS -- //
    // ++ VALUE CALCULATIONS ++ //

    protected function IsCharged() {
	return $this->ChargeTable()->HasInvoice($this->GetKeyValue());
    }
    // PUBLIC because Charges table-wrapper uses it to create a new Charge record
    public function WhenSent_asSQL() {
	return '"'.$this->WhenSent().'"';	// shouldn't need sanitizing
    }

    // -- VALUE CALCULATIONS -- //
    // ++ TABLES ++ //

    protected function ProjectTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_PROJECTS);
    }
    protected function SessionTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper('wfcSessions');
    }
    protected function InvoiceLineTable() {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_INVOICE_LINES);
    }
    protected function ChargeTable() {
	return $this->GetConnection()->MakeTableWrapper('wftcCharges');
    }

    // -- TABLES -- //
    // ++ RECORDS ++ //

    protected function ChargeRecord() {
	return $this->ChargeTable()->Record_forInvoice($this->GetKeyValue());
    }
    /*----
      RETURNS: recordset of lines for this invoice
      HISTORY:
	2011-06-18 written
    */
    public function InvoiceLineRecords() {
	$tInvc = $this->InvoiceLineTable();
	return $tInvc->SelectRecords('ID_Invc='.$this->GetKeyValue(),'Seq,LineDate');
    }
    private $rcProj;
    public function ProjectRecord() {
	$doLoad = FALSE;
	$idProj = $this->ProjectID();
	if (empty($this->rcProj)) {
	    $doLoad = TRUE;
	} elseif ($this->rcProj->GetKeyValue() != $idProj) {
	    $doLoad = TRUE;
	}
	if ($doLoad) {
	    $this->rcProj = $this->ProjectTable()->GetRecord_forKey($idProj);
	}
	return $this->rcProj;
    }
    public function SessionRecords() {
	$tSess = $this->SessionTable();
	$rs = $tSess->SelectRecords('ID_Invc='.$this->GetKeyValue());
	return $rs;
    }
    protected function ProjectRecords() {
	return $this->ProjectTable()->Records_forDropDown();
    }

    // -- RECORDS -- //
    // ++ RECORDSET CALCULATIONS ++ //

    /*----
      ACTION: Calculates a few stats about the invoice's lines
	* renumbers those lines, preserving their existing order
	* updates each line's balance
      HISTORY:
	2011-06-19 written
	2011-12-04 fixed update -- was calling table update instead of row update
    */
    protected function MakeLineStats() {
	throw new exception('2018-03-01 If anything is still calling this, it will need updating.');
        $id = $this->Value('ID');
        if ($this->idCache != $id) {
	    $rsLines = $this->Lines(TRUE);	// TRUE = sort by date (not sure if that's best -- prevents Seq override)
	    $intSeq = 0;
	    $dlrBal = 0;
	    while ($rsLines->NextRow()) {
		$intSeq++;
		$dlrLine = $rsLines->GetFieldValue('CostLine');
		$dlrBal += $dlrLine;
		$dlrBalOld = $rsLines->GetFieldValue('CostBal');
		$arUpd = NULL;
		if ($rsLines->GetFieldValue('Seq') != $intSeq) {
			$arUpd['Seq'] = $intSeq;
		}
		if ($dlrBalOld != $dlrBal) {
			$arUpd['CostBal'] = $dlrBal;
		}
		if (is_array($arUpd)) {
			$rsLines->Update($arUpd);
		}
	    }
	    $this->arLineStats['count'] = $intSeq;
	    $this->arLineStats['bal'] = $dlrBal;
	    $this->idCache = $id;
        }
    }
    /*----
        RETURNS: number of lines in this invoice
        ACTIONS: runs MakeLineStats(), which corrects balances and sequence
        HISTORY:
                2011-06-18 written
    */
    public function LineCount() {
        $this->MakeLineStats();
        return $this->arLineStats['count'];
    }
    /*----
        RETURNS: balance of all lines currently assigned to this invoice
        ACTIONS: runs MakeLineStats(), which corrects balances and sequence
        HISTORY:
                2011-06-18 written
    */
    public function LineBalance() {
        $this->MakeLineStats();
        return $this->arLineStats['bal'];
    }

    // -- RECORDSET CALCULATIONS -- //
    // ++ ACTIONS ++ //

    /*----
      ACTION: Remove all sessions from this invoice
	i.e. mark them as un-invoiced; don't delete
	Best way to do this seems to be call {invoice line}->DoVoid(), which also empties the attached session(s).
    */
    protected function DoEmpty() {
	$rs = $this->InvoiceLineRecords();
	$qOk = 0;
	$qRecs = $rs->RowCount();
	while ($rs->NextRow()) {
	    $ok = $rs->DoVoid();
	    if ($ok) {
		$qOk++;
	    } else {
		$ok = FALSE;
	    }
	}
	
	if ($qOk == $qRecs) {
	    $s = $qOk.' invoice line'.fcString::Pluralize($qOk).' voided.';
	    fcApp::Me()->GetPageObject()->AddSuccessMessage($s);
	} else {
	    $qDiff = $qRecs - $qOk;
	    $s = $qRecs." invoice lines present, but $qDiff could not be fully voided.";
	    fcApp::Me()->GetPageObject()->AddErrorMessage($s);
	}
    }
    
    // ACTION: make sure there is a Charge record for this Invoice
    protected function MakeCharge() {
	$this->ChargeTable()->DoCharge_forInvoice($this);
    }
    protected function MarkSent() {
	$arChg = array(
	  'WhenSent' 	=> 'NOW()'
	  );
	$this->Update($arChg);
    }
    // ACTION: updates the recorded invoice balance to $dlrBal.
    public function UpdateBalance($dlrBal) {
	$ar = array(
	  'TotalAmt'	=> $dlrBal,
	  'WhenEdited'	=> 'NOW()'
	  );
	return $this->Update($ar);
    }

    // -- ACTIONS -- //
    // ++ FORM BUILDING ++ //

    /*----
      HISTORY:
	2010-11-06 adapted from VbzStockBin for VbzAdminTitle
	2011-01-26 adapted from VbzAdminTitle
	2011-12-03 adapted for WorkFerret
	2011-12-28 adapting for wfrcInvoice (was clsWFInvoice)
    */
    private $frmPage;
    private function PageForm() {
	if (empty($this->frmPage)) {

	    $oForm = new fcForm_DB($this);
	      $oField = new fcFormField_Num($oForm,'ID_Proj');
		$oCtrl = new fcFormControl_HTML_DropDown($oField,array());
                $oCtrl->SetRecords($this->ProjectRecords());

	      $oField = new fcFormField_Text($oForm,'InvcSeq');
		$oCtrl = new fcFormControl_HTML($oField,array('size'=>3));

	      $oField = new fcFormField_Text($oForm,'InvcNum');
		$oCtrl = new fcFormControl_HTML($oField,array('size'=>20));

	      $oField = new fcFormField_Num($oForm,'TotalAmt');
		$oCtrl = new fcFormControl_HTML($oField,array('size'=>5));

	      $oField = new fcFormField_Time($oForm,'WhenCreated');
		$oCtrl = new fcFormControl_HTML_Timestamp($oField,array('size'=>15));
		  $oCtrl->Editable(FALSE);	// make control read-only

	      $oField = new fcFormField_Time($oForm,'WhenEdited');
		$oCtrl = new fcFormControl_HTML_Timestamp($oField,array('size'=>15));
		  $oCtrl->Editable(FALSE);	// make control read-only

	      $oField = new fcFormField_Time($oForm,'WhenSent');
		$oCtrl = new fcFormControl_HTML_Timestamp($oField,array('size'=>15));
		//$oField->Format('n/j G:i');

	      $oField = new fcFormField_Time($oForm,'WhenVoid');
		$oCtrl = new fcFormControl_HTML_Timestamp($oField,array('size'=>15));

	      $oField = new fcFormField_Text($oForm,'Notes');
		$oCtrl = new fcFormControl_HTML_TextArea($oField,array('rows'=>3,'cols'=>50));

	    $this->frmPage = $oForm;
	}
	return $this->frmPage;
    }
    private $tpPage;
    protected function PageTemplate() {
	if (empty($this->tpPage)) {
	    $sTplt = <<<__END__
<table class=form-record>
  <tr><td align=right><b>ID</b>:</td><td>[#ID#]</td></tr>
  <tr><td align=right><b>Project</b>:</td><td>[#ID_Proj#]</td></tr>
  <tr><td align=right><b>Sequence</b>:</td><td>[#InvcSeq#]</td></tr>
  <tr><td align=right><b>Number</b>:</td><td>[#InvcNum#]</td></tr>
  <tr><td align=right><b>Total</b>:</td><td>[#TotalAmt#]</td></tr>
  <tr><td></td><td>
    <table>
      <tr><td><b>Timestamps</b>:</td></tr>
      <tr><td align=right><b>Created</b>:</td><td>[#WhenCreated#]</td></tr>
      <tr><td align=right><b>Edited</b>:</td><td>[#WhenEdited#]</td></tr>
      <tr><td align=right><b>Sent</b>:</td><td>[#WhenSent#]</td></tr>
      <tr><td align=right><b>Charged</b>:</td><td>[#!WhenCharged#]</td></tr>
      <tr><td align=right><b>Voided</b>:</td><td>[#WhenVoid#] [#!DoVoid#]</td></tr>
    </table>
  </td></tr>
  <tr><td align=right><b>Notes</b>:</td><td>[#Notes#]</td></tr>
</table>
__END__;
	    $this->tpPage = new fcTemplate_array('[#','#]',$sTplt);
	}
	return $this->tpPage;
    }

    // -- FORM BUILDING -- //
    // ++ REPORT MANAGEMENT ++ //
    
    /*----
      RETURNS: file path to user file area
    */
    protected function GetUserFilePath() {
	return FP_FILE_REPOSITORY;
    }
    // TODO: make this settable via web, uploadable, etc.
    protected function GetTemplateSpec() {
	return $this->GetUserFilePath().'/'.FN_INVOICE_TEMPLATE;
    }
    
    const kREPORT_MODE_DOC = 'doc';
    const kREPORT_MODE_HTML = 'html';
    const kREPORT_MODE_DEBUG = 'debug';
    const kTEMPLATE_ACT_DEBUG = 'debug';
    const kTEMPLATE_ACT_EDIT = 'edit';
    
    protected function AdminDoReport($eMode) {
	$sInvcNum = $this->InvoiceNumber();
	
	$arMain = array(
	  'project name'	=> $this->ProjectRecord()->GetNameString(),
	  'invoice number'	=> $sInvcNum,
	  'date today'	=> date('Y/m/d'),
	  'final total'		=> $this->Amount_toBill_formatted(),
	  );
	
	// FILESPECS -- hardwired for now:

	$fpBase = $this->GetUserFilePath();
	
	switch ($eMode) {
	  case self::kREPORT_MODE_HTML:
	    // HTML version
	    $fsIn = $fpBase.'/invoice-template.html';
	    $fnOut = "invoice-$sInvcNum.html";
	    $fsOut = $fpBase.$fnOut;
	    $rpMain = new fcReportFile($sInvcNum,$fsIn,$fsOut);
	    break;
	  case self::kREPORT_MODE_DOC:
	  case self::kREPORT_MODE_DEBUG:
	    // ODT version
	    $fsIn = $this->GetTemplateSpec();
	    $fnOut = "invoice-$sInvcNum.odt";
	    $fsOut = $fpBase.'/'.$fnOut;
	    $rpMain = new fcReport_OpenDoc($sInvcNum,$fsIn,$fsOut);
	    break;
	}

	$rs = $this->InvoiceLineRecords();
	$arSub = $rs->ReadRecords_asReportArray();
	
	// TEST
	$arMain['main'] = $arSub;

	$rcMain = new fcDataRow_array();
	$rcMain->SetFieldValues($arMain);
	$rpMain->SetRecords($rcMain);

	$doDebug = ($eMode == self::kREPORT_MODE_DEBUG);
	if ($doDebug) {
	    $rpMain->DoReport();	// generate but don't deliver
	    $sContent = $rpMain->GetOutput_HTML();
	} else {
	    $rpMain->DoReport($fnOut);	// generate and deliver
	    $sContent = "Delivered report <b>$fnOut</b>.";
	}
	$sErr = $rpMain->RenderErrors();
	if (is_null($sErr)) {
	    fcApp::Me()->GetPageObject()->AddSuccessMessage('No errors found.');
	} else {
	    fcApp::Me()->GetPageObject()->AddErrorMessage($sErr);
	}
	fcApp::Me()->AddContentString("<div class=content>$sContent</div>");
    }
    protected function AdminDoTemplate($e) {
	if (!is_null($e)) {
	    $oApp = fcApp::Me();
	    $rpMain = new fcReport_OpenDoc($this->InvoiceNumber(),$this->GetTemplateSpec());
	    $rpMain->LoadTemplateFile();
	    $rpMain->FormatContent($rpMain->GetTemplateText());
	    	    
	    switch ($e) {
	      case self::kTEMPLATE_ACT_DEBUG:
		$sContent = $rpMain->GetOutput_HTML();
		
		$sErr = $rpMain->RenderErrors();
		if (is_null($sErr)) {
		    $oApp->GetPageObject()->AddSuccessMessage('No errors found.');
		} else {
		    $oApp->GetPageObject()->AddErrorMessage($sErr);
		}
		$oApp->AddContentString("<div class=content>$sContent</div>");
		break;
	      case self::kTEMPLATE_ACT_EDIT:
		// check to see if we're saving or just editing
	      
		$oFormIn = fcHTTP::Request();
		$doSave = $oFormIn->getBool('btnSaveTplt');
		
		$doRedirect = FALSE;
		if ($doSave) {
		    $sContent = $oFormIn->GetString('content');
		    $rpMain->SaveTemplateContent($sContent);

		    // report the command results from rezipping the template:
		    // 2017-03-07 This should be hidden by default (CSS? JavaScript?); commenting out for now
		    //$oApp->AddContentString('<span class=content>'.$rpMain->RenderCommandResults().'</span>');
		    // ...and actually, the call below probably repats this text anyway.

		    if ($rpMain->HasErrors()) {
			$oApp->GetPageObject()->AddSuccessMessage($rpMain->RenderErrors());
		    } else {
			$oApp->GetPageObject()->AddSuccessMessage($rpMain->GetSuccessMessage());
			$doRedirect = TRUE;
		    }
		}
		// 2017-03-07 This should be hidden by default (CSS? JavaScript?); commenting out for now
		//$oApp->AddContentString('<span class=content>'.$rpMain->RenderCommandResults().'</span>');
		
		if ($doRedirect) {
		    $this->SelfRedirect();
		}
		
		$sContent = $rpMain->GetOutput_XML();
		$htContent = htmlspecialchars($sContent);
		$out = <<<__END__
<form method=post>
<table><tr><td align=right>
<textarea name=content autofocus cols=80 rows=40>
$htContent
</textarea>
<br><input type=submit name=btnSaveTplt value='Save'>
</td></tr></table>
</form>
__END__;
		$oApp->AddContentString($out);
		break;
	    }
	}
    }

    // -- REPORT MANAGEMENT -- //
    // ++ STATES ++ //

      //++reporting++//
    
    private $eMV_RptType;
    protected function SetMenuValue_ReportType($e) {
	$this->eMV_RptType = $e;
    }
    protected function GetMenuValue_ReportType() {
	return $this->eMV_RptType;
    }

    private $eMV_TpltAct;
    protected function SetMenuValue_TemplateAction($e) {
	$this->eMV_TpltAct = $e;
    }
    protected function GetMenuValue_TemplateAction() {
	return $this->eMV_TpltAct;
    }    
    
      //--reporting--//
      //++other++//
    
    private $bMV_DoEdit;
    protected function SetMenuValue_DoEdit($b) {
	$this->bMV_DoEdit = $b;
    }
    protected function GetMenuValue_DoEdit() {
	return $this->bMV_DoEdit;
    }

    private $bMV_DoEmpty;
    protected function SetMenuValue_DoEmpty($b) {
	$this->bMV_DoEmpty = $b;
    }
    protected function GetMenuValue_DoEmpty() {
	return $this->bMV_DoEmpty;
    }

      //--other--//

    // -- STATES -- //
    // ++ WEB UI ++ //

    /*----
      NOTE: "edit" and "empty" are independent flags, not values of "do:".
    */
    protected function DoAdminPageMenu() {
	$oMenu = fcApp::Me()->GetHeaderMenu();

	  $oMenu->SetNode($oGrp = new fcHeaderMenuGroup('do'));
	  // ($sGroupKey,$sKeyValue=TRUE,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
	    $oGrp->SetNode($ol = new fcMenuOptionLink('edit',TRUE,NULL,NULL,'edit this invoice'));
	      $this->SetMenuValue_DoEdit($ol->GetIsSelected());
	    $oGrp->SetNode($ol = new fcMenuOptionLink('empty',TRUE,NULL,NULL,'remove all lines from this invoice'));
	      $this->SetMenuValue_DoEmpty($ol->GetIsSelected());

	  $oMenu->SetNode($oGrp = new fcHeaderChoiceGroup('rep','report'));
						// $sKeyValue,$sPopup=NULL,$sDispOff=NULL,$sDispOn=NULL
	    $oGrp->SetChoice(new fcHeaderChoice(self::kREPORT_MODE_DOC,'generate an attachable document for this invoice'));
	    $oGrp->SetChoice(new fcHeaderChoice(self::kREPORT_MODE_HTML,'generate an HTML page for this invoice'));
	    $oGrp->SetChoice(new fcHeaderChoice(self::kREPORT_MODE_DEBUG,'display tree of generated invoice content'));
	    
	    $this->SetMenuValue_ReportType($oGrp->GetChoiceValue());

	  $oMenu->SetNode($oGrp = new fcHeaderChoiceGroup('tplt','template'));

	    $oGrp->SetChoice($ol = new fcHeaderChoice(self::kTEMPLATE_ACT_DEBUG,'view tree of template contents'));
	    $oGrp->SetChoice($ol = new fcHeaderChoice(self::kTEMPLATE_ACT_EDIT,'edit template contents'));
	    
	    $this->SetMenuValue_TemplateAction($oGrp->GetChoiceValue());
    }
    public function AdminPage() {
	$this->DoAdminPageMenu();
    
	$oPathIn = fcApp::Me()->GetKioskObject()->GetInputObject();
	$oFormIn = fcHTTP::Request();
	
	$doSave = $oFormIn->getBool('btnSave');
	$sAction = $oPathIn->getString('do');

	if ($doSave) {
	    $this->AdminSave();
	    $this->SelfRedirect();
	} elseif ($this->GetMenuValue_DoEmpty()) {
	    $this->DoEmpty();
	    $this->SelfRedirect();
	    die();
	} else {
	    switch ($sAction) {
	      case 'void':
		$arUpd = array(
		  'WhenVoid'	=> 'NOW()',
		  );
		$this->Update($arUpd);
		$this->SelfRedirect();
		die();
	      case 'send':
		$this->MarkSent();
		$this->SelfRedirect();
		die();
	      case 'charge':
		$this->MakeCharge();
		$this->SelfRedirect();
		die();
	    }
	}

	$id = $this->GetKeyValue();

	$doEdit = $this->GetMenuValue_DoEdit();
	$doForm = $doEdit;
	$sRepType = $this->GetMenuValue_ReportType();
	$doRep = !is_null($sRepType);
	$sTpltAct = $this->GetMenuValue_TemplateAction();
	$doTplt = !is_null($sTpltAct);
	
	$out = NULL;
	
	// if we're generating a report, go ahead and do that so we can redirect back
	if ($doRep) {
	    
	    $this->AdminDoReport($sRepType);
	    if ($sRepType != self::kREPORT_MODE_DEBUG) {
		$this->SelfRedirect();
	    }
	    
	} elseif ($doTplt) {
	
	    $this->AdminDoTemplate($sTpltAct);
	
	} else {
	
	    // calculate form customizations

	    if (!$this->IsVoid() && !$doEdit) {
		$sPopup = 'immediately VOID this invoice';
		$arLink['do'] = 'void';
		$htVoidNow = ' ['.$this->SelfLink('void now',$sPopup,$arLink).']';
	    } else {
		$htVoidNow = NULL;
	    }

	    if ($this->IsCharged()) {
		$htCharge = $this->ChargeRecord()->SelfLink_date();
	    } else {
		if ($doEdit) {
		    $htCharge = 'not charged';
		} else {
		    if ($this->WasSent()) {
			$sPopup = 'create a charge for this invoice';
			$arLink['do'] = 'charge';
			$htCharge = ' ['.$this->SelfLink('charge now',$sPopup,$arLink).']';
		    } else {
			$htCharge = 'not sent';
		    }
		}
	    }

	    // ++ RENDER THE FORM

	    if ($doForm) {
		$out .= "\n<form method=post>";
	    }

	    $frmEdit = $this->PageForm();
	    if ($this->IsNew()) {
		$frmEdit->ClearValues();
	    } else {
		$frmEdit->LoadRecord();
	    }

	    $oTplt = $this->PageTemplate();
	    $arCtrls = $frmEdit->RenderControls($doEdit);
	      // custom vars
	      $arCtrls['ID'] = $this->SelfLink();
	      $arCtrls['!DoVoid'] = $htVoidNow;
	      $arCtrls['!WhenCharged'] = $htCharge;

	    if (!$this->WasSent() && !$doEdit) {
		$sPopup = 'mark this invoice as sent, effective now';
		$arLink['do'] = 'send';
		$arCtrls['WhenSent'] = '['.$this->SelfLink('send now',$sPopup,$arLink).']';
	    }

	    $oTplt->SetVariableValues($arCtrls);
	    $out .= $oTplt->Render();

	    // -- RENDER THE FORM
	    // FORM FOOTER

	    if ($doForm) {
		$out .= '<input type=submit name=btnSave value="Save">';
		$out .= '</form>';
	    }
	    $out .= $this->ListLines();

	}
	return $out;
    }
    /*-----
      ACTION: Save the user's edits to the shipment
      HISTORY:
	2011-02-17 Retired old custom code; now using objForm helper object
	2011-12-03 Adapted from VbzCart to WorkFerret's Invoice Line Record class
	2011-12-27 Copying from Invoice Line Record class to Invoice Record class, replacing hand-built AdminSave().
    */
    private function AdminSave() {
	$oForm = $this->PageForm();
	$oForm->Save();
	$sMsg = $oForm->MessagesString();
	$this->SelfRedirect(array(),$sMsg);	// clear the form data out of the page reload
    }
    // TODO: require $oHdr (fcHeaderMenu) as an argument; not sure how to handle setting title...
    public function AdminList() {
	if ($this->hasRows()) {
	    $oPathIn = fcApp::Me()->GetKioskObject()->GetInputObject();
	    $oFormIn = fcHTTP::Request();

	    //$oMenu = fcApp::Me()->GetHeaderMenu();
	    $oMenu = new fcHeaderMenu();
	    $oHdr = new fcSectionHeader('Invoices',$oMenu);
	    $arContext = array('table'=>$this->GetTableWrapper()->GetActionKey());
	      // ($sGroupKey,$sKeyValue=TRUE,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
	      $oMenu->SetNode($olVV = new fcMenuOptionLink('view','void','voided',NULL,NULL,'view voided invoices'));
		$olVV->AddLinkArray($arContext);

	    $doShowVoid = $olVV->GetIsSelected();

	    $out = $oHdr->Render();

	    $out .= <<<__END__
<table>
  <tr>
    <th>ID</th>
    <th>Prj</th>
    <th>Number</th>
    <th>Total</th>
    <th>Created</th>
    <th>Edited</th>
    <th>Sent</th>
    <th>Voided</th>
    <th>Notes</th>
  </tr>
__END__;
	    $isOdd = TRUE;
	    while ($this->NextRow()) {
		$doShow = TRUE;
		$isVoid = $this->IsVoid();
		if ($isVoid) {
		    $doShow = $doShowVoid;
		}

		if ($doShow) {

		    if ($isVoid) {
			$css = 'voided';
		    } else {
			$css = $isOdd?'odd':'even';
			$isOdd = !$isOdd;
		    }

		    $ftID = $this->SelfLink();
		    $rcProj = $this->ProjectRecord();
		    $ftProj = $rcProj->SelfLink($rcProj->GetFieldValue('InvcPfx'),$rcProj->GetFieldValue('Name'));
		    $ftNum = $this->GetFieldValue('InvcNum');
		    $ftTot = $this->GetFieldValue('TotalAmt');
		    $ftWhenCrea = $this->GetFieldValue('WhenCreated');
		    $ftWhenEdit = $this->GetFieldValue('WhenEdited');
		    $ftWhenSent = $this->WhenSent();
		    $ftWhenVoid = $this->GetFieldValue('WhenVoid');
		    $ftNotes = $this->GetFieldValue('Notes');

		    $out .= <<<__END__
  <tr class="$css">
    <td>$ftID</td>
    <td>$ftProj</td>
    <td>$ftNum</td>
    <td align=right>$ftTot</td>
    <td>$ftWhenCrea</td>
    <td>$ftWhenEdit</td>
    <td>$ftWhenSent</td>
    <td>$ftWhenVoid</td>
    <td>$ftNotes</td>
  </tr>
__END__;
		}
	    }
	    $out .= "\n</table>";
	    return $out;
	} else {
	    return '<div class=content>No invoices found.</div>';
	}
    }
    public function ListLines() {
	$rs = $this->InvoiceLineRecords();
	return $rs->AdminList();
    }
    /*-----
      RETURNS: HTML for drop-down list of invoices in current data set
	Includes "NONE" as a choice. (should this be an option in $arArgs?)
      TODO: Refactor this as a Ferreteria Forms control.
    */
    public function DropDown(array $arArgs=NULL) {
	if (!array_key_exists('ctrl.name',$arArgs)) {
	    throw new exception('Ferreteria Internal Error: must specify name for drop-down control.');
	}
	$strName = $arArgs['ctrl.name'];
	$strNone = fcArray::Nz($arArgs,'none','none found');
	$sqlNone = fcArray::Nz($arArgs,'none.sql','NULL');	// SQL to use for 'none'
	$intDeflt = fcArray::Nz($arArgs,'default',0);
	if ($this->hasRows()) {
	    $out = "\n".'<select name="'.$strName.'">';
	    // "NONE"
	    if ($intDeflt == 0) {
		$htSelect = " selected";
	    } else {
		$htSelect = '';
	    }
	    $out .= "\n".'  <option'.$htSelect.' value="'.$sqlNone.'">NONE</option>';
	    // actual invoices
	    while ($this->NextRow()) {
		$id = $this->GetKeyValue();
		if ($id == $intDeflt) {
		    $htSelect = " selected";
		} else {
		    $htSelect = '';
		}
		$out .= "\n".'  <option'.$htSelect.' value="'.$id.'">'.$this->ListItem_Text()."</option>";
	    }
	    $out .= "\n</select>\n";
	    return $out;
	} else {
	    return $strNone;
	}
    }

    // -- WEB UI -- //
    // ++ ACTIONS ++ //

    /*----
      INPUT:
	$arSess: array of session data, unsorted
      HISTORY:
	2015-05-05 rewriting from scratch
    */
    public function AddLines(array $arSess) {

	// collect sessions in an array so we can sort them

	$tSess = $this->SessionTable();
	foreach ($arSess as $idSess => $val) {
	    $rcSess = $tSess->GetRecord_forKey($idSess);
	    $arSessRecs[$rcSess->SortKey()] = $rcSess->GetFieldValues();
	}

	// do the sort

	ksort($arSessRecs);

	// go through them in order, doing additional calculations, and assign them to the invoice

	$nSeq = $this->NextILineSeq();
	$ctsBal = 0;
	$idInvc = $this->GetKeyValue();
	$rcSess = $this->SessionTable()->SpawnRecordset();
	$rcILine = $this->InvoiceLineTable()->SpawnRecordset();
	foreach ($arSessRecs as $sSort => $arSess) {
	    $rcSess->SetFieldValues($arSess);
	    $ctsLine = $rcSess->GetFiguredCost_cents();
	    $ctsBal += $ctsLine;
	    $arSess['CostLine'] = $ctsLine/100;
	    $arSess['CostBal'] = $ctsBal/100;
	    $arSess['TimeTotal'] = $rcSess->TimeFinal_hours_rounded();
	    $rcSess->SetFieldValues($arSess);			// update object from modified fields
	      $rcILine->CopySession($rcSess);
	      $rcILine->SetInvoiceID($idInvc);
	      $rcILine->SetSeq($nSeq);
	      $rcILine->Create();			// write the ILine data to a new record
	    $rcSess->Assign_toInvoice($rcILine);	// assign the Session record to that ILine
	    $nSeq++;
	}

	// update the invoice

	$arUpd = array(
	  'TotalAmt'	=> $ctsBal/100,
	  'ID_Debit'	=> 'NULL'       // not using this anymore
	  );

	$this->Update($arUpd);
    }
    /*-----
      INPUT:
	$iArSess: array of session data, unsorted
      VERSION: consolidates by date+rate
	This implementation is probably buggy, however.
      HISTORY:
        2011-06-15 significant rewrite to fix transcription bug
	2014-03-26 moved most of this process into the Invoice Line class, to fix the bug for real.
    */
    public function AddLines_OLD(array $iArSess) {

        // copy the sessions into an array for sorting:
	$idxLine = 0;
        $intBillAmt = 0;
	$arSess = NULL;
	foreach ($iArSess as $idSess=>$val) {
	    $rcSess = $this->Engine()->Sessions()->GetItem($idSess);
	    $strSort = $rcSess->SortKey().'.'.$idSess;         // date plus sorting index plus Session ID
	    $arSess[$strSort] = $rcSess->Values();
            $intBillAmt += (int)($rcSess->Value('CostLine') * 100);
	}

	$dlrAddedAmt = $intBillAmt/100;
	$dlrInvcAmtOld = $this->LineBalance();
	$dlrInvcAmtNew = $dlrInvcAmtOld + $dlrAddedAmt;	// add new lines to existing invc total

	// update the invoice
	$arUpd = array(
	  'TotalAmt'	=> $dlrInvcAmtNew,
	  'ID_Debit'	=> 'NULL'       // not using this anymore
	  );
	$this->Update($arUpd);

	if (!is_null($arSess)) {
	    // sort lines by sort key (defined by session object SortKey(): date + seq)
	    ksort($arSess);

        // go through sessions and add invoice lines as appropriate
        // - setup for iteration:

	    // get the number of lines already in the invoice -- numbering starts there
	    // when these change, an invoice line is triggered:
	    $idRateLast = NULL;
	    $strDateLast = NULL;

	    // invoice line setup
	    $rcILine = $this->InvoiceLineTable()->SpawnItem();
	    $rcILine->InvoiceID($this->KeyValue());
	    $rcILine->InvoiceStart();
	    $rcILine->AddSessions($arSess);
        }
    }

    // -- ACTIONS -- //
}

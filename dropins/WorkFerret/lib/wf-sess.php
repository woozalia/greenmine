<?php
/*
  PURPOSE: classes for managing Sessions in WorkFerret
  HISTORY:
    2013-11-08 split off from WorkFerret.main
    2017-02-07,8 revamping as Ferreteria dropin
*/
class wfcSessions extends fcTable_keyed_single_standard implements fiLinkableTable, fiEventAware, fiInsertableTable {
    use ftLinkableTable;

    // ++ SETUP ++ //

    protected function SingularName() {
	return 'wfcSession';
    }
    protected function TableName() {
	return 'wf_session';
    }
    public function GetActionKey() {
	return KS_ACTION_WORKFERRET_SESSION;
    }

    // -- SETUP -- //
    // ++ EVENTS ++ //
  
    public function DoEvent($nEvent) {}	// no action needed
    public function Render() {
	return 'A search function by date might be useful -- "what was I doing, for whom, during this time-span?".';
    }

    // -- EVENTS -- //
    // ++ INTERNAL STATES ++ //
    
    private $olViewInvcd;
    protected function SetMenuOption_ViewInvoiced(fcMenuOptionLink $ol) {
	$this->olViewInvcd = $ol;
    }
    public function GetMenuOption_ViewInvoiced() {
	return $this->olViewInvcd;
    }
    private $olDoNew;
    protected function SetMenuOption_DoNewSession(fcMenuOptionLink $ol) {
	$this->olDoNew = $ol;
    }
    public function GetMenuOption_DoNewSession() {
	return $this->olDoNew;
    }
    private $olDoRecalc;
    protected function SetMenuOption_DoRecalculate(fcMenuOptionLink $ol) {
	$this->olDoRecalc = $ol;
    }
    public function GetMenuOption_DoRecalculate() {
	return $this->olDoRecalc;
    }
    private $olDoInvc;
    protected function SetMenuOption_DoInvoice(fcMenuOptionLink $ol) {
	$this->olDoInvc = $ol;
    }
    public function GetMenuOption_DoInvoice() {
	return $this->olDoInvc;
    }
    
    // -- INTERNAL STATES -- //
    // ++ CALCULATIONS ++ //
    
    /*----
      NOTE: Calculated total may not match stored total, but it is not checked and not corrected.
	If discrepancies are going to be corrected, that should be done elsewhere.
	...or possibly there should be a $doCorrect boolean parameter.
      ASSUMES: there is at least one entry in the session list.
    */
    public function FigureTotalForSessionList(array $arIDs) {
	foreach ($arIDs as $id) {
	    $rc = $this->GetRecord_forKey($id);
	    $sSort = $rc->SortKey();
	    $arRecs[$sSort] = $rc->GetFieldValues();
	}
	ksort($arRecs);	// sort the sessions properly
	$dlrTotal = NULL;
	foreach ($arRecs as $sSort => $arRow) {
	    $rc->SetFieldValues($arRow);
	    $dlrLine = $rc->GetFiguredCost_dollars();
	    $dlrTotal += $dlrLine;
	}
	return $dlrTotal;
    }
    
    // -- CALCULATIONS -- //
    // ++ RECORD ++ //

    /*----
      INPUT:
	$idProj = ID of Project whose Sessions we want
	$doUseInvcd = if TRUE, include Sessions that have already been invoiced
      HISTORY:
	2017-03-11 This does not seem to need to be PUBLIC anymore; making it PROTECTED.
    */
    protected function Records_forProject($idProj,$doUseInvcd) {
	$qof = new fcSQLt_Filt('AND');
	$qof->AddCond('ID_Proj='.$idProj);

	// TODO: prevent ID_InvcLine from being saved as "0" when "NONE" is selected
	if ($doUseInvcd) {
	    $qof->AddCond('(ID_InvcLine IS NOT NULL) AND (ID_InvcLine != 0)');
	} else {
	    $qof->AddCond('(ID_InvcLine IS NULL) OR (ID_InvcLine=0)');
	}
	$sqlFilt = $qof->RenderValue();

	return $this->SelectRecords($sqlFilt,'WhenStart,IFNULL(Sort,1000),WhenFinish,Seq');
    }
    // PUBLIC because Invoice Line class uses it too
    public function Records_forinvoiceLine($idInvcLine) {
	$sqlFilt = 'ID_InvcLine = '.$idInvcLine;
	return $this->SelectRecords($sqlFilt,'WhenStart,IFNULL(Sort,1000),WhenFinish,Seq');
    }

    // -- RECORDS -- //
    // ++ WEB UI ++ //
    
    // PUBLIC because it needs to be called before we fetch the recordset, which depends on who is calling
    public function CreateMenuItems_forRows() {
	$sSfx = '.'.$this->GetActionKey();
	
	// ($sGroupKey,$sKeyValue=TRUE,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
	    
	$ol = new fcMenuOptionLink('do'.$sSfx,'new',NULL,NULL,'create new session');
	  $this->SetMenuOption_DoNewSession($ol);
	$ol = new fcMenuOptionLink('do'.$sSfx,'bal','balance',NULL,'recalculate running balance');
	  $this->SetMenuOption_DoRecalculate($ol);
	$ol = new fcMenuOptionLink('do'.$sSfx,'invc','invoice',NULL,'add sessions to a new or existing invoice');
	  $this->SetMenuOption_DoInvoice($ol);
	$ol = new fcMenuOptionLink('view'.$sSfx,'invcd','invoiced',NULL,'show sessions already invoiced');
	  $this->SetMenuOption_ViewInvoiced($ol);
    }
    public function AdminRows_forProject($idProj) {
	$this->CreateMenuItems_forRows();
	$doShowInvcd = $this->GetMenuOption_ViewInvoiced()->GetIsSelected();

	$rs = $this->Records_forProject($idProj,$doShowInvcd);
	$rs->SetProjectID($idProj);
	//return $rs->AdminRows();	// FUTURE - when using Ferreteria properly
	return $rs->AdminList();	// 2017-02-14 for now
    }
    public function AdminRows_forInvoiceLine($idInvcLine) {
	$this->CreateMenuItems_forRows();
	$rs = $this->Records_forinvoiceLine($idInvcLine);
	return $rs->AdminList();	// 2017-02-14 for now
    }
}
// base class so we can override trait methods
class wfcSession_base extends fcRecord_keyed_single_integer {
    use ftSaveableRecord;
}
class wfcSession extends wfcSession_base implements fiLinkableRecord, fiEventAware, fiEditableRecord {
    use ftLinkableRecord;
    use ftExecutableTwig;	// standard events

    // ++ EVENTS ++ //
  
    protected function OnCreateElements() {}
    protected function OnRunCalculations() {
	if ($this->IsNew()) {
	    $sBrow = 'new session';
	    $sCont = 'New Session';
	} else {
	    $id = $this->GetKeyValue();
	    $rcProj = $this->ProjectRecord();
	    $htProj = $rcProj->SelfLink_name();
	    $sProj = $rcProj->GetInvoicePrefix();
	    $sCont = "Session #$id in $htProj";
	    $sBrow = "s$id $sProj";
	}
    
	$oPage = fcApp::Me()->GetPageObject();
	$oPage->SetBrowserTitle($sBrow);
	$oPage->SetContentTitle($sCont);
    }
    public function Render() {
	return $this->AdminPage();
    }
    /*
    public function MenuExec() {
	return $this->AdminPage();
    }*/

    // -- EVENTS -- //
    // ++ STATIC ++ //

    /*----
      PURPOSE: Displays section header to use when Sessions are being displayed in a foreign context
	Currently only used by the Project record page.
      TODO: 2017-02-14 This probably belongs in wf-sess-form.php and probably should be non-static.
    */
    static public function SectionHdr(array &$arArgs,$urlBase) {
	throw new exception('2017-02-14 Nothing should be calling this anymore.');
	$doNew = $this->GetMenuOption_DoNewSession()->GetIsSelected();
	$doInvc = $this->GetMenuOption_DoInvoice()->GetIsSelected();
	$doForm = $doInvc || $doNew;
	$arArgs['doForm'] = $doForm;

	$oMenu = new fcHeaderMenu();
	$oHdr = new fcSectionHeader('Work Sessions',$oMenu);
	
	  $oMenu->SetNode($oGrp = new fcHeaderMenuGroup('do'));
	  
	    // ($sKeyValue,$sGroupKey=NULL,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
	    $oGrp->SetNode($this->GetMenuOption_DoNewSession());
	    $oGrp->SetNode($this->GetMenuOption_DoRecalculate());
	    $oGrp->SetNode($this->GetMenuOption_DoInvoice());

	  $oMenu->SetNode($oGrp = new fcHeaderMenuGroup('view'));

	    $oGrp->SetNode($this->GetMenuOption_ViewInvoiced());
	
	$out = $oHdr->Render();
	if ($doForm) {
	    $out .= "\n<form method=post>";
	    if ($doInvc) {
		$htBtns = '<div align=right><input type=submit name=btnInvc value="Create Invoice"></div>';
	    } else {
		$htBtns = '';
	    }
	    $out .= $htBtns;
	}
	return $out;
    }

    // -- STATIC -- //
    // ++ STATES ++ //
    
    private $doFinish = FALSE;
    protected function SetDoFinish($b) {
	$this->doFinish = $b;
    }
    protected function GetDoFinish() {
	return $this->doFinish;
    }

    // -- STATES -- //
    // ++ FIELD VALUES ++ //

    // PUBLIC for copying to invoice line
    public function Description() {
	return $this->GetFieldValue('Descr');
    }
    // PUBLIC so it can be set externally before creating a new Session record
    public function SetProjectID(int $id) {
        $this->SetFieldValue('ID_Proj',$id);
    }
    // PUBLIC so specialized Form object can access it
    public function GetProjectID() {
	if (is_null($this->GetFieldValue('ID_Proj'))) {
	    throw new exception('Project ID got set to NULL');
	}
        return $this->GetFieldValue('ID_Proj');
    }
    protected function GetRateID() {
	return $this->GetFieldValue('ID_Rate');
    }
    protected function GetInvoiceID() {
	return $this->GetFieldValue('ID_Invc');
    }
    // PUBLIC for ILine
    public function GetInvoiceLineID() {
        return $this->GetFieldValue('ID_InvcLine');
    }
    public function GetSequence() {
	return $this->GetFieldValue('Seq');
    }
    // PUBLIC for ILine and SessCalc
    public function CostLine_stored() {
	return $this->GetFieldValue('CostLine');
    }
    // PUBLIC for ILine to copy from
    public function CostBalance_stored() {
	return $this->GetFieldValue('CostBal');
    }
    // PUBLIC for ILine to copy from
    public function TimeTotal_stored() {
	return $this->GetFieldValue('TimeTotal');
    }
    /*----
      PUBLIC for ILine to copy from
      FORMAT: as stored in database
      NOTE: normally we wouldn't need to specify "dbase", because that's the assumed format
	for fields in a recordset, but since these timestamps are also needed in other formats,
	it seemed best to be unambiguous.
    */
    public function WhenStart_dbase() {
	return $this->GetFieldValue('WhenStart');
    }
    /*----
      PUBLIC for SessCalc to use
      FORMAT: as stored in database
    */
    public function WhenFinish_dbase() {
	return $this->GetFieldValue('WhenFinish');
    }
    // PUBLIC for SessCalc to use
    public function TimePlus() {
	return $this->GetFieldValueNz('TimeAdd');
    }
    // PUBLIC for SessCalc to use
    public function TimeMinus() {
	return $this->GetFieldValueNz('TimeSub');
    }
    // PUBLIC for SessCalc to use
    public function CostPlus() {
	return $this->GetFieldValueNz('CostAdd');
    }

    // -- FIELD VALUES -- //
    // ++ FIELD CALCULATIONS ++ //

    protected function HasProject() {
	if ($this->HasRow()) {
	    return !is_null($this->GetProjectID());
	} else {
	    return FALSE;
	}
    }
    protected function HasInvoice() {
	return !is_null($this->GetInvoiceID());
    }
   /*----
      PURPOSE: Calculate the next Seq number to use for the given Project
    */
    protected function GetNextSeq() {
	$idProj = $this->GetProjectID();
	if (empty($idProj)) {
	    throw new exception('WorkFerret Internal Error: Session needs to have Project ID set before it can calculate next seq.');
	}
	$sqlFilt = "ID_Proj=$idProj";
	$sqlSort = 'Seq DESC';
	$sqlOther = 'LIMIT 1';
	$rc = $this->GetTableWrapper()->SelectRecords($sqlFilt,$sqlSort,$sqlOther);
	if ($rc->HasRows()) {
	    $rc->NextRow();
	    $seq = $rc->GetFieldValue('Seq');
	} else {
	    $seq = 0;
	}
	return $seq+1;
    }
    /*----
      HISTORY:
	2017-03-09 Added the FieldIsSet() check because this happens when no Rate ID is set.
	  Feels like this shouldn't actually happen, but I don't have time to investigate right now.
    */
    protected function HasRate() {
	if ($this->FieldIsSet('ID_Rate')) {
	    return $this->FieldIsNonZeroInt('ID_Rate');
	} else {
	    return FALSE;
	}
    }
    public function SortKey() {
	return $this->GetFieldValue('WhenStart')
	  .'.'.$this->GetFieldValue('Sort')
	  .'.'.sprintf('%04d',$this->GetKeyValue())
	  ;
    }
    protected function WhenStart_isSet() {
	return $this->FieldIsNonBlank('WhenStart');
    }
    /*----
      RETURNS: Session start time in UNIX integer format
    */
    protected function WhenStart_UT() {
    //echo 'START in DBASE:['.$this->WhenStart_dbase().']<br>';
	return strtotime($this->WhenStart_dbase());	// convert dbase string time to UNIX time
    }
    public function WhenStart_text() {
	$utTime = $this->WhenStart_UT();	// start time as UNIX integer
	$out = date('Y-m-d',$utTime);		// convert UNIX time to string
	return $out;
    }
    protected function WhenFinish_UT() {
    //echo 'FINISH FRIENDLY:['.$this->WhenFinish_dbase().']<br>';
	return strtotime($this->WhenFinish_dbase());	// convert friendly time to UNIX time
    }
    protected function WhenFinish_isSet() {
	return $this->FieldIsNonBlank('WhenFinish');
    }
    public function WhenEntered_text() {
	$utTime = strtotime($this->GetFieldValue('WhenEntered'));	// convert SQL time to UNIX time
	$out = date('Y-m-d',$utTime);				// convert UNIX time to string
	return $out;
    }
    public function WhenEdited_text() {
	$utTime = strtotime($this->GetFieldValue('WhenEdited'));	// convert SQL time to UNIX time
	$out = date('Y-m-d',$utTime);				// convert UNIX time to string
	return $out;
    }
    public function WhenFigured_text() {
	$utTime = strtotime($this->GetFieldValue('WhenFigured'));	// convert SQL time to UNIX time
	$out = date('Y-m-d',$utTime);				// convert UNIX time to string
	return $out;
    }
    protected function TimeSpan_isSet() {
	return ($this->WhenStart_isSet() && $this->WhenFinish_isSet());
    }
    protected function TimePlus_isSet() {
	return !is_null($this->TimePlus());
    }
    protected function TimeMinus_isSet() {
	return !is_null($this->TimeMinus());
    }
    protected function CostPlus_isSet() {
	return !is_null($this->CostPlus());
    }
    /*----
      RETURNS: Calculation of the final cost for this session, based on:
	* WhenStart
	* WhenFinish
	* TimeAdd
	* TimeSub
	* BillRate
	* CostAdd
	Tentatively, this can be used as the value for CostLine.
      HISTORY:
	2018-05-31 Renamed from Cost_calc() -> GetFiguredCost_dollas()
    */
    public function GetFiguredCost_dollars() {
	if ($this->UsesTime()) {
	    $dlrRate = $this->RateUsed();
	    $nHours = $this->TimeFinal_hours_rounded();
	    $dlrCalc = $dlrRate * $nHours;
	} else {
	    $dlrCalc = 0;
	}
	$dlrCalc += $this->CostPlus();
	return $dlrCalc;
    }
    public function GetFiguredCost_cents() {
	if ($this->UsesTime()) {
	    $dlrRate = $this->RateUsed();
	    $nMins = $this->TimeFinal_minutes();
	    $ctsCalc = round((($dlrRate*100) * $nMins) / 60);
	} else {
	    $ctsCalc = 0;
	}
	$ctsCalc += round($this->CostPlus() * 100);
	return $ctsCalc;
    }
    /*----
      RETURNS: TRUE iff enough fields are set to determine a cost based on time
    */
    protected function UsesTime() {
	return $this->TimePlus_isSet() || $this->TimeMinus_isSet() || $this->TimeSpan_isSet();
    }
    protected function TimeSpan_seconds() {
	if ($this->TimeSpan_isSet()) {
	    //echo 'TIME START: '.$this->WhenStart_UT().' .. TIME FINISH: '.$this->WhenFinish_UT().'<br>';
	    return $this->WhenFinish_UT() - $this->WhenStart_UT();
	} else {
	    return 0;
	}
    }
    protected function TimeSpan_minutes() {
	$nSpanSeconds = $this->TimeSpan_seconds();
	$fltSpanMinutes = ($nSpanSeconds) / 60;
	return (int)$fltSpanMinutes;
    }
    protected function TimeSpan_hours() {
	$nSpanSeconds = $this->TimeSpan_seconds();
	$fltSpanHours = ($nSpanSeconds) / 3600;
	return (int)$fltSpanHours;
    }
    protected function TimeAdjustments_minutes() {
	$nMinutes = 0;
	$nMinutes += $this->TimePlus();
	$nMinutes -= $this->TimeMinus();
	return $nMinutes;
    }
    protected function TimeAdjustments_hours() {
	$fltHours = $this->TimeAdjustments_minutes() / 60;
	return (int)$fltHours;
    }
    // PUBLIC for SessionSummer
    public function TimeFinal_minutes() {
	return $this->TimeSpan_minutes() + $this->TimeAdjustments_minutes();
    }
    /*----
      PUBLIC for ILine to copy from
      NOTE: $nRound was originally 0.1, but this led to significant rounding errors
	over sufficiently large invoices, e.g. ~$50 over 2 weeks @ $55/hr.
      HISTORY:
	2018-06-01 changed $nRound from 0.1 to 0.001
	  Renamed from TimeFinal_hours() to TimeFinal_hours_rounded()
    */
    public function TimeFinal_hours_rounded($nRound = 0.001) {
	$fltHours = $this->TimeFinal_minutes() / 60;
	return ((int)($fltHours / $nRound)) * $nRound;
    }
    // 2018-06-01 ...because, why are we even rounding? Leads to rounding errors.
    public function TimeFinal_hours() {
	return $this->TimeFinal_minutes() / 60;
    }
    /*----
      RETURNS: TRUE iff the final total is based only on hours worked
    */
    public function IsTimeBased() {
	return !$this->CostPlus_isSet();
    }
    /*----
      RETURNS: TRUE if the Rate is based on quantity (typically hours)
      PUBLIC so that Form object can call it at save time
    */
    public function HasQtyRate() {
	if ($this->HasRate()) {
	    $rcRate = $this->RateRecord();
	    return $rcRate->IsQtyBased();
	}
	return FALSE;
    }
    protected function RateSelected() {
	$rcRate = $this->RateRecord();
	if (is_object($rcRate)) {
	    $dlrRate = $rcRate->GetFieldValue('PerHour');
	} else {
	    $dlrRate = NULL;
	}
	return $dlrRate;
    }
    public function RateUsed() {
	if ($this->IsNew() || is_null($this->GetFieldValue('BillRate'))) {
	    $dlrRate = $this->RateSelected();
	} else {
	    $dlrRate = $this->GetFieldValue('BillRate');
	}
	return $dlrRate;
    }
    /*----
      ACTION: Ensures that BillRate is always set if there's a BillRate type.
      HISTORY:
	2016-04-11 written
    *//*
    protected function FigureEffectiveBillRate() {
	if ($this->HasRate() && is_null($this->Value('BillRate'))) {
	    $this->Value('BillRate',$this->RateUsed());
	}
    }//*/

    // -- FIELD CALCULATIONS -- //
    // ++ FORMATTING ++ //

    // 2017-02-08 Adapted from WorkFerret.main.php:ShowBal()
    static protected function FormatBalance($nSaved,$nCalc) {
	if ($nSaved == $nCalc) {
	    return $nSaved;
	} else {
	    return '<small><s>'.$nSaved.'</s><br>'.$nCalc.'</small>';
	}
    }

    // -- FORMATTING -- //
    // ++ TABLES ++ //

    protected function ProjectTable($id=NULL) {
        return $this->GetConnection()->MakeTableWrapper('cwftProjects',$id);
    }
    protected function RateTable($id=NULL) {
        return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_RATES,$id);
    }
    protected function InvoiceTable($id=NULL) {
        return $this->GetConnection()->MakeTableWrapper('wftcInvoices',$id);
    }
    protected function InvoiceLineTable($id=NULL) {
        return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_INVOICE_LINES,$id);
    }

    // -- TABLES -- //
    // ++ RECORDS ++ //

      //++single++//
    
    private $rcProj;
    public function ProjectRecord() {
	$idProj = $this->GetProjectID();
	if (is_null($idProj)) {
	    return NULL;
	} else {
	    $doLoad = TRUE;
	    if (is_object($this->rcProj)) {
		if ($this->rcProj->GetKeyValue() == $idProj) {
		    $doLoad = FALSE;
		}
	    }
	    if ($doLoad) {
		$this->rcProj = $this->ProjectTable($idProj);
	    }
	    return $this->rcProj;
	}
    }
    private $rcRate;
    public function RateRecord() {
	if ($this->HasRate()) {
	    $idRate = $this->GetRateID();
	    if (!is_null($idRate)) {
		$doLoad = TRUE;
		if (is_object($this->rcRate)) {
		    if ($this->rcRate->GetKeyValue() == $idRate) {
			$doLoad = FALSE;
		    }
		}
		if ($doLoad) {
		    $this->rcRate = $this->RateTable($idRate);
		}
		return $this->rcRate;
	    }
	}
	return NULL;
    }
    private $rcInvc;
    public function InvoiceRecord() {
	$idInvc = $this->GetInvoiceID();
	if (is_null($idInvc)) {
	    return NULL;
	} else {
	    $doLoad = TRUE;
	    if (is_object($this->rcInvc)) {
		if ($this->rcInvc->GetKeyValue() == $idInvc) {
		    $doLoad = FALSE;
		}
	    }
	    if ($doLoad) {
		$this->rcInvc = $this->InvoiceTable($idInvc);
	    }
	    return $this->rcInvc;
	}
    }
    private $rcInvcLine;
    public function InvcLineRecord() {
	$idLine = $this->GetInvoiceLineID();
	if (is_null($idLine)) {
	    return NULL;
	} else {
	    $doLoad = TRUE;
	    if (is_object($this->rcInvcLine)) {
		if ($this->rcInvcLine->GetKeyValue() == $idLine) {
		    $doLoad = FALSE;
		}
	    }
	    if ($doLoad) {
		$this->rcInvcLine = $this->InvoiceLineTable($idLine);
	    }
	    return $this->rcInvcLine;
	}
    }

      //--single--//
      //++multi++//

    // PUBLIC so Form object can call it
    public function ProjectRecords_forChoice() {
	$arArgs = array(
	  'inact'	=> !$this->ProjectRecord()->IsActive(),
	  'always'	=> $this->GetProjectID(),				// always show $idProj even if inactive
	);
	return $this->ProjectTable()->Records_forDropDown($arArgs);
    }
    // PUBLIC so Form object can call it
    public function InvoiceLineRecords_forChoice() {
	$tILines = $this->InvoiceLineTable();
	if ($this->HasInvoice()) {
	    $rs = $tILines->SelectRecords('ID_Invc='.$this->GetInvoiceID());
	    return $rs;
	} else {
	    return NULL;
	}
    }

      //--multi--//
    // -- RECORDS -- //
    // ++ WRITE DATA ++ //

      //++calculations++//
    
    /*----
      OVERRIDE
    */
    public function GetStorableValues_Changed() {
	$ar = parent::GetStorableValues_Changed();
	$ar['CostLine'] = $this->GetFiguredCost_dollars();
	return $ar;
    }
    protected function GetStorageValue_BillRate() {
	if ($this->HasQtyRate()) {
	    $sql = $this->RateUsed();
	} else {
	    $sql = 'NULL';
	}
	return $sql;
    }
    /*----
      OVERRIDE
      HISTORY:
	2017-06-15 $ar param has been eliminated by parent, which has been renamed.
    */
    public function GetInsertStorageOverrides() {
	$ar['WhenEntered'] = 'NOW()';
	$ar['Seq'] = $this->GetNextSeq();	// integer is compatible with both storage and native
	$ar['BillRate'] = $this->GetStorageValue_BillRate();
	if ($this->GetDoFinish()) {
	    $ar['WhenFinish'] = 'NOW()';
	}
	return $ar;
    }
    /*----
      OVERRIDE
      HISTORY:
	2017-06-15 $ar param has been eliminated by parent.
    */
    public function GetUpdateStorageOverrides() {
	$ar['WhenEdited'] = 'NOW()';
	$ar['BillRate'] = $this->GetStorageValue_BillRate();
	return $ar;
    }

      //--calculations--//

    /*----
      ACTION: Assign this Session to an invoice.
      INPUT: $idLine is the specific invoice line to which it should be attached.
    */
    public function Assign_toInvoice(wfcrInvcLine $rcLine) {
	$idInvc = $rcLine->GetInvoiceID();
	$arUpd = array(
	  'ID_Invc'	=> $idInvc,
	  'ID_InvcLine'	=> $rcLine->GetKeyValue(),
	  );
	$this->Update($arUpd);
    }

    // -- WRITE DATA -- //
    // ++ WEB INTERFACE: CONTROLS ++ //
    
    const KS_BTN_SAVE_EDIT = 'btnSaveSessEdit';		// wfcSession::KS_BTN_SAVE_EDIT
    const KS_BTN_SAVE_ENTRY = 'btnSaveEntry';	// wfcSession::KS_BTN_SAVE_ENTRY
    const KS_BTN_FINISH_ENTRY = 'btnFinEntry';	// wfcSession::KS_BTN_FINISH_ENTRY
    
    // 2015-05-03 these may be obsolete shortly...

    /* 2017-02-14 Seems not to be in use.
    public function Rates_DropDown() {
	$arArgs = array(
	  'default'	=> $this->GetFieldValueNz('ID_Rate'),
	  'name'	=> 'ID_Rate',
	  'none'	=> 'none available'
	  );
	$htOut = $this->ProjectRecord()->Rates_DropDown($arArgs);
	return $htOut;
    }*/
    public function CheckBox($iName='sess',array $iAttr=NULL) {
	$htAttr = NULL;
	if (is_array($iAttr)) {
	    foreach ($iAttr as $key=>$val) {
		if ($val === TRUE) {
		    $htAttr .= ' '.$key;
		} elseif ($val === FALSE) {
		    // add nothing
		} else {
		    $htAttr .= ' '.$key.'="'.$val.'"';
		}
	    }
	}
	$out = '<input type=checkbox name="'.$iName.'['.$this->GetKeyValue().']"'.$htAttr.'>';
	return $out;
    }
    /* 2017-02-14 Seems not to be in use.
    public function RadioBtn($iName='sess',array $iAttr=NULL) {
	$htAttr = NULL;
	if (is_array($iAttr)) {
	    foreach ($iAttr as $key=>$val) {
		if ($key === TRUE) {
		    $htAttr .= ' '.$key;
		} elseif ($key === FALSE) {
		    // add nothing
		} else {
		    $htAttr .= ' '.$key.'="'.$val.'"';
		}
	    }
	}
	$out = '<input type=radio name="'.$iName.'" value="'.$this->GetKeyValue().'"'.$htAttr.'>';
	return $out;
    } */

    // -- WEB INTERFACE: CONTROLS -- //
    // ++ WEB INTERFACE: TEMPLATES ++ //

    private $tpLine;
    protected function LineTemplate() {
	if (empty($this->tpLine)) {
            $sNow = date('n/j G:i');
            $htBtnSave		= wfcSession::KS_BTN_SAVE_ENTRY;
            $htBtnFinish	= wfcSession::KS_BTN_FINISH_ENTRY;
	    $sTplt = <<<__END__
<tr>
  <td colspan=2>
    <input type=hidden name="context" value="session">
    [#ID_Proj#]
      new
  </td>
  <td colspan=2></td>
  <td>[#WhenStart#]</td>
  <td>[#WhenFinish#]</td>
  <td>[#Sort#]</td>
  <td>[#TimeAdd#]</td>
  <td>[#TimeSub#]</td>
  <td align=center>-</td>
  <td>[#ID_Rate#]</td>
  <td align=center>-</td>
  <td>[#CostAdd#]</td>
</tr>
<tr>
  <td colspan=12>
    Description: [#Descr#]
    <input type=submit name="$htBtnSave" value="Create">
    <input type=submit name="$htBtnFinish" value="Finish">
  </td>
</tr>
__END__;
	    $this->tpLine = new fcTemplate_array('[#','#]',$sTplt);
	}
	return $this->tpLine;
    }
    private $tpPage;
    protected function PageTemplate() {
	if (empty($this->tpPage)) {
	    $sTplt = <<<__END__
<table class=form-record>
  <tr><td align=right><b>ID</b>:</td><td>[#ID#]</td></tr>
  <tr><td align=right><b>Project</b>:</td><td>[#ID_Proj#]</td></tr>
  <tr><td align=right><b>Started</b>:</td><td>[#WhenStart#]</td></tr>
  <tr><td align=right><b>Finished</b>:</td><td>[#WhenFinish#]</td></tr>
  <tr><td align=right><b>Sort</b>:</td><td>[#Sort#]</td></tr>
  <tr><td align=right><b>+Time</b>:</td><td>[#TimeAdd#]</td></tr>
  <tr><td align=right><b>-Time</b>:</td><td>[#TimeSub#]</td></tr>
  [#!TimeTotalRow#]
  <tr><td align=right><b>Rate Type</b>:</td><td>[#ID_Rate#]</td></tr>
  <tr><td align=right><b>Rate Used</b>:</td><td>[#BillRate#]</td></tr>
  <tr><td align=right><b>+Cost</b>:</td><td>[#CostAdd#]</td></tr>
  [#!CostTotalRow#]
  <tr><td align=right><b>Invoice</b>:</td><td>[#ID_Invc#]</td></tr>
  <tr><td align=right><b>Invoice Line</b>:</td><td>[#ID_InvcLine#]</td></tr>

  <tr><td align=right><b>Description</b>:</td><td>[#Descr#]</td></tr>
  <tr><td align=right><b>Notes</b>:</td><td>[#Notes#]</td></tr>
  <tr><td align=right><b>When Entered</b>:</td><td>[#WhenEntered#]</td></tr>
  <tr><td align=right><b>When Edited</b>:</td><td>[#WhenEdited#]</td></tr>
  <tr><td align=right><b>When Figured</b>:</td><td>[#WhenFigured#]</td></tr>
</table>
__END__;
	    $this->tpPage = new fcTemplate_array('[#','#]',$sTplt);
	}
	return $this->tpPage;
    }

    // -- WEB INTERFACE: TEMPLATES -- //
    // ++ WEB INTERFACE: FORMS ++ //

    private $frmLine;
    protected function LineForm() {
	if (is_null($this->frmLine)) {
	    $oForm = new wfcForm_Session_line($this);
	    $this->frmLine = $oForm;
	}
	return $this->frmLine;
    }
    /*----
      HISTORY:
	2013-06-08 adapted from VbzCart:VbzAdminStkItem to WorkFerret:clsWFProject (later renamed cwfrProject)
	2013-10-23 changed from private to public (needed when adding new session)
    */
    private $frmPage;
    public function PageForm() {
	if (empty($this->frmPage)) {
	    $oForm = new wfcForm_Session_page($this);
	    $this->frmPage = $oForm;
	}
	return $this->frmPage;
    }

    // -- WEB INTERFACE: FORMS -- //
    // ++ WEB INTERFACE: DISPLAY ++ //

    /*-----
      NOTES: This ought to be somehow consolidated with AdminPage, so we don't duplicate edit-handling code
    */
    public function DoEditRow() {
	$frmEdit = $this->LineForm();
        $frmEdit->ClearValues();  // for now, this is only used to add new rows
	$arCtrls = $frmEdit->RenderControls(TRUE); // always edit
	$oTplt = $this->LineTemplate();

  	// render the template
	$oTplt->SetVariableValues($arCtrls);
	$out = $oTplt->Render();

	return $out;
    }
    protected function AdminList_menu() {
	$oMenu = new fcHeaderMenu();
	$oHdr = new fcSectionHeader('Work Sessions',$oMenu);

	if ($this->HasProject()) {
	    // these menu options only make sense inside a Project page

	    $t = $this->GetTableWrapper();
	    
	      $oMenu->SetNode($oGrp = new fcHeaderMenuGroup('do'));
	      
		// ($sKeyValue,$sGroupKey=NULL,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
		$oGrp->SetNode($t->GetMenuOption_DoNewSession());
		$oGrp->SetNode($t->GetMenuOption_DoRecalculate());
		$oGrp->SetNode($t->GetMenuOption_DoInvoice());

	      $oMenu->SetNode($oGrp = new fcHeaderMenuGroup('view'));

		$oGrp->SetNode($t->GetMenuOption_ViewInvoiced());
	}

	return $oHdr->Render();
    }
    /*-----
      INPUT:
	$iDoEdit: TRUE = table should be formatted for editing (allow for wider columns
	  by putting description on separate line)
    */
    static protected function TableHdr($iDoEdit) {
	$out = "\n<tr>\n"
	      .'<th colspan=3>ID</th>'
	      .'<th>Date</th>'
	      .'<th>Start</th>'
	      .'<th>Finish</th>'
	      .'<th>Sort</th>'
	      .'<th>+Time</th>'
	      .'<th>-Time</th>'
	      .'<th>t Tot</th>'
	      .'<th>$Rate</th>'
	      .'<th>t*R</th>'
	      .'<th>+$</th>'
	      .'<th>$ Tot</th>'
//	      .'<th>Invoice</th>'
	      .'<th><big>&sum;</big>$</th>';
	if ($iDoEdit) {
	    // multiline header is more confusing than helpful, so do nothing
	} else {
	    $out .= '<th>Description</th>';
	}
	$out .= '</tr>';
	return $out;
    }
    // PURPOSE: handle input from the new-session-entry form
    protected function HandleEntryForm() {
	$oFormIn = fcHTTP::Request();
	$doFinish = $oFormIn->GetBool(wfcSession::KS_BTN_FINISH_ENTRY);
	$doSave = $oFormIn->GetBool(wfcSession::KS_BTN_SAVE_ENTRY);
	
	/*
	echo 'POST:'.\fcArray::Render($_POST);
	echo 'OFORMIN:'.$oFormIn->DumpHTML();
	die("FINISH=[$doFinish]"); //*/

	/*
	if ($doFinish) {	// finish new session (save with end at current time)
	    $this->EditFinish_line();
	    $this->ProjectRecord()->SelfRedirect();
	}*/
	if ($doSave || $doFinish) {	// save new session
	    $this->SetDoFinish($doFinish);
	    $this->EditSave_line();
	    $this->ProjectRecord()->SelfRedirect();
	}
    }
    /*-----
      RETURNS: HTML table of all sessions in dataset
      TODO: convert this to use Ferreteria Forms
      HISTORY:
	2017-02-14 Significant revisions begun
    */
    public function AdminList() {
	$this->HandleEntryForm();
	$out = $this->AdminList_menu();
    
	$t = $this->GetTableWrapper();
	$doNew = $t->GetMenuOption_DoNewSession()->GetIsSelected();

	if ($this->hasRows() || $doNew) {
	    $doInvc = $t->GetMenuOption_DoInvoice()->GetIsSelected();
	    $doBal = $t->GetMenuOption_DoRecalculate()->GetIsSelected();
	    $doEdit = $doNew;	// we might eventually edit existing lines, but for now...
	    $doForm = $doEdit || $doInvc;

	    if ($doForm) {
		$out .= "\n<form method=post>";
		if ($doInvc) {
		    $out .= '<div align=right><input type=submit name=btnInvc value="Collect Sessions"></div>';
		}
	    }

	    $out .= "\n<table class=listing>".self::TableHdr($doEdit);

	    $isOdd = TRUE;

	    if ($doNew) {
		$out .= $this->DoEditRow();   // add form row
	    }
	    $oCalc = new SessionRecordCalc($this);
	    //$oTotalYr = new cSessionSummer_Year();
	    //$oTotalMo = new cSessionSummer_Month();
	    //$oTotalDy = new cSessionSummer_Day();
	    $oSums = new cSummerManager_YMD();
	    while ($this->NextRow()) {
/* TODO: if Seq is 0 (or NULL), set it (...if the sorting is appropriate; maybe we should signal that
  with a flag argument to this function)
*/
		//$wtStyle = $isOdd?'background:#ffffff;':'background:#eeeeee;';
		$cssClass = $isOdd?'odd':'even';
		$isOdd = !$isOdd;

		$oCalc->CalcRow();
		$oCalc->SumRow();
		$oCalc->Update();
                $ftStop = $oCalc->ftStop;
                $ftWorkCost = $oCalc->dlrWorkCost;
                $ftCostLine = self::FormatBalance($this->GetFieldValue('CostLine'),$oCalc->dlrCostTot);
                $ftCostBal = self::FormatBalance($this->GetFieldValue('CostBal'),$oCalc->dlrBalCalc);
                $needCalc = $oCalc->needCalc;

		// CALCULATIONS FOR DISPLAY

		$strWhenStart = $this->GetFieldValue('WhenStart');
		$dtStart = strtotime($strWhenStart);

		$out .= $oSums->RenderHeaders($dtStart);
		/*
		$dyCur = date('Y.m.j',$dtStart);
		if ($dyLast != $dyCur) {
		    $out .= $oTotalDy->Render();
		    $dyLast = $dyCur;
		    
		    $moCur = date('Y.m',$dtStart);
		    if ($moLast != $moCur) {
			$out .= $oTotalMo->Render();
			$moLast = $moCur;
			
			$yrCur = date('Y',$dtStart);
			if ($yrLast != $yrCur) {
			    $out .= $oTotalYr->Render();
			    $yrLast = $yrCur;
			    //$out .= "\n<tr style=\"background: #444466; color: #ffffff;\"><td colspan=12><b>$yrCur</b></td></tr>";
			    $out .= "\n<tr class='header-year'><td colspan=12><b>$yrCur</b></td></tr>";
			}
			
			$sMonth = date('F',$dtStart);
			$out .= "\n<tr class='header-month'><td colspan=11>$sMonth</td></tr>";
		    }
		    
		    $nDay = date('j',$dtStart);
		    $sSfx = date('S',$dtStart);
		    $sDOW = date('l',$dtStart);
		    $sDay = "$nDay<sup>$sSfx</sup> ($sDOW)";
		    $out .= "\n<tr class='header-day'><td colspan=10>$sDay</td></tr>";
		}
		*/
		$out .= $oSums->RenderHeaders($dtStart);	// also renders totals
		$oSums->AddSession($this);

		$ftDate = date('m/d',$dtStart);
		$ftStart = fcTime::DefaultDate($strWhenStart,$strWhenStart);

		$ftTimeAdd = $this->GetFieldValue('TimeAdd');
		$ftTimeSub = $this->GetFieldValue('TimeSub');
		$ftTimeTot = $this->GetFieldValue('TimeTotal');
		
		$sRate = $this->GetFieldValue('BillRate');
		if (is_null($this->GetFieldValue('ID_Rate'))) {
		    $ftRate = $sRate;
		} else {
		    $rcRate = $this->RateRecord();
		    if (is_null($rcRate)) {
			if (is_null($sRate)) {
			    $sRate = '<span class=error>n/a!</span>';	// 2017-02-09 This may represent an internal error.
			}
			$ftRate = $sRate;
		    } else {
			if (is_null($sRate)) {
			    $sRate = '('.$rcRate->GetRateValue().')';
			}
			$ftRate = $rcRate->SelfLink($sRate);
		    }
		}
		$ftCostAdd = $this->GetFieldValue('CostAdd');
		$ftCostTot = $this->GetFieldValue('CostLine');

		$vSort = $this->GetFieldValue('Sort');
		$ftSort = empty($vSort)?'-':sprintf('%0.1f',$vSort);

		$ftID_link = $this->SelfLink();
		if ($needCalc) {
		    $ftDelta = '<span style="color: red;">&Delta;</span>';
		} else {
		    $ftDelta = '';
		}
		if ($doInvc) {
		    $ftID = '<td>'.$this->CheckBox('sess',array('checked'=>TRUE)).'</td><td>'.$ftDelta.'</td><td>'.$ftID_link.'</td>';
		} else {
		    $ftID = '<td align=right>'.$this->GetFieldValue('Seq').'</td><td>'.$ftDelta.'</td><td>'.$ftID_link.'</td>';
		}

		$ftDescr = $this->GetFieldValue('Descr');
		$txtNotes = $this->GetFieldValue('Notes');
		if (!is_null($txtNotes)) {
		    $ftDescr .= " <i>$txtNotes</i>";
		}

	      // INVOICE INFO
		$ftInvc = '';

		$idInvc = $this->GetFieldValue('ID_Invc');
		if ($idInvc <= 0) {
		    $ftInvc .= '<b>no invc</b>';
		} else {
		    $rcInvc = $this->InvoiceRecord();
		    if (is_null($rcInvc)) {
			$ftInvc .= '<b>unknown invc ID='.$idInvc.'</b>';
		    } else {
			$ftInvc .= '<b>invc</b> '.$rcInvc->SelfLink($rcInvc->GetFieldValue('InvcNum'));
		    }
		}

		$ftInvc .= ' | ';

		$idILine = $this->GetFieldValue('ID_InvcLine');
		if ($idILine <= 0) {
		    $ftInvc .= '<b>no line</b>';
		} else {
		    $rcILine = $this->InvcLineRecord();
		    if (is_null($rcILine)) {
			$ftInvc .= '<b>unknown line ID='.$idILine.'</b>';
		    } else {
			$ftInvc .= '<b>line</b> '.$rcILine->SelfLink($rcILine->ShortDesc());
		    }
		}
		//$htTR = "\n<tr style=\"$wtStyle\">";
		$htTR = "\n<tr class=\"$cssClass\">";
		$out .= $htTR
		  ."\n$ftID"
		  ."<td align=center>$ftDate</td>"
		  ."<td align=center>$ftStart</td>"
		  ."<td align=center>$ftStop</td>"
		  ."<td align=center>$ftSort</td>"
		  ."<td align=right>$ftTimeAdd</td>"
		  ."<td align=right>$ftTimeSub</td>"
		  ."<td align=right>$ftTimeTot</td>"
		  ."<td align=right>$ftRate</td>"
		  ."<td align=right>$ftWorkCost</td>"	// t * R
		  ."<td align=right>$ftCostAdd</td>"	// +$
		  ."<td align=right>$ftCostLine</td>"	// $ tot
		  //."<td align=right><small>$ftInvc</small></td>"	// invoice info
		  ."<td align=right>$ftCostBal</td>";	// SUM($)
		if ($doEdit) {
		    $out .= "</tr>$htTR<td colspan=3></td><td colspan=11>$ftDescr</td>";
		} else {
		    $out .= "<td>$ftDescr</td>";
		}
		if (($idInvc > 0) || ($idILine > 0)) {
		    $out .= "</tr>$htTR<td colspan=3></td><td colspan=12>$ftInvc</td>";
		}
		$out .= '</tr>';
	    }
	    $out .= 
	      //$oTotalDy->Render()
	      //.$oTotalMo->Render()
	      //.$oTotalYr->Render()
	      $oSums->RenderSums()
	      ."\n</table>"
	      ;
	    // this has to go outside the table
	    if ($doForm) {
		$out .= "\n</form>";
	    }
	} else {
	    $out .= '<div class=content>No sessions found.</div>';
	}
	return $out;
    }
    function AdminPage() {
	$oPathIn = fcApp::Me()->GetKioskObject()->GetInputObject();
	$oFormIn = fcHTTP::Request();
    
	$strAct = $oPathIn->GetString('do');
	$doEdit = $oPathIn->GetBool('edit');
	$doAdd = ($strAct == 'new');
	$doCalc = ($strAct == 'recalc');
	$doUpd = ($strAct == 'update');
	$doForm = $doEdit || $doAdd;
	$doSave = $oFormIn->GetBool(wfcSession::KS_BTN_SAVE_EDIT);

	if ($doSave) {		// save entered session
	    $this->EditSave_page();
	    $this->SelfRedirect();	// clear the form data out of the page reload
	}

	if ($doCalc) {
	    $objCalc = new SessionRecordCalc($this);
	    $objCalc->CalcRow();
	    $objCalc->Update();
	    $this->Reload();
	}

	// do the header, with edit link if appropriate
	if ($doAdd) {
	    $doEdit = TRUE;	// new row must be edited before it can be created
	    $this->SetKeyValue($oPathIn->GetInt('sess'));
	    //$sTitle = 'New session';
	} else {
	    $id = $this->GetKeyValue();
	    $strDesc = $this->GetFieldValue('WhenStart');
	    $intSeq = $this->GetFieldValue('Seq');
	    if ($intSeq) {
		$strDesc .= '.'.$intSeq;
	    }
	    //$sTitle = 'Session '.$id.' - '.$strDesc;
	}
	//fcApp::Me()->GetPageObject()->SetPageTitle($sTitle);
	
	$oMenu = fcApp::Me()->GetHeaderMenu();
	  // ($sGroupKey,$sKeyValue=TRUE,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
	  $oMenu->SetNode($ol = new fcMenuOptionLink('edit',TRUE,NULL,NULL,'edit this session'));
	    //$ol->SetBasePath($this->SelfURL());

	$tInvcs = $this->InvoiceTable();
	$tILines = $this->InvoiceLineTable();

	$isNew = $this->IsNew();
	$frmEdit = $this->PageForm();
	if ($isNew) {
	    $frmEdit->ClearValues();
	} else {
	    $frmEdit->LoadRecord();
	}
	$oTplt = $this->PageTemplate();
	$arCtrls = $frmEdit->RenderControls($doEdit);
	  // custom vars
	  $arCtrls['ID'] = $this->SelfLink();
	  $arCtrls['WhenEntered'] = $this->WhenEntered_text();
	  $arCtrls['WhenEdited'] = $this->WhenEdited_text();
	  $arCtrls['WhenFigured'] = $this->WhenFigured_text();

	if ($doForm) {
	    $out = "\n<form method=post>";

	    // ++ CUSTOM VARS

	    $arCtrls['!TimeTotalRow'] = '';
	    $arCtrls['!CostTotalRow'] = '';
	    // TODO: add an [update] link somewhere appropriate

	    // -- CUSTOM VARS

	    $oTplt->SetVariableValues($arCtrls);
	    $out .= $oTplt->Render();

	    $out .=
	      '<input type=submit name="'
	      .wfcSession::KS_BTN_SAVE_EDIT
	      .'" value="Save">'
	      .'</form>';
	} else {

	    // ++ CALCULATIONS

	    //$this->FigureEffectiveBillRate();	// make sure PerHour is set right

	    $objCalc = new SessionRecordCalc($this);
	      $objCalc->CalcRow();
	      $intTimeTotCalc	= $objCalc->intTimeTot;
	      $dlrCalcCost	= $objCalc->dlrCostTot;
	      $needCalc		= $objCalc->needCalc;

	    if ($doUpd) {
		// NOTE: if BillRate is NOT NULL, then $dlrCalcCost should be set to it. Check this...
		$arUpd = array(
		  'CostLine'	=> $dlrCalcCost
		  );
		if (is_null($this->GetFieldValue('BillRate'))) {
		    $arUpd['BillRate'] = $this->RateSelected();
		}
		$this->Update($arUpd);
		// we could just reload here, but it gets annoying when debugging
		$this->SelfRedirect();
	    }

	    // // ++ TIME CALCULATIONS

	    $intTimeTot = $this->GetFieldValue('TimeTotal');
	    $htTimeTot = empty($intTimeTot)?'-':$intTimeTot.' minute'.fcString::Pluralize($intTimeTot);
	    if ($intTimeTot==$intTimeTotCalc) {
		// calculated time matches recorded time
		$htTimeTotCalc = '&radic;';
	    } else {
		// recorded time needs recalculating
		$htTimeTotCalc = '&rarr; '.$intTimeTotCalc.' minute'.fcString::Pluralize($intTimeTotCalc);
		$needCalc = TRUE;
	    }
	    $htTimeTotRow = "\n<tr><td align=right><b>Total Time</b>:</td><td><i>$htTimeTot</i></td><td>$htTimeTotCalc</td></tr>";
	    $arCtrls['!TimeTotalRow'] = $htTimeTotRow;

	    // // -- TIME CALCULATIONS
	    // // ++ COST CALCULATIONS

	    $mnyCostLine = $this->GetFieldValue('CostLine');
	    if ($mnyCostLine == $dlrCalcCost) {
		// calculated line cost total matches recorded line cost total
		$htCostTotCalc = '&radic;';
	    } else {
		// recorded line cost total needs recalculating
		$htCostTotCalc = '&rarr; '.$dlrCalcCost;
		$needCalc = TRUE;
	    }

	    if ($needCalc) {
		$htLink = $this->SelfLink('update','update the line total',array('do'=>'update'));
		$htCostTotCalc .= " [$htLink]";
	    }

	    $htCostTot = is_null($mnyCostLine)?'-':('$'.$mnyCostLine);
	    $htCostTotRow = "\n<tr><td align=right><b>Total Cost</b>:</td><td>$htCostTot</td><td>$htCostTotCalc</td></tr>";
	    $arCtrls['!CostTotalRow'] = $htCostTotRow;

	    // // -- COST CALCULATIONS
	    // -- CALCULATIONS

	    $oTplt->SetVariableValues($arCtrls);
	    $out = $oTplt->Render();
	}
	return $out;
    }
    /*-----
      ACTION: Save the user's edits
      HISTORY:
	2011-02-17 Retired old custom code; now using objForm helper object
	2011-12-03 Adapted from VbzCart to WorkFerret's Invoice Line Record class.
	2011-12-27 Copying from Invoice Line Record class to Invoice Record class, replacing hand-built AdminSave().
	2013-06-16 Copying from Invoice Record class to WF Session Record class, but commenting out for now
		Retro note: presumably this refers to the old code. no longer needed.
	2013-10-23 This is needed in order to, you know, save sessions. It also needs
		to be public (but was private); fixed that.
	2015-06-18 AdminSave() split into EditSave_page(), EditSave_line(), and EditFinish_line().
    */
    public function EditSave_page() {
	$oForm = $this->PageForm();
	$out = $oForm->Save();
	return $out;
    }
    public function EditSave_line() {
	$oForm = $this->LineForm();
	$out = $oForm->Save();
	return $out;
    }
    /* 2019-02-23 this has been commented out for probably quite some time now
    public function EditFinish_line() {
	$oForm = $this->LineForm();
	//$oForm->SetRecordValue('WhenFinish',time());			// for now, must be in NATIVE format
	//$oForm->FieldObject('WhenFinish')->SetValue(time(),FALSE);	// FALSE: blank = don't write
	$oForm->GetStorageObject('WhenFinish')->SetValueWritable('NOW()');
	$out = $oForm->Save();
	return $out;
    }
    */

    // -- WEB INTERFACE: DISPLAY -- //
}
abstract class cSessionSummer {

    abstract public function GetNameString();
    abstract protected function GetColCount();
    
    private $nMin = 0;
    public function AddMinutes($nMin) {
	$this->nMin += $nMin;
    }
    public function GetMinutesTotal() {
	return $this->nMin;
    }
    protected function ClearMinutes() {
	$this->nMin = 0;
    }
    
    // TODO: This needs to be broken down by rate
    private $nCents = 0;
    public function AddCents($nCents) {
	$this->nCents += $nCents;
    }
    public function GetCentsTotal() {
	return $this->nCents;
    }
    public function ClearMoney() {
	$this->nCents = 0;
    }
    /*
    public function AddDollars($fDlrs) {
	echo "ADDING [$fDlrs]<br>";
	$nCents = round($fDlrs * 100);
	$this->nCents += $nCents;
    }
    public function GetDollarsTotal() {
	return ($this->nCents)/100;
    }
    public function ClearDollars() {
	$this->nCents = 0;
    }
    */
    
    
    public function RenderHeader($s) {
	$nCols = $this->GetColCount();
	$sName = $this->GetNameString();
	return "\n<tr class='header-$sName'><td colspan=$nCols><b>$s</b></td></tr>";
    }
    public function RenderSums() {
	$nMins = $this->GetMinutesTotal();
	$nCents = $this->GetCentsTotal();
	if (($nMins > 0) || ($nCents > 0)) {
	    $sName = $this->GetNameString();
	    //$nMins = $this->GetMinutesTotal();
	    $fHrs = $nMins / 60;
	    $sHrs = sprintf('%01.1f',$fHrs);
	    //$fDlrs = $this->GetDollarsTotal();
	    $sDlrs = sprintf('%01.2f',$nCents/100);
	    $nCols = $this->GetColCount();
	    $sApprox = ((float)$sHrs != $fHrs)?'~':'';
	    
	    // TODO: This needs to be broken down by rate
	    $out = "\n<tr class='header-$sName'><td colspan=$nCols><b>$sName totals</b>: $nMins minutes ($sApprox$sHrs hours) = \$$sDlrs</td></tr>";
	} else {
	    $out = NULL;
	}
	$this->ClearMinutes();
	$this->ClearMoney();
	return $out;
    }
}
class cSessionSummer_Year extends cSessionSummer {
    public function GetNameString() {
	return 'year';
    }
    protected function GetColCount() {
	return 12;
    }
}
class cSessionSummer_Month extends cSessionSummer {
    public function GetNameString() {
	return 'month';
    }
    protected function GetColCount() {
	return 11;
    }
}
class cSessionSummer_Day extends cSessionSummer {
    public function GetNameString() {
	return 'day';
    }
    protected function GetColCount() {
	return 10;
    }
}

class cSummerManager {
    
    private $arTrackers = array();
    public function AddTracker(cSessionSummer $oSum) {
	$sKey = $oSum->GetNameString();
	$this->arTrackers[$sKey] = $oSum;
    }
    protected function GetTracker($sKey) {
	return $this->arTrackers[$sKey];
    }
    public function AddSession(wfcSession $oSess) {
	foreach ($this->arTrackers as $sKey => $oSum) {
	    $nMinutes = $oSess->TimeFinal_minutes();
	    $ctsTotal = $oSess->GetFiguredCost_cents();
	    $oSum->AddMinutes($nMinutes);
	    $oSum->AddCents($ctsTotal);
	}
    }
    public function RenderSums() {
	$out = NULL;
	$arRev = array_reverse($this->arTrackers);
	foreach ($arRev as $sKey => $oSum) {
	    $out .= $oSum->RenderSums();
	}
	return $out;
    }
}
class cSummerManager_YMD extends cSummerManager {

    public function __construct() {
	$this->CreateTrackers();
    }

    protected function CreateTrackers() {
	$this->AddTracker(new cSessionSummer_Year());
	$this->AddTracker(new cSessionSummer_Month());
	$this->AddTracker(new cSessionSummer_Day());
    }

    protected function GetTracker_Year() {
	return $this->GetTracker('year');
    }
    protected function GetTracker_Month() {
	return $this->GetTracker('month');
    }
    protected function GetTracker_Day() {
	return $this->GetTracker('day');
    }
    
    private $yrLast = 0;
    private $moLast = 0;
    private $dyLast = 0;
    
    public function RenderHeaders($dt) {
	$out = NULL;
    
	$dyCur = date('Y.m.j',$dt);
	if ($this->dyLast != $dyCur) {
	    $oTotalDy = $this->GetTracker_Day();
	
	    $out .= $oTotalDy->RenderSums();
	    $this->dyLast = $dyCur;
	    
	    $moCur = date('Y.m',$dt);
	    if ($this->moLast != $moCur) {
		$oTotalMo = $this->GetTracker_Month();
		$out .= $oTotalMo->RenderSums();
		$this->moLast = $moCur;
		
		$yrCur = date('Y',$dt);
		if ($this->yrLast != $yrCur) {
		    $oTotalYr = $this->GetTracker_Year();
		    $out .= $oTotalYr->RenderSums();
		    $this->yrLast = $yrCur;
		    //$out .= "\n<tr style=\"background: #444466; color: #ffffff;\"><td colspan=12><b>$yrCur</b></td></tr>";
		    $out .= $oTotalYr->RenderHeader($yrCur);
		}
		
		$sMonth = date('F',$dt);
		$out .= $oTotalMo->RenderHeader($sMonth);
	    }
	    
	    $nDay = date('j',$dt);
	    $sSfx = date('S',$dt);
	    $sDOW = date('l',$dt);
	    $sDay = "$nDay<sup>$sSfx</sup> ($sDOW)";
	    $out .= $oTotalDy->RenderHeader($sDay);
	}
	return $out;
    }
}

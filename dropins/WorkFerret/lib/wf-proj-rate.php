<?php
/*
  PURPOSE: classes for managing Project-Rate mapping in WorkFerret
  HISTORY:
    2013-11-08 split off from WorkFerret.main
*/
class cwftProjs_x_Rates extends fcTable_wName_wSource_wRecords {
    use ftName_forTable;

    private $arRates;	// array of all rates
    private $arX;	// complete matrix of projs x rates
    private $arP_IDs;	// project IDs, sorted for display

    // ++ SETUP ++ //
    
    protected function InitVars() {
	parent::InitVars();
	$this->arX = NULL;
    }
    protected function TableName() {
	return 'wf_rate_proj';
    }
    protected function SingularName() {
	return 'cwfrProj_x_Rate';
    }
    
    // -- SETUP -- //
    // ++ TABLES ++ //

    protected function ProjectTable() {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_PROJECTS);
    }
    
    // -- TABLES -- //
    
    public function Load() {
	if (is_null($this->arX)) {
	    $rc = $this->SelectRecords(NULL,'ID_Proj,ID_Rate');
	    while ($rc->NextRow()) {
		$idProj = $rc->GetFieldValue('ID_Proj');
		$idRate = $rc->GetFieldValue('ID_Rate');
		$this->arX[$idProj][$idRate] = TRUE;
	    }
	}
    }
    public function Exists($iProj,$iRate) {
	if (is_null($this->arX)) {
	    return NULL;
	} else {
	    $arRates = fcArray::Nz($this->arX,$iProj);
	    return fcArray::Nz($arRates,$iRate);
	}
    }
    /*----
      RETURNS: array of all project data
      OUTPUT: RETURN and $arProjs
    */
    private $arProjs=NULL;	// array of all projects
    protected function ProjectArray() {
	if (is_null($this->arProjs)) {
	    $tblP = $this->ProjectTable();
	    $rc = $tblP->SelectRecords();
	    while ($rc->NextRow()) {
		$idProj = $rc->GetFieldValue('ID');
		$this->arProjs[$idProj] = $rc->GetFieldValues();
	    }
	}
	return $this->arProjs;
    }
    /*----
      RETURNS: array of all rate data
      OUTPUT: RETURN and $arRates
    */
    protected function Rates() {
	if (empty($this->arRates)) {
	    $tblR = $this->Engine()->Rates();
	    $rc = $tblR->GetData();
	    while ($rc->NextRow()) {
		$idRate = $rc->Value('ID');
		$this->arRates[$idRate] = $rc->Values();
	    }
	}
	return $this->arRates;
    }
    /*----
      RETURNS: list of project IDs, sorted for display purposes
      OUTPUT: RETURN and $arP_IDs
    */
    protected function ProjectIDArray() {
	if (empty($this->arP_IDs)) {
	    $rsP = $this->ProjectTable()->SelectRecords(NULL,'InvcPfx');
	    $idx = 0;
	    while ($rsP->NextRow()) {
		$this->arP_IDs[$idx] = $rsP->GetFieldValue('ID');
		$idx++;
	    }
	}
	return $this->arP_IDs;
    }
    public function RenderRate_Hdr() {
	$out = "\n<tr>\n"
	  .'<th>ID</th>'
	  .'<th colspan=2>Status</th>'
	//  .'<th>Project</th>'
	  .'<th>Name</th>'
	  .'<th>$/Hour</th>';

	// add a column for each project
	$arPS = $this->ProjectIDArray();
	$arPR = $this->ProjectArray();
	$rcP = $this->ProjectTable()->SpawnRecordset();
	if (count($arPS)) {
	    $out .= '<th>Projects:</th>';
	    foreach ($arPS as $idx => $idP) {
		$arP = $arPR[$idP];
		$rcP->SetFieldValues($arP);
		$out .= '<th>'.$rcP->SelfLink($rcP->GetInvoicePrefix(),$rcP->GetNameString()).'</th>';
	    }
	}

/* OLD VERSION
	$oPrjs = $this->Engine()->Projects()->GetData(NULL,NULL,'InvcPfx');
	if ($oPrjs->HasRows()) {
	    $out .= '<th>Projects:</th>';
	    while ($oPrjs->NextRow()) {
		$out .= '<th>'.$oPrjs->SelfLink($oPrjs->Value('InvcPfx'),$oPrjs->Value('Name')).'</th>';
	    }
	}
*/

	$out .= '</tr>';
	return $out;
    }
    public function RenderRate($iRate,$iDoAssign) {
	if ($iDoAssign) {
	    $sName = 'rxp[<$PROJ$>]['.$iRate.']';
	    $ftYes = '<td align=center><input type=checkbox checked name="'.$sName.'"></td>';
	    $ftNo = '<td align=center><input type=checkbox name="'.$sName.'"></td>';
	} else {
	    $ftYes = '<td align=center>+</td>';
	    $ftNo = '<td></td>';
	}

	$out = NULL;
	$arPS = $this->ProjectIDArray();
	foreach ($arPS as $idx => $idP) {
	    if ($this->Exists($idP,$iRate)) {
		$out .= str_replace('<$PROJ$>',$idP,$ftYes);
	    } else {
		$out .= str_replace('<$PROJ$>',$idP,$ftNo);
	    }
	}
	return $out;
    }
    /*----
      INPUT: iar = new assignment data to completely replace whatever was previously in the table
	Each element is in the format iar[project ID][rate ID] = (something)
    */
    public function Update_fromArray($iar) {
	$db = $this->GetConnection();
	//$db->Exec('START TRANSACTION;');
	$db->TransactionOpen();
	$db->ExecuteAction('DELETE FROM '.$this->TableName_Cooked().';');
	foreach ($iar as $idP => $arRates) {
	    foreach ($arRates as $idR => $dummy) {
		$this->Insert(array('ID_Proj'=>$idP,'ID_Rate'=>$idR));
	    }
	}
	//$db->Exec('COMMIT;');
	$db->TransactionSave();
    }
}
class cwfrProj_x_Rate extends fcDataRecord {

}

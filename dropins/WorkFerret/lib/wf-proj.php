<?php
/*
  PURPOSE: classes for managing Projects in WorkFerret
  HISTORY:
    2013-11-08 split off from WorkFerret.main
    2017-02-07 revamping as Ferreteria dropin
*/

class cwftProjects extends fcTable_keyed_single_standard implements fiLinkableTable, fiEventAware {
    use ftLinkableTable;
    use ftExecutableTwig;
    use ftLoggableTable;


    // ++ SETUP ++ //
    
    protected function SingularName() {
	return 'cwfrProject';
    }
    protected function TableName() {
	return 'wf_project';
    }
    public function GetActionKey() {
	return KS_ACTION_WORKFERRET_PROJECT;
    }
    
    // -- SETUP -- //
    // ++ EVENTS ++ //
  
    protected function OnCreateElements() {}
    protected function OnRunCalculations() {
	$oPage = fcApp::Me()->GetPageObject();
	$oPage->SetPageTitle('Projects');
	//$oPage->SetBrowserTitle('Suppliers (browser)');
	//$oPage->SetContentTitle('Suppliers (content)');
    }
    public function Render() {
	return $this->AdminPage();
    }
    /*
    public function MenuExec() {
	return $this->AdminPage();
    }*/

    // -- EVENTS -- //
    // ++ RECORDS ++ //

    public function RootNode() {
	$rcRoot = $this->SpawnRecordset();
	//$rcRoot->ClearValue('ID');
	return $rcRoot;
    }
    public function Records_forDropDown(array $arArgs=NULL) {
        $idAlways = fcArray::Nz($arArgs,'current');    // show specified project even if it's inactive
        $doInact = fcArray::Nz($arArgs,'inact',FALSE); // show inactive projects?
	if (!$doInact) {
	    $sqlFilt = 'isActive';
	    if (!is_null($idAlways)) {
		$sqlFilt .= ' OR (ID='.$idAlways.')';
	    }
	} else {
	    $sqlFilt = NULL;
	}
	$rs = $this->SelectRecords($sqlFilt);
	return $rs;
    }

    // -- RECORDS -- //
    // ++ WEB UI ++ //

    /*----
      HISTORY:
	2011-04-21 renamed from ListPage() -> AdminPage()
    */
    public function AdminPage() {
	//fcApp::Me()->GetKioskObject()->SetPagePath($this->SelfURL());
	$oPathIn = fcApp::Me()->GetKioskObject()->GetInputObject();
	$oFormIn = fcHTTP::Request();
	
	$sAction = $oPathIn->GetString('do');
	$doAdd = ($sAction == 'add');

	$doAddSave = $oFormIn->GetBool('btnSave');

	if ($doAddSave) {
	    $db = $this->GetConnection();
	    $strName = $oFormIn->GetString('name');
	    $strPfx = $oFormIn->GetString('pfx');
	    $arIns = array(
	      'ID_Parent'	=> 'NULL',
	      'Name'		=> $db->SanitizeValue($strName),
	      'InvcPfx'		=> $db->SanitizeValue($strPfx)
	      );
	    $this->Insert($arIns);
	    $idEv = $this->CreateEvent(KS_EVENT_NEW_RECORD,'new project',array('ins'=>$arIns));
	}

	$oMenu = fcApp::Me()->GetHeaderMenu();
	  /* 2017-02-07 adapted from elsewhere - in theory, this could work here
	  $oMenu->SetNode($ol = new fcMenuOptionLink(KS_NEW_REC,'id'));
	    $ol->SetBasePath($this->SelfURL());
	  */
	  // ($sGroupKey,$sKeyValue=TRUE,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
	  $oMenu->SetNode($ol = new fcMenuOptionLink('do','add',NULL,NULL,'add a new project'));
	    //$ol->SetBasePath($this->SelfURL());

	/* 2017-02-07 old MediaWiki code
	$objPage = new clsWikiFormatter($vgPage);
	$objSection = new clsWikiSection_std_page($objPage,'Projects',2);

	$objLink = $objSection->AddLink_local(new clsWikiSectionLink_option(array(),'add','do','add'));
	  $objLink->Popup('add a new project');

	$out = $objSection->Render();

	$wgOut->AddHTML($out);
	*/

	$out = "\n<table class=form-record><tr><td>"
	  .$this->RootNode()->DrawTree()
	  ."\n</td></tr></table>"
	  ;

	if ($doAdd) {
	    $out .= '<h3>new project</h3>'
	      .'<form method=post>'
	      .'Prefix: <input name=pfx size=3> Name: <input name=name size=10>'
	      .'<input type=submit name=btnSave value="Add"><input type=submit name=btnCancel value="Discard"></form>'
	      ;
	}
	return $out;
    }
    public function DrawTree() {
	throw new exception('2017-02-07 Why do we need this method?');
	$objRoot = $this->RootNode();
	$out = $objRoot->DrawTree();
	return $out;
    }
    public function DropDown(array $arArgs=NULL) {
        $rs = $this->Records_forDropDown($arArgs);
	return $rs->DropDown($arArgs);
    }

    // -- WEB UI -- //
}

/*
  HISTORY:
    2011-10-19 moved HasParent() from cwftProjects (was clsWFProjects) to clsWFProject
    2018-05-19 I mean, I've made hundreds of changes since 2011, but apparently it only now occurred to me
      to mention: added ftLoggableRecord because the CreateEvent() call needs it.
*/
class cwfrProject extends fcRecord_keyed_single_integer implements fiLinkableRecord, fiEventAware, fiEditableRecord {
    use ftLinkableRecord;
    use ftSaveableRecord;
    use ftExecutableTwig;
    use ftLoggableRecord;

    // ++ EVENTS ++ //
  
    protected function OnCreateElements() {}
    /*----
      NOTE: Most of this stuff doesn't actually *have* to be here, but the title calculations do, and it just
	made sense to put it all together here.
    */
    protected function OnRunCalculations() {
	$oPathIn = fcApp::Me()->GetKioskObject()->GetInputObject();
	$oFormIn = fcHTTP::Request();
	
	if ($oFormIn->GetBool('btnSaveProj')) {
	    $this->AdminSave($oFormIn->GetString('EvNotes'));
	}

	$sAction = $oPathIn->GetString('do');
	
	$doEdit = $oPathIn->GetBool('edit');
	$strCtxt = $oPathIn->GetString('context');
	
	// isn't this just the same as $this->IsNew()? TODO: Investigate...
	$doAdd = (($sAction == KS_NEW_REC) && (empty($strCtxt)));

	// do the header, with edit link if appropriate
	if ($doAdd) {
	    $doEdit = TRUE;	// new row must be edited before it can be created
	    $this->SetFieldValue('ID_Proj',$oPathIn->GetInt('proj'));
	    $sTitle = 'New project';
	    $id = NULL;
	} else {
	    $id = $this->GetKeyValue();
	    $sTitle = 'Project '.$id.' - '.$this->GetNameString();
	    
	    // "edit" menu only appears if we're not adding
	    $oMenu = fcApp::Me()->GetHeaderMenu();
	      // ($sKeyValue,$sGroupKey=NULL,$sDispOff=NULL,$sDispOn=NULL,$sPopup=NULL)
	      $oMenu->SetNode($ol = new fcMenuOptionLink('edit',TRUE,NULL,NULL,'edit this project'));
		//$ol->SetBasePath($this->SelfURL());
	}
//	$this->SetDoAdd($doAdd);
	$this->SetDoEdit($doEdit);
	
	$oPage = fcApp::Me()->GetPageObject();
	$oPage->SetPageTitle($sTitle);
	//$oPage->SetBrowserTitle('Suppliers (browser)');
	//$oPage->SetContentTitle('Suppliers (content)');
    }
    public function Render() {
	return $this->AdminPage();
    }
    
    // -- EVENTS -- //
    // ++ INTERNAL STATES ++ //

    private $doEdit;
    protected function SetDoEdit($b) {
	$this->doEdit = $b;
    }
    protected function GetDoEdit() {
	return $this->doEdit;
    }

    // -- INTERNAL STATES -- //
    // ++ FIELD VALUES ++ //

    protected function ParentID() {
        return $this->GetFieldValue('ID_Parent');
    }
    protected function RateID() {
	return $this->GetFieldValue('ID_Rate');
    }
    // PUBLIC because needed for Invoice report
    public function GetNameString() {
	return $this->GetFieldValue('Name');
    }
    public function IsActive() {
	if ($this->IsNew()) {
	    return TRUE;
	} else {
	    return $this->GetFieldValue('isActive');
	    /* 2017-02-07 apparently this is somehow magically already converted, so we don't need this code:
	    return fcFieldStorage_Bit::FromStorage($this->GetFieldValue('isActive'));
	    */
	}
    }
    // PUBLIC so Charge records can use it to generate page title
    public function GetInvoicePrefix() {
	return $this->GetFieldValue('InvcPfx');
    }

    // -- FIELD VALUES -- //
    // ++ FIELD CALCULATIONS ++ //

    // TRAIT HELPER
    public function SelfLink_name() {
	return $this->SelfLink($this->GetNameString());
    }
    public function HasParent() {
	return !is_null($this->ParentID());
    }
    public function HasRateDefault() {
	return !is_null($this->RateID());
    }
    public function NextSessSeq() {
	// When deleting this: delete commented code referring to it and make session->GetNextSeq() protected.
	throw new exception('2017-02-09 It looks like this is no longer needed.');
	$t = $this->SessionTable();
	return $t->GetNextSeq($this->GetKeyValue());
    }
    /*----
      RETURNS: text description of the default rate for this project
    */
    protected function RateDescr($htNone='<i>(none)</i>') {
	if ($this->HasRateDefault()) {
	    return $this->DefaultRateRecord()->Descr();
	} else {
	    return $htNone;
	}
    }
    // CALLBACK
    public function ListItem_Text() {
	return $this->GetNameString();
    }
    // CALLBACK from fcFormControl_HTML_DropDown::RenderValue()
    public function ListItem_Link() {
	return $this->SelfLink_name();
    }

    // -- FIELD CALCULATIONS -- //
    // ++ RECORDS ++ //

    public function ParentRecord() {
	$idParent = $this->ParentID();
	if (is_null($idParent)) {
	    return NULL;
	} else {
	    return $this->Table()->GetItem($idParent);
	}
    }
    /*----
      RETURNS: recordset of all possible parent records
	i.e. all active projects except this one
    */
    protected function ParentRecords() {
	$rs = $this->GetTableWrapper()->Records_forDropDown(
	  array(
	    'current'=>$this->GetKeyValue()
	    )
	  );
	return $rs;
    }
    public function Rates() {
        throw new exception('Rates() is deprecated; call RateRecords() instead.');
    }
    public function RateRecords() {
	if ($this->IsNew()) {
	    $rs = NULL;
	} else {
	    $tX = $this->RateTable();	// Projs X Rates table
	    $rs = $tX->Rates_forProj($this->GetKeyValue());
	}
	return $rs;
    }
    /*----
      RETURNS: Record for the default rate for this project
    */
    protected function DefaultRateRecord() {
	$idRate = $this->RateID();
	$rcRate = $this->RateTable($idRate);
	return $rcRate;
    }
    public function InvoiceRecords() {
	return $this->InvoiceTable()->SelectRecords('ID_Proj='.$this->GetKeyValue());
    }
    protected function ChildRecords() {
	$id = $this->GetKeyValue();
	if (is_null($id)) {
	    $sqlFilt = 'ID_Parent IS NULL';
	} else {
	    $sqlFilt = 'ID_Parent='.$id;
	}

	return $this->GetTableWrapper()->SelectRecords($sqlFilt,'Name');
    }

    // -- RECORDS -- //
    // ++ TABLES ++ //

    protected function RateTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_WORKFERRET_RATES,$id);
    }
    protected function SessionTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper('wfcSessions',$id);
    }
    protected function ChargeTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper('wftcCharges',$id);
    }
    protected function InvoiceTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper('wftcInvoices',$id);
    }

    // -- TABLES -- //
    // ++ UI: ACTIONS ++ //

    /*-----
      ACTION: Save the user's edits to the record
    */
    private function AdminSave($iNotes) {
	$oForm = $this->PageForm();
	
	$arData = array(
	  'old'	=> $this->GetFieldValues(),
	  'new' => $oForm->GetFieldArray_toWrite_native(),
	  );
	$idEv = $this->CreateEvent(KS_EVENT_CHANGE_RECORD,'edited project',$arData);
	$this->CreateEvent_Notes($idEv,$iNotes);
	
	//$oForm->ClearValues();
	$oForm->Save();
	$sMsg = $oForm->MessagesString();
	$this->SelfRedirect(array(),$sMsg);	// clear the form data out of the page reload
    }

    // -- UI: ACTIONS -- //
    // ++ UI: CONTROLS ++ //

    public function Rates_DropDown(array $iarArgs=NULL) {
	$rs = $this->RateRecords();
	return $rs->DropDown($iarArgs);
    }
    /*----
      VERSION: straight HTML ul-list
    */
    public function DrawTree($nLevel=0) {
	$rs = $this->ChildRecords();
	if ($rs->HasRows()) {
	    $sIndent = str_repeat("\t",$nLevel);		// make the raw HTML more readable
	    $out = "\n$sIndent<ul>";
	    $nLevel++;
	    $sIndent = str_repeat("\t",$nLevel);		// make the raw HTML more readable
	    while ($rs->NextRow()) {
		$sName = $rs->GetNameString();
		$css = $rs->IsActive()?'state-active':'state-inactive';
		$sTwig = "<span class=$css>$sName</span>";	// italicize if inactive
		$ftTwig = $rs->SelfLink($sTwig,'edit '.$sTwig);
		$out .=
		  "\n<li>$sIndent$ftTwig</li>"
		  .$rs->DrawTree($nLevel);
	    }
	    $nLevel--;
	    $sIndent = str_repeat("\t",$nLevel);		// make the raw HTML more readable
	    $out .= "\n$sIndent</ul>";
	 } else {
	    if ($nLevel == 0) {
		$out = "\nNo projects created yet.";
	    } else {
		$out = '';
	    }
	 }
	 return $out;
    }
    public function DropDown(array $iArArgs=NULL) {
	$strName = nz($iArArgs['name'],'proj');
	$strNone = nz($iArArgs['none'],'none found');
	$useNone = isset($iArArgs['incl none']);
	$intDeflt = (int)nz($iArArgs['default'],0);

	if ($this->hasRows()) {
	    $out = "\n".'<select name="'.$strName.'">';
	    if ($useNone) {
		$htSelect = empty($intDeflt)?' selected':'';
		$out .= "\n  <option$htSelect value=0>- global -</option>\n";
	    }

	    while ($this->NextRow()) {
		$id = $this->KeyValue();

		if ($id == $intDeflt) {
		    $htSelect = " selected";
		} else {
		    $htSelect = '';
		}
		$strName = '';
		if ($this->HasParent()) {
		    $strName = $this->ParentRecord()->Value('Name').'.';
		}
		$strName .= $this->Value('Name');
		if (!$this->IsActive()) {
		    $strName = '('.$strName.')';	// indicate inactive projects
		}
		$out .= "\n".'  <option'.$htSelect.' value="'.$id.'">'.$strName."</option>\n";
	    }
	    $out .= "\n</select>\n";
	    return $out;
	} else {
	    return $strNone;	// only one item in list, so no choice to make
	}
    }

    // -- UI: CONTROLS -- //
    // ++ UI: DISPLAY ++ //

    public function AdminPage() {
	$doEdit = $this->GetDoEdit();
	
	$frmEdit = $this->PageForm();
	if ($this->IsNew()) {
	    $frmEdit->ClearValues();
	} else {
	    $frmEdit->LoadRecord();
	}
	$oTplt = $this->PageTemplate();
	$arCtrls = $frmEdit->RenderControls($doEdit);

	/*
	if ($this->HasParent()) {
	    $rcParent = $this->ParentRecord();
	    $htParent = $rcParent->SelfLink($rcParent->GetNameString());
	} else {
	    $htParent = '<i>none</i>';
	}
	*/
	if ($this->IsNew()) {
	    $htNextInvc = NULL;
	} else {
	    $nNextInvc = $this->InvoiceTable()->NextInvoiceSeq($this->GetKeyValue());
	    $htNextInvc = "<tr><td align=right><b>Invc seq</b>:</td><td>$nNextInvc</td></tr>";
	}

	$arCtrls['ID'] = $this->SelfLink();
//	$arCtrls['ID_Parent'] = $htParent;
	$arCtrls['!NextInvc'] = $htNextInvc;
 	$oTplt->SetVariableValues($arCtrls);
	
	$htRec = NULL;
	
	if ($doEdit) {
	    $htRec .= "\n<form method=post>";
	}
	$htRec .= $oTplt->Render();
	if ($doEdit) {
	    $htRec .= 
	      "\n<input type=submit name=btnSaveProj value=Save>"
	      ."\n</form>"
	      ;
	}

	$out = $htRec
	    // sessions for this project
	    .$this->ListSessions()
	    // charges for this project
	    .$this->ListCharges()
	    // invoices for this project
	    .$this->ListInvoices()
	    ;
	return $out;
    }
    /*----
      HISTORY:
	2017-03-09 Re-inserted $this->HandleSessForm(). Not sure if this is the right place for it.
	2017-04-12 Turns out that the show-invoice flag is not used here anymore. It's retrieved where it is needed.
    */
    public function ListSessions() {
	$out = $this->HandleSessForm();
	$t = $this->SessionTable();
	$idProj = $this->GetKeyValue();
	$out .= $t->AdminRows_forProject($idProj); 
	return $out;
    
    }
    public function ListCharges() {
	$t = $this->ChargeTable();
	$idProj = $this->GetKeyValue();
	return $t->AdminRows_forProject($idProj);
    }
    public function ListInvoices() {
	$t = $this->InvoiceTable();
	$idProj = $this->GetKeyValue();
	return $t->AdminRows_forProject($idProj);
    }

    // -- UI: DISPLAY -- //
    // ++ UI: FORM CREATION ++ //

    /*----
      HISTORY:
	2011-03-29 adapted from vcrAdminPackage (formerly clsPackage) to VbzAdminStkItem
	2013-06-08 adapted from VbzCart:VbzAdminStkItem to WorkFerret:cwfrProject
    */
    private $frmPage;
    private function PageForm() {
	if (empty($this->frmPage)) {
	    $oForm = new fcForm_DB($this);

	      $oField = new fcFormField_Text($oForm,'Name');
		$oCtrl = new fcFormControl_HTML($oField,array('size'=>20));

              $oField = new fcFormField_Num($oForm,'ID_Parent');
		$oCtrl = new fcFormControl_HTML_DropDown($oField,array());
		  $oCtrl->SetRecords($this->ParentRecords());
		    $oCtrl->AddChoice(NULL,'(root)');

              $oField = new fcFormField_Num($oForm,'ID_Rate');
		$oCtrl = new fcFormControl_HTML_DropDown($oField,array());
		  $oCtrl->SetRecords($this->RateRecords());
		  $oCtrl->NoDataString('none available');

              $oField = new fcFormField_Text($oForm,'InvcPfx');
		$oCtrl = new fcFormControl_HTML($oField,array('size'=>4));

	      $oField = new fcFormField_Bit($oForm,'isActive');
		$oCtrl = new fcFormControl_HTML_CheckBox($oField,array());
		  $oCtrl->DisplayStrings(
		    '<font color=green>active</font>',
		    '<font color=grey>inactive</font>'
		    );

              $oField = new fcFormField_Text($oForm,'Notes');
		$oCtrl = new fcFormControl_HTML_TextArea($oField,array('rows'=>4,'cols'=>60));

	    $this->frmPage = $oForm;
	}
	return $this->frmPage;

    }
    private $tpPage;
    protected function PageTemplate() {
	if (empty($this->tpPage)) {
	    $sTplt = <<<__END__
<table class=form-record>
  <tr><td class='mw-label'><b>ID</b>:</td><td>[#ID#] [#isActive#]</td></tr>
  <tr><td class='mw-label'><b>Name</b>:</td><td>[#Name#]</td></tr>
  <tr><td class='mw-label'><b>Default Rate</b>:</td><td>[#ID_Rate#]</td></tr>
  <tr><td class='mw-label'><b>Invc Prefix</b>:</td><td>[#InvcPfx#]</td></tr>
  [#!NextInvc#]
  <tr><td class='mw-label'><b>Parent</b>:</td><td>[#ID_Parent#]</td></tr>
  <tr><td class='mw-label'><b>Project Notes</b>:</td><td>[#Notes#]</td></tr>
</table>
__END__;
	    $this->tpPage = new fcTemplate_array('[#','#]',$sTplt);
	}
	return $this->tpPage;
    }

    // -- UI: FORM CREATION -- //
    // ++ UI: FORM PROCESSING ++ //

    /*-----
      ACTION: Handles input from session-related form submissions
      HISTORY:
	2011-11-22 commented out check for context == 'session'; let's make the action
	  entirely dependent on the name of the button (plus hidden field values if needed)
	2017-02-14 removed more unneeded "context" code
    */
    public function HandleSessForm() {
	$oPathIn = fcApp::Me()->GetKioskObject()->GetInputObject();
	$oFormIn = fcHTTP::Request();

	$tSess = $this->SessionTable();
	$nChg = 0;

	$rsSess = $tSess->SpawnRecordset();
	$rsSess->SetProjectID($this->GetKeyValue());

	$out = NULL;
	
	if ($oFormIn->getBool('btnSaveSess')) {

	    // SAVE NEW SESSION

	    $rsSess->EditSave_line();
	    $this->SelfRedirect();
	}

	if ($oFormIn->getBool('btnFinishSess')) {

	    // FINISH NEW SESSION (save with end at current time)

	    $rsSess->EditFinish_line();
	    $this->SelfRedirect();
	}

	if ($oFormIn->getBool('btnInvc')) {

	    // RENDER LIST OF SESSIONS TO SELECT for including on invoice

	    $argSess = $oFormIn->getArray('sess');	// get list of sessions selected
	    if (is_array($argSess)) {
	    
	    /* 2017-02-07 MediaWiki code
		$objPage = new clsWikiFormatter($vgPage);
		//$objSection = new clsWikiSection($objPage,'Invoice','create or modify invoice',3);
		$oSection = new clsWikiSection_std_page($objPage,'Add to Invoice',3);
	    */
		$oHdr = new fcSectionHeader('Add Sesions to Invoice');

		$out = "\n<form method=post>"
		  .'<table style="border: thin solid black;"><tr><td>'
		  .$oHdr->Render()
		  ;

		$arBoxAttr = array('checked' => TRUE);	// set all sessions checked by default

		// show check box for each session; also calculate what the balance should be:
		$intTotal = 0;
		$intBalLast = NULL;
		$htChecks = '';

		// This builds the preliminary list of sessions for the user to approve
		foreach ($argSess as $idSess=>$val) {
		// for each session to be added...
// TO DO: make sure there are no gaps by checking the Seq field
		    $rcSess = $this->SessionTable($idSess);
		    $htChecks .= ' '.$rcSess->CheckBox('SessAdd',$arBoxAttr).$idSess."\n";

// TO DO: get rid of code for handling balance-only sessions
		    // add to balance and
		    $intTotalPenult = $intTotal;
		    $intTotal += (int)($rcSess->GetFieldValue('CostLine') * 100);
		    $intBalPenult = $intBalLast;	// penultimate balance is the one which should be on the invoice
		    $intBalLast = (int)($rcSess->GetFieldValue('CostBal') * 100);
		}
		$out .= "\n<table>"
		  ."\n<tr><td align=right><b>Sessions to add</b>:</td><td>\n$htChecks</td></tr>"
		  ."\n</table>";
		$rsInvcs = $this->InvoiceTable()->GetUnsent($this->GetKeyValue());
		if ($rsInvcs->hasRows()) {
		    $htCtrl = $rsInvcs->DropDown(array('ctrl.name'=>'invoice_id'));
		    $htInvcs = '<input type=radio name=addTo value="old">Add to: '.$htCtrl;
		} else {
		    $htInvcs = '<i>No unsent invoices available</i>';
		}
		$out .= "\n<ul>";
		  $out .= "\n".'<li><input type=radio name=addTo value="new" checked>Create new invoice</li>';
		  $out .= "\n<li>$htInvcs</li>";
		$out .= "\n</ul>";
		$out .= '<br>Notes for invoice:<br><textarea name="notes" rows=3 cols=30></textarea>';
		$out .= '<input type=submit name="btnAddSess" value="Add Sessions">';
		$out .= '</td></tr></table></form>';
	    } else {
		$out = 'Please select sessions to be added to the invoice.';
	    }
	} elseif ($oFormIn->getBool('btnAddSess')) {

	    // ADD THE SELECTED SESSIONS TO AN INVOICE

	    $arSess = $oFormIn->getArray('SessAdd');	// get list of sessions to add
	    $strActCode = $oFormIn->getString('addTo');
	    switch ($strActCode) {
	      case 'new':
		// create the invoice object
		$sNotes = $oFormIn->getString('notes');
		$tInvc = $this->InvoiceTable();
		$dlrTotal = $this->SessionTable()->FigureTotalForSessionList(array_keys($arSess));
 		$id = $tInvc->Create($this->GetKeyValue(),$dlrTotal,$sNotes);
 		if ($id === FALSE) {
		    echo 'Insert failed: <b>'
		      .$this->GetConnection()->ErrorString()
		      .'</b><br><b>SQL</b>: '
		      .$tInvc->sql
		      .'<br>'
		      ;
		}
		$strAction = 'adding sessions to new invoice';
		break;
	      case 'old':
		// get the ID of the invoice to use
		$id = $oFormIn->getInt('invoice_id');
		$strAction = 'adding sessions to existing invoice';
		break;
	    }
	    if (empty($id)) {
		echo 'REQUEST:'.fcArray::Render($_REQUEST);
		throw new exception("Ferreteria internal error: no invoice ID when $strAction.");
	    }
	    $rcInvc = $this->InvoiceTable($id);	// get the target invoice
	    $sInvcNum = $rcInvc->GetFieldValue('InvcNum');
	    $out = $strAction
	      .' #'.
	      $rcInvc->SelfLink($sInvcNum)
	      ;

	    $idSessInvc = $oFormIn->GetInt('SessInv');
	    $rcInvc->AddLines($arSess,$idSessInvc);
	}
	return $out;
    }

    // -- UI: FORM PROCESSING -- //

}

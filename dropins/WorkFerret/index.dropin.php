<?php
/*
PURPOSE: define locations for libraries using modloader.php
FILE SET: WorkFerret libraries
HISTORY:
  2013-10-31 created
  2017-02-07 adapting for Greenmine
  2020-03-10 updated for latest Ferreteria
*/

// ACTIONS

define('KS_ACTION_WORKFERRET_PROJECT','wfproj');
define('KS_ACTION_WORKFERRET_RATE','wfrate');
define('KS_ACTION_WORKFERRET_INVOICE','wfinvc');
define('KS_ACTION_WORKFERRET_INVOICE_LINE','wfivl');
define('KS_ACTION_WORKFERRET_SESSION','wfsess');
define('KS_ACTION_WORKFERRET_CHARGE','wfchg');

// CLASS NAMES

define('KS_CLASS_WORKFERRET_PROJECTS','cwftProjects');
define('KS_CLASS_WORKFERRET_PROJECTS_X_RATES','cwftProjs_x_Rates');
define('KS_CLASS_WORKFERRET_RATES','cwftRates');
define('KS_CLASS_WORKFERRET_INVOICES','wftcInvoices');
define('KS_CLASS_WORKFERRET_INVOICE_LINES','wfctInvcLines');
define('KS_CLASS_WORKFERRET_SESSIONS','wfcSessions');
define('KS_CLASS_WORKFERRET_CHARGES','wftcCharges');

// PERMITS
define('KS_PERMIT_WORKFERRET_MANAGE_PROJECTS','workferret.proj.manage');

$arActions = array(
  KS_ACTION_WORKFERRET_PROJECT		=> array(
    'class'	=> KS_CLASS_WORKFERRET_PROJECTS,
    'text'	=> 'Projects',
    'summary'	=> 'manage project definitions',
    'perms'	=> KS_PERMIT_WORKFERRET_MANAGE_PROJECTS
    ),
  KS_ACTION_WORKFERRET_RATE		=> array(
    'class'	=> KS_CLASS_WORKFERRET_RATES,
    'text'	=> 'Rates',
    'summary'	=> 'manage billing rates',
    'perms'	=> KS_PERMIT_WORKFERRET_MANAGE_PROJECTS
    ),
  KS_ACTION_WORKFERRET_INVOICE		=> array(
    'class'	=> KS_CLASS_WORKFERRET_INVOICES,
    'text'	=> 'Invoices',
    'summary'	=> 'do stuff with invoices, duh',
    'perms'	=> KS_PERMIT_WORKFERRET_MANAGE_PROJECTS
    ),
  KS_ACTION_WORKFERRET_INVOICE_LINE		=> array(
    'class'	=> KS_CLASS_WORKFERRET_INVOICE_LINES,
    'text'	=> 'Invoice Lines',
    ##'summary'	=> 'do stuff with invoice lines',
    'perms'	=> KS_PERMIT_WORKFERRET_MANAGE_PROJECTS
    ),
  KS_ACTION_WORKFERRET_SESSION		=> array(
    'class'	=> KS_CLASS_WORKFERRET_SESSIONS,
    'text'	=> 'Sessions',
    'summary'	=> 'create/edit/invoice work sessions',
    'perms'	=> KS_PERMIT_WORKFERRET_MANAGE_PROJECTS
    ),
  KS_ACTION_WORKFERRET_CHARGE		=> array(
    'class'	=> KS_CLASS_WORKFERRET_CHARGES,
    'text'	=> 'Charges',
    'summary'	=> 'manage billing charges',
    'perms'	=> KS_PERMIT_WORKFERRET_MANAGE_PROJECTS
    ),
);
  
/* 2020-03-10 old dropin style
$om = $oRoot->SetNode(new fcMenuFolder('WorkFerret','WorkFerret','billable hours management'));

  $omi = $om->SetNode(new fcDropinLink(
    KS_ACTION_WORKFERRET_PROJECT,
    KS_CLASS_WORKFERRET_PROJECTS,
    'Projects','manage project definitions'));

    $omi->SetRequiredPrivilege(KS_PERMIT_WORKFERRET_MANAGE_PROJECTS);
    
  $omi = $om->SetNode(new fcDropinLink(
    KS_ACTION_WORKFERRET_RATE,
    KS_CLASS_WORKFERRET_RATES,
    'Rates','manage billing rates'));

    $omi->SetPageTitle('Billing Rates');
    $omi->SetRequiredPrivilege(KS_PERMIT_WORKFERRET_MANAGE_PROJECTS); // security kluge

  $omi = $om->SetNode(new fcDropinLink(
    KS_ACTION_WORKFERRET_INVOICE,
    KS_CLASS_WORKFERRET_INVOICES,
    'Invoices','do stuff with invoices, duh'));

    $omi->SetRequiredPrivilege(KS_PERMIT_WORKFERRET_MANAGE_PROJECTS); // security kluge
    
  $omi = $om->SetNode(new fcDropinLink(
    KS_ACTION_WORKFERRET_INVOICE_LINE,
    KS_CLASS_WORKFERRET_INVOICE_LINES,
    'Invoice Lines'));

    $omi->SetRequiredPrivilege(KS_PERMIT_WORKFERRET_MANAGE_PROJECTS); // security kluge

  $omi = $om->SetNode(new fcDropinLink(
    KS_ACTION_WORKFERRET_SESSION,
    KS_CLASS_WORKFERRET_SESSIONS,
    'Sessions','create/edit/invoice work sessions'));

    $omi->SetRequiredPrivilege(KS_PERMIT_WORKFERRET_MANAGE_PROJECTS); // security kluge
    
  $omi = $om->SetNode(new fcDropinLink(
    KS_ACTION_WORKFERRET_CHARGE,
    KS_CLASS_WORKFERRET_CHARGES,
    'Charges','manage billing charges'));

    $omi->SetRequiredPrivilege(KS_PERMIT_WORKFERRET_MANAGE_PROJECTS); // security kluge
*/
$arDropin = array(
  'name'	=> 'WorkFerret',
  'descr'	=> 'work logging and billing',
  'version'	=> '0.30',
  'date'	=> '2020-03-10',
  'URL'		=> 'https://htyp.org/WorkFerret',
  'section'	=> array(
    'title'		=> 'WorkFerret',
    'actions'		=> $arActions,
    ),
  'classes'	=> array(	// list of files and the classes they contain
    'lib/wf-charge.php'		=> array(KS_CLASS_WORKFERRET_CHARGES),
    'lib/wf-charge-form.php'	=> array('wfcForm_Charge_page','wfcForm_Charge_line'),
    'lib/wf-invc.php'		=> array(KS_CLASS_WORKFERRET_INVOICES),
    'lib/wf-invc-line.php'	=> array(KS_CLASS_WORKFERRET_INVOICE_LINES),
    'lib/wf-proj.php'		=> array(KS_CLASS_WORKFERRET_PROJECTS),
    'lib/wf-proj-rate.php'	=> array(KS_CLASS_WORKFERRET_PROJECTS_X_RATES),
    'lib/wf-rate.php'		=> array(KS_CLASS_WORKFERRET_RATES),
    'lib/wf-sess.php'		=> array(KS_CLASS_WORKFERRET_SESSIONS),
    'lib/wf-sess-calc.php'	=> array('SessionRecordCalc'),
    'lib/wf-sess-form.php'	=> array('wfcForm_Session_page','wfcForm_Session_line'),
     ),
  );

<?php
/*
  PURPOSE: Experimental interface to Friendica data
  HISTORY:
    2017-04-08 started in a fit of pique and whimsy
      The problem is going to be providing proper support for multiple databases. This is partly implemented, but needs work.
*/
// ACTIONS

define('KS_ACTION_FERRETIKA_CONTACT','fkcontact');

// PERMITS

define('KS_PERMIT_FERRETIKA_MANAGE_DATA','ferretika.data.manage');

$om = NULL;	// TODO

$arDropin = array(
  'name'	=> 'Friendica',
  'descr'	=> 'alternative interface to Friendica data',
  'version'	=> '0.0',
  'date'	=> '2018-03-13',
  'URL'		=> NULL,	// nothing yet
  'classes'	=> array(	// list of files and the classes they contain
  ),
  'menu'	=> $om,
  'features'	=> array(
    ),
  );

<?php namespace greenmine\FinanceFerret;
/*
  PURPOSE: Account classes for FinanceFerret
  HISTORY:
    2015-05-08 split off from FinanceFerret.php
*/

class ctAccts extends \ferreteria\data\cStandardTableSingleKey implements \fiLinkableTable, \fiEventAware {
    use \ftLinkableTable;
    use \ftExecutableTwig;
    #use \ftLoggableTable;

    // ++ SETUP ++ //
    
    protected function SingleRowClass() : string { return __NAMESPACE__.'\\crAcct'; }
    protected function GetTableName() : string { return 'Accounts'; }
    public function GetActionKey() : string { return KS_ACTION_FINFER_ACCT; }

    // -- SETUP -- //
    // ++ EVENTS ++ //
  
    protected function OnCreateElements() {}
    protected function OnRunCalculations() {
        $oPage = \fcApp::Me()->GetPageObject();
        $oPage->SetPageTitle('Money Accounts');
        //$oPage->SetBrowserTitle('Suppliers (browser)');
        //$oPage->SetContentTitle('Suppliers (content)');
    }
    public function Render() : string { return $this->AdminPage(); }

    // -- EVENTS -- //
    // ++ STATES ++ //

    private $doAll=FALSE;
    private $arOpen=NULL;
    protected function GetNodeOpenArray() : array {
        if (is_null($this->arOpen)) {
            $oPathIn = \fcApp::Me()->GetKioskObject()->GetInputObject();
            $sOpen = $oPathIn->GetString('nodes');
            if (strlen($sOpen) > 1) {
                $this->arOpen = array_flip(\fcString::Xplode($sOpen));
            } else {
                $this->arOpen = array();
            }
        }
        return $this->arOpen;
    }
    public function IsNodeOpen($id) {
        if ($this->doAll) {
            return TRUE;
        } else {
            $ar = $this->GetNodeOpenArray();
            return array_key_exists($id,$ar);
        }
    }
    public function GetNodeListString($idChg) {
        if (empty($idChg)) {
            throw new \Exception('FinanceFerret usage error: ID received is empty.');
        }
        $doAdd = TRUE;
        $s = NULL;
        $arOpen = $this->GetNodeOpenArray();
        foreach ($arOpen as $id => $dummy) {
            if ($id == $idChg) {
                $doAdd = FALSE;
            } else {
                $s .= '.'.$id;
            }
        }
        if ($doAdd) {
            $s .= '.'.$idChg;
        }
        return $s;
    }

    // -- STATES -- //
    // ++ LOOKUP ++ //
    
    public function GetAccountName($idAcct, $sNotFound = '(no record)') {
        $rcAcct = $this->GetRecord_forKey($idAcct);
        if ($rcAcct->HasRows()) {
            $sAcct = $rcAcct->GetNameString();
        } else {
            $sAcct = $sNotFound;
        }
        return $sAcct;
    }
    
    // -- LOOKUP -- //
    // ++ RECORDS ++ //

    protected function GetRootNode() { return $this->SpawnRecordset(); }
    /*----
      PUBLIC so Transactions register can access it
      HISTORY:
        2018-05-06
          made public, renamed, rewrote to generate SQL directly instead of using stored query
          also, I don't understand why I was doing the CONCAT_WS() thing; removing for now
            TODO I was probably trying to return ID_Alias instead of ID where ID_Alias isn't NULL
        2018-05-12 renamed from GetRecordset_forDropDown() to GetRecords_forDropDown()
    */
//    public function GetRecordset_forDropDown() {
    public function GetRecords_forDropDown() {
        //$sqlFields = "CONCAT_WS('.',ID_Alias,ID) AS ID, Name";
        $sqlFields = "ID, Name";
        $sqlSource = $this->SourceString_forSelect();
        $sql = "SELECT $sqlFields FROM $sqlSource ORDER BY Name";
        $rs = $this->FetchRecords($sql);
        return $rs;
    }
    // PROBABLY OBSOLETE
    /*
    public function DataSet_DropDown() {
	return $this->DataSQL('SELECT * FROM qryCbx_Accts;','ffcAcctListLine');
    }*/
    /*-----
      ACTION: Find an account or accounts which match the given text
      RETURNS: recordset of all matching accounts
      FUTURE: Should we search descriptions if no match found in Name?
    */
    public function Search($iText) {
        throw new \exception('2018-05-09 If anyone is still calling this, it will need fixing.');
        $sqlText = strtoupper($this->Engine()->SafeParam($iText));
        // first, look for an exact match:
        $objRows = $this->GetData('UPPER(Name)="'.$sqlText.'"');
        if ($objRows->RowCount() == 0) {
            // next, look for acct names starting with the given text:
            $objRows = $this->GetData('UPPER(Name) LIKE "'.$sqlText.'%"');
            if ($objRows->RowCount() == 0) {
            // if no matches, look for acct names *containing* the given text:
            $objRows = $this->GetData('UPPER(Name) LIKE "%'.$sqlText.'%"');
            }
        }
        return $objRows;
    }

    // -- RECORDS -- //
    // ++ WEB UI ++ //

    public function AdminPage() {
        $oApp = \fcApp::Me();
        
        $oPathIn = $oApp->GetKioskObject()->GetInputObject();
        $oFormIn = \fcHTTP::Request();
        
        $oMenu = $oApp->GetHeaderMenu();
          $oMenu->SetNode($ol = new \fcMenuOptionLink('do','add',NULL,NULL,'add a new money account'));
            $doAdd = $ol->GetIsSelected();

        $out = NULL;
            
        if ($doAdd) {
            $rcNew = $this->SpawnRecordset();
            $out = $rcNew->AdminPage();
        }
            
        $out .= "\n<table class=form-tree><tr><td>"
          .$this->GetRootNode()->DrawTree()
          ."\n</td></tr></table>"
          ;
        
        // TODO writing here

        return $out;
    }
    
    // -- WEB UI -- //

}

class crAcct extends \ferreteria\data\cRecordDeluxe implements \fiLinkableRecord, \fiEventAware {
    use \ftLinkableRecord;
    use \ftExecutableTwig;
    #use \ftSaveableRecord;

    // TODO: refactor these
    private $strPagePfx;	// used in EnterRegister()
    private $strPageSfx;	// used in EnterRegister()

    // ++ EVENTS ++ //

    protected function OnCreateElements() {}
    /*----
      NOTE: Most of this stuff doesn't actually *have* to be here, but the title calculations do, and it just
	made sense to put it all together here.
    */
    protected function OnRunCalculations() {
        $oPage = \fcApp::Me()->GetPageObject();
        if ($this->RowCount() > 0) {
            $sName = $this->GetNameString();
            $id = $this->GetKeyValue();
    
            $oPage->SetBrowserTitle("ac$id $sName");
            $oPage->SetContentTitle("Account #$id: $sName");
        } else {
            $oPage->SetBrowserTitle("acct n/a");
            $oPage->SetContentTitle("Account Record Not Found");
        }
    }
    public function Render() { return $this->AdminPage(); }
    
    // -- EVENTS -- //
    // ++ STATES ++ //
    
    // 2018-05-15 There's probably a better way to do this.
    private $arPage;
    protected function SetPageState(array $ar) { $this->arPage = $ar; }
    protected function GetPageState() { return $this->arPage; }
    
    protected function IsNodeOpen() {
        if ($this->IsRoot()) {
            return TRUE;	// root node is always open
        } else {
            return $this->GetTableWrapper()->IsNodeOpen($this->GetKeyValue());
        }
    }
    protected function GetNodeListString() {
        return $this->GetTableWrapper()->GetNodeListString($this->GetKeyValue());
    }
    protected function GetNodeChangeURL() {
        $sNodes = $this->GetNodeListString();
        if (strlen($sNodes) > 0) {
            $ar = array('nodes' => $sNodes);
        } else {
            $ar = NULL;
        }
        $url = $this->GetTableWrapper()->SelfURL($ar);
        return $url;
    }
        
    // -- STATES -- //
    // ++ FIELD VALUES ++ //

    // PUBLIC for Transaction.GetSummaryString()
    public function GetNameString() { return $this->GetFieldValue('Name'); }
    // TODO: make this an actual field
    protected function GetIsActive() { return TRUE; }
    
    // 2018-04-02 these won't work anymore
    protected function TypeID() { return $this->Value('ID_Type'); }
    protected function Name() {
	throw new exception('2018-04-03 Call GetNameString() instead.');
    }

    // -- FIELD VALUES -- //
    // ++ FIELD CALCULATIONS ++ //

    public function ListItem_Link() { return $this->SelfLink($this->ListItem_Text()); }
    public function ListItem_Text() { return $this->GetNameString(); }
    // (klugey?) root does not exist in the db, so no ID = ID is NULL = IsNew() = IsRoot()
    public function IsRoot() { return $this->IsNew(); }

    // -- FIELD CALCULATIONS -- //
    // ++ TABLES ++ //

    protected function OwnerTable() {
        return $this->GetDatabase()->MakeTableWrapper(KS_CLASS_FINFER_OWNERS);
    }
    protected function AccountTypeTable() {
        return $this->GetDatabase()->MakeTableWrapper(KS_CLASS_FINFER_ACCT_TYPES);
    }
    protected function TransactionTable() {
        return $this->GetDatabase()->MakeTableWrapper(KS_CLASS_FINFER_TRANSACTS);
    }
    protected function TransactionTypeTable() {
        return $this->GetDatabase()->MakeTableWrapper();
    }
    
    // -- TABLES -- //
    // ++ RECORDS ++ //

    /*----
      RETURNS: recordset containing all active account records except the current one
      TODO: filter out inactive accounts (unless current account is also inactive)
    */
    protected function GetParentRecords() {
        throw new \exception('2018-05-12 If anything is using this, it will need rewriting.');
        $id = $this->KeyValue();
        $rs = $this->Table()->GetData("ID != $id");
        return $rs;
    }
    protected function GetChildRecords() {
        $id = $this->GetKeyValue();
        if (is_null($id)) {
            $sqlFilt = 'ID_Parent IS NULL';
        } else {
            $sqlFilt = 'ID_Parent='.$id;
        }
        return $this->GetTableWrapper()->SelectRecords($sqlFilt,'Name');
    }

    private $rcType=NULL;	// account type datarecord
    protected function TypeRecord() {
        if (is_null($this->rcType)) {
            $tbl = $this->Engine()->AcctTypes();
            $rc = $tbl->GetItem($this->TypeID());
            $this->rcType = $rc;
        }
        return $this->rcType;
    }
    protected function TxTypeRecords() { return $this->TypeRecord()->TxTypes(); }
    /*-----
      ACTION: Returns a recordset of all transactions originating with this account
    */
    public function TrxactsSrce() {
        throw new \exception('2018-04-02 Who calls this?');
        return $this->Engine()->Trxacts()->GetData('ID_Acct='.$this->KeyValue());
    }

    // -- RECORDS -- //
    // ++ WEB UI: BLOCKS ++ //

    /*----
      VERSION: straight HTML ul-list
    */
    public function DrawTree($nLevel=0) {
        $rs = $this->GetChildRecords();
        if ($rs->HasRows()) {
            if ($this->IsNodeOpen()) {
                $sIndent = str_repeat("\t",$nLevel);		// make the raw HTML more readable
                $out = "\n$sIndent<ul>";
                $nLevel++;
                $sIndent = str_repeat("\t",$nLevel);		// make the raw HTML more readable
                while ($rs->NextRow()) {
                    $sName = $rs->GetNameString();
                    
                    // 2018-05-29 This can be better optimized -- pass recordset to DrawTree(), or cache it
                    $rsSub = $rs->GetChildRecords();
                    $nSubs = $rsSub->RowCount();
                    if ($nSubs == 0) {
                        $htOpen = '&mdash;';
                        $htSubs = '';
                    } else {
                        $urlSubs = $rs->GetNodeChangeURL();
                        $sOpen = $rs->IsNodeOpen()?'[-]':'[+]';
                        $htOpen = "<a href='$urlSubs'>$sOpen</a>";
                        $htSubs = " ($nSubs)";
                    }
                    
                    $css = $rs->GetIsActive()?'state-active':'state-inactive';
                    // 2018-06-05 popup does not support HTML
                    $htTwig = "<span class=$css>$sName</span>";	// italicize if inactive
                    $sTwig = $sName;
                    $ftTwig = $rs->SelfLink($htTwig,"go to &ldquo;$sTwig&rdquo; account");
                    $out .=
                      "\n<li>$htOpen $sIndent$ftTwig$htSubs</li>"
                      .$rs->DrawTree($nLevel);
                }
                $nLevel--;
                $sIndent = str_repeat("\t",$nLevel);		// make the raw HTML more readable
                $out .= "\n$sIndent</ul>";
            } else {
                $out = NULL;
            }
        } else {
            if ($nLevel == 0) {
                $out = "\nNo accounts created yet.";
            } else {
                $out = '';
            }
        }
        return $out;
    }

    // -- WEB UI: BLOCKS -- //
    // ++ WEB UI: ELEMENTS ++ //

    public function DropDown_TrxType(array $iarArgs=NULL) {
        throw new \exception('2018-05-09 If anyone is still calling this, it will need fixing.');
        $strName = nz($iarArgs['form.name'],'type');
        $strDef = nz($iarArgs['default']);
        $strEmtpy = nz($iarArgs['empty'],'N/A');

        $objRow = $this->TxTypeRecords();

        if ($objRow->hasRows()) {
            $out = '<select name="'.$strName.'">'."\n";
            while ($objRow->NextRow()) {
            $strCode = $objRow->Code;
            $htSelect = "";
            if ($strCode == $strDef) {
                $htSelect = " SELECTED";
            }
            if ($objRow->isEquity) {
                $strSign = '';
            } else {
                $strSign = $objRow->isDebit?'-':'+';
            }
            $strDescr = $strCode.$strSign;
            $out .= '<option'.$htSelect.' value="'.$strCode.'">'.$strDescr.'</option>'."\n";
            }
            $out .= '</select>'."\n";
        } else {
            $out = $strEmpty;
        }
        return $out;
    }
    /*-----
      ACTION: Display a drop-down list for all the records in the current dataset
    */
    public function DropDown(array $iarArgs=NULL) {
        throw new \exception('2018-05-07 Is anything still calling this?');
        $strName = nz($iarArgs['form.name'],'acct');
        $idDef = nz($iarArgs['default']);
        $strEmpty = nz($iarArgs['empty'],'N/A');

        $objRow = $this;
        if ($objRow->hasRows()) {
            $out = '<select name="'.$strName.'">'."\n";
            while ($objRow->NextRow()) {
            $idAcct = $objRow->ID;
            $strLine = $objRow->Name;
            if (!is_null($objRow->Descr)) {
                $strLine .= ' - '.$objRow->Descr;
            }
            $htSelect = "";
            if ($idAcct == $idDef) {
                $htSelect = " SELECTED";
            }
            $out .= '<option'.$htSelect.' value="'.$idAcct.'">'.$strLine.'</option>'."\n";
            }
            $out .= '</select>'."\n";
        } else {
            $out = $strEmpty;
        }
        return $out;
    }
    /*-----
      ACTION: Display a ComboBox for all the records in the current dataset
      DEPRECATED; use DropDown() instead
    */
    public function ComboBox($idCur=NULL,$iDoLong=FALSE) {
        throw new \exception('2018-05-07 Is anything still calling this?');
        $this->StartRows();
        $out = '<select name="acct">'."\n";
        while ($this->NextRow()) {
            $htSelect = "";
            if ($idCur == $this->ID) {
                $htSelect = " SELECTED";
            }
            $strDescr = $this->Name;
            if ($iDoLong) {
                if (!is_null($this->Descr)) {
                    $strDescr .= ': '.$this->Descr;
                }
            }
            $out .= '<option'.$htSelect.' value="'.$this->ID.'">'.$strDescr.'</option>'."\n";
        }
        $out .= '</select>'."\n";
        return $out;
    }
    /*----
      FUNCTION: ShowAsHTML - show the account formatted helpfully in HTML (using links)
    */
    public function ShowAsHTML($iDepth) {
        throw new \exception('2018-05-15 If anything is still using this, it will need updating.');
        global $agPage;

        $arLink['page'] = 'acct';
        $arLink['id'] = $this->ID;
        $out = $agPage->SelfLink_HTML($arLink,$this->Name,TRUE);

        if (!is_null($this->ID_Parent)) {
            $objParent = $this->objDB->Accts()->GetItem($this->ID_Parent);
            if ($iDepth) {
            $out .= ' &lt; '.$objParent->ShowAsHTML($iDepth-1);
            }
        }
        return $out;
    }

    // -- WEB UI: ELEMENTS -- //
    // ++ WEB UI: PAGES ++ //

    public function AdminPage() {
        if ($this->IsNew()) {
            $sShow = 'rec';
        } else {
            $sName = $this->GetNameString();
        
            $oMenu = \fcApp::Me()->GetHeaderMenu();
            $oMenu->SetNode($oGrp = new \fcHeaderChoiceGroup('show','View'));
                        // ($sKeyValue,$sPopup=NULL,$sDispOff=NULL,$sDispOn=NULL)
              $oGrp->SetChoice($ol = new \fcHeaderChoice('rec',"'$sName' record",'record'));
              $oGrp->SetChoice($ol = new \fcHeaderChoice('trx',"'$sName' transactions",'register'));
              $sShow = $oGrp->GetChoiceValue();
              
              $this->SetPageState($oGrp->GetSelfArray());
        }

        switch ($sShow) {
          case 'rec':
            $out = $this->AdminRecord();
            break;
          case 'trx':
            $out = $this->AdminRegister();
            break;
          default:
            $out = NULL;
        }
        return $out;
        }
        protected function AdminRecord() {
        $oPathIn = \fcApp::Me()->GetKioskObject()->GetInputObject();
        $oFormIn = \fcHTTP::Request();
        
        $out = NULL;

        $doSave = $oFormIn->GetBool('btnSave');

        // save edits before showing events
        if ($doSave) {
            $frm = $this->PageForm();
            $frm->Save();
            $ftSaveMsg = $frm->MessagesString();
            $this->SelfRedirect(NULL,$ftSaveMsg);
        }

        if ($this->IsNew()) {
            $doEdit = TRUE;
        } else {
            $sName = $this->GetNameString();
            $oMenu = \fcApp::Me()->GetHeaderMenu();

              $oMenu->SetNode($oGrp = new \fcHeaderChoiceGroup('do','Action'));	
              
            // ($sKeyValue,$sPopup=NULL,$sDispOff=NULL,$sDispOn=NULL)
            $oGrp->SetChoice($ol = new \fcHeaderChoice('edit','edit '.$sName));
            
              $sAct = $oGrp->GetChoiceValue();
              $ol->AddLinkArray($this->GetPageState());	// make sure other page state is preserved
            
            $doEdit = FALSE;
            switch($sAct) {
              case 'edit':
            $doEdit = TRUE;
            break;
            }
        }

        $frmEdit = $this->PageForm();
        if ($this->IsNew()) {
            $frmEdit->ClearValues();
        } else {
            $frmEdit->LoadRecord();
        }
        $oTplt = $this->PageTemplate();
        $arCtrls = $frmEdit->RenderControls($doEdit);
        $arCtrls['!ID'] = $this->SelfLink();

        if ($doEdit) {
            $out .= "\n<form method=post>";
            $arCtrls['!extra'] = '<tr>	<td colspan=2><b>Edit notes</b>: <input type=text name="'
              .KS_FERRETERIA_FIELD_EDIT_NOTES
              .'" size=60></td></tr>'
              ;
        } else {
            $arCtrls['!extra'] = NULL;
        }

        $oTplt->SetVariableValues($arCtrls);
        $out .= $oTplt->RenderRecursive();
        
        if ($doEdit) {	    
            $out .= <<<__END__
<input type=submit name="btnSave" value="Save">
<input type=reset value="Reset">
</form>
__END__;
        }

        return $out;
    }

    private $frmPage=NULL;
    private function PageForm() {
        if (is_null($this->frmPage)) {
        
            $oForm = new \fcForm_DB($this);

              $oField = new \fcFormField_Text($oForm,'Name');
            $oCtrl = new \fcFormControl_HTML($oField,array('size'=>25));

              $oField = new \fcFormField_Text($oForm,'Descr');
            $oCtrl = new \fcFormControl_HTML($oField,array('size'=>50));

              $oField = new \fcFormField_Num($oForm,'ID_Parent');
            $oField->ControlObject($oCtrl = new \fcFormControl_HTML_DropDown($oField));
            $oCtrl->SetRecords($this->GetTableWrapper()->GetRecords_forDropDown());
            $oCtrl->AddChoice(NULL,'(root)');

              $oField = new \fcFormField_Num($oForm,'ID_Alias');
            $oField->ControlObject($oCtrl = new \fcFormControl_HTML_DropDown($oField));
            $oCtrl->SetRecords($this->GetTableWrapper()->GetRecords_forDropDown());
            $oCtrl->AddChoice(NULL,'(none)');

              $oField = new \fcFormField_Num($oForm,'ID_Type');
            $oField->ControlObject($oCtrl = new \fcFormControl_HTML_DropDown($oField));
            $oCtrl->SetRecords($this->AccountTypeTable()->GetRecords_forDropDown());
            
              $oField = new \fcFormField_Num($oForm,'ID_Owner');
            $oField->ControlObject($oCtrl = new \fcFormControl_HTML_DropDown($oField));
            $oCtrl->SetRecords($this->OwnerTable()->GetRecords_forDropDown());
            $oCtrl->AddChoice(NULL,'(none)');

            $this->frmPage = $oForm;
        }
        return $this->frmPage;
    }
    private $tpPage;
    protected function PageTemplate() {
        if (empty($this->tpPage)) {
            $sTplt = <<<__END__
<table class=form-record>
  <tr>	<td align=right><b>ID</b>:</td>		<td>[[!ID]]</td>	</tr>
  <tr>	<td align=right><b>Name</b>:</td>	<td>[[Name]]</td>	</tr>
  <tr>	<td align=right><b>About</b>:</td>	<td>[[Descr]]</td>	</tr>
  <tr>	<td align=right><b>Parent</b>:</td>	<td>[[ID_Parent]]</td>	</tr>
  <tr>	<td align=right><b>Alias</b>:</td>	<td>[[ID_Alias]]</td>	</tr>
  <tr>	<td align=right><b>Type</b>:</td>	<td>[[ID_Type]]</td>	</tr>
  <tr>	<td align=right><b>Owner</b>:</td>	<td>[[ID_Owner]]</td>	</tr>
  [[!extra]]
</table>
__END__;
            $this->tpPage = new \fcTemplate_array('[[',']]',$sTplt);
        }
        return $this->tpPage;
    }

    private $frmLine;
    protected function LineForm() {
        if (empty($this->frmLine)) {
            $oForm = new fcForm_DB($this->Table()->ActionKey(),$this);

              $oField = new fcFormField_Num($oForm,'ID');
            $oCtrl = new fcFormControl_HTML_Hidden($oForm,$oField,array());

              $oField = new fcFormField_Text($oForm,'Name');
            $oCtrl = new fcFormControl_HTML($oForm,$oField,array('size'=>15));

              $oField = new fcFormField_Text($oForm,'Descr');
            $oCtrl = new fcFormControl_HTML($oForm,$oField,array('size'=>40));

              $oField = new fcFormField_Num($oForm,'ID_Parent');
            $oCtrl = new fcFormControl_HTML_DropDown($oForm,$oField,array());
                    $oCtrl->Records($this->ParentRecords());	// ideally: all active accounts except the current one

              $oField = new fcFormField_Num($oForm,'ID_Owner');
            $oCtrl = new fcFormControl_HTML_DropDown($oForm,$oField,array());
                    $oCtrl->Records($this->OwnerRecords());

            $this->frmPage = $oForm;
        }
        return $this->frmPage;
    }
    private $tpLine;
    protected function LineTemplate() {
        if (empty($this->tpPage)) {
            $sTplt = <<<__END__
  <tr style="{{!cssStyle}}">
    <td align=right>{{ID}}</td>
    <td>{{Name}}</td>
    <td>{{Descr}}</td>
    <td>{{ID_Parent}}</td>
    <td>{{ID_Owner}}</td>
  </tr>
__END__;
            $this->tpLine = new fcTemplate_array('{{','}}',$sTplt);
        }
        return $this->tpLine;
    }
    protected function AdminRow($cssStyle) {
        $row = $this->Values();

        $id = $row['ID'];
        $strName = $row['Name'];
        $strDescr = $row['Descr'];
        $idParent = $row['ID_Parent'];
        $htOwner = $row['ID_Owner'];

        if (is_null($idParent)) {
            $htParent = '-';
        } else {
            $rcParent = $this->Table()->GetItem($idParent);
            $htParent = $rcParent->AdminLink($rcParent->Name());
        }
        /* 2015-07-08 This is from when I had all the controls in each row.
        $htAct = ' ['
          .$agPage->SelfLink(array('page'=>'acct','id'=>$id),'E','edit this account').'&nbsp;'
          .$agPage->SelfLink(array('page'=>'acct.trxs','id'=>$id),'TS','transactions sourced by this account').'&nbsp;'
          .$agPage->SelfLink(array('page'=>'acct.trxd','id'=>$id),'TT','transactions targeting this account').'&nbsp;'
          .$agPage->SelfLink(array('page'=>'acct','id'=>$id,'do'=>'reg'),'R','enter transactions from register').'] ';
        */
        $htName = $this->AdminLink($this->Name());

        // form variables
        $arCtrls = $row;
        $arCtrls['!cssStyle'] = $cssStyle;
        $arCtrls['ID_Parent'] = $htParent;
        $arCtrls['Name'] = $htName;

        // render the form
        $oTplt = $this->LineTemplate();
        $oTplt->VariableValues($arCtrls);
        $out = $oTplt->Render();

        /* 2015-07-30 pre-template version
        $out = '<tr style="'.$cssStyle.'">'.
          '<td align=right>'.$id.'</td>'.
        //		  '<td align=center>'.$htAct.'</td>'.
          '<td>'.$htName.'</td>'.
          '<td>'.$strDescr.'</td>'.
          '<td>'.$htParent.'</td>'.
          '<td>'.$htOwner.'</td>'.
          '</tr>'; */
        return $out;
    }
    protected function EditRow() {
        // form variables
        $arCtrls = $this->Values();
        $arCtrls['!cssStyle'] = '';
        $arCtrls['ID'] = '';
        $arCtrls['Descr'] = '';
        $arCtrls['ID_Parent'] = '';
        $arCtrls['Name'] = '';
        $arCtrls['ID_Owner'] = '';

        // render the form
        $oTplt = $this->LineTemplate();
        $oTplt->VariableValues($arCtrls);
        $out = $oTplt->Render();

        return $out;
    }
    /*----
      RENDERS: Table (with header row) showing all accounts in the current recordset
    */
    public function AdminList() {
        $vgPage = clsApp::Me()->MWSpecialPage();
        $doEdit = $vgPage->Arg('edit');
        $doAdd = $vgPage->Arg('add');

        $rs = $this;
        if ($rs->hasRows()) {
            $out = <<<__END__
<table>
  <tr>
    <th>ID</th>
    <th>Name</th>
    <th>Description</th>
    <th>Parent</th>
    <th>Owner<th>
    </th>
  </tr>
__END__;
            $isOdd = FALSE;
            $tAccts = $this->Table();	// for looking up parent records
            while ($rs->NextRow()) {
                $isOdd = !$isOdd;
                $cssStyle = $isOdd?'background:#ffffff;':'background:#eeeeee;';
                $out .= $this->AdminRow($cssStyle);
            }
            if ($doAdd) {
                $out .= $this->EditRow();
            }
            $out .= '</table>';
        } else {
            $out = 'No accounts found';
        }
        return $out;
    }
    /*----
      HISTORY:
        2015-07-29 A note from 2009-12-06 said "This is probably obsolete", but it isn't.
    */
    public function AdminRegister() {
        if ($this->IsNew()) {
            $out = NULL;	// new account, no transactions yet
        } else {
            $idAcct = $this->GetKeyValue();
            $tTrx = $this->TransactionTable();
            $ar = $tTrx->FigureStats_forAccount($idAcct);
            $nRows = $ar['NumRows'];
            if ($nRows == 0) {
                    $out = NULL;
            } else {
                    $out = <<<__END__
<span class=content>
<b># Rows</b>: $nRows
<b># Active</b>: {$ar['NumActive']}
<b>Earliest</b>: {$ar['DateFirst']}
<b>Latest</b>: {$ar['DateFinal']}
</span>
__END__;
            }
            $out .= $tTrx->ShowRegister_forAccount($idAcct);
        }
        return $out;
    }
    /*----
      PURPOSE: account register bulk data entry
    */
    public function EnterRegister() {
        throw new \exception('2018-05-09 This will need updating/rewriting.');
        global $wgRequest;
        //global $vgPage;

        $vgPage = clsApp::Me()->Page();
        $vgPage->UseHTML();

        $out = '';

        $strSt = $wgRequest->getVal('pgst');
        $intSt = (int)$strSt;
        //if (strlen($this->strPageSfx) == 0) {
        $lenSt = strlen($strSt);
        if ($lenSt == 0) {
            // use default values
            $lenSt = 2;
            $intSt = 1;
    //	} else {
    //	    $lenSt = strlen($this->strPageSfx);
    //	    $intSt = $this->strPageSfx;
        }
    //$out .= "strSt=$strSt intSt=$intSt lenSt=$lenSt strlen(strSt)=".strlen($strSt)."<br>";
        $arArgs['idx.len'] = $lenSt;
        $idxRowSt = $intSt;

        $strPg = $wgRequest->getVal('pgkey');

        if ($wgRequest->getBool('btnParse')) {
            $strTrx = $wgRequest->getVal('trxlist');

            $out .= '<form method=POST><table name=tblParsed>';

            $this->strPagePfx = $strPg;
            $this->strPageSfx = $strSt;
            $htPg = htmlspecialchars($strPg);
            $htTrx = htmlspecialchars($strTrx);
            $htSt = htmlspecialchars($strSt);

            $xts = new xtString(NULL,TRUE);
            $arLines = explode("\n",$strTrx);
            $intLine = $intSt;
            $arRaw = NULL;
            foreach ($arLines as $line) {
            if (!empty($line)) {
                if (substr($line,0,1) == ' ') {
                $xts->Value = substr($line,1);
                $xts->DelTail();
                $ar = $xts->SplitFirst(' ');
                $key = $ar['before'];
                $val = $ar['after'];
                $arRaw[$key] = $val;
                //$out .= '<br> - field ['.$key.']=['.$val.']';
                } else {
                if (is_array($arRaw)) {
                    $arArgs['idx.val'] = $intLine;
                    $out .= $this->HandleRegisterLine($arRaw,$arArgs);

                    $intLine++;
                }
                $arRaw = NULL;
                //$out .= '<br>LINE '.$intLine.':';
                $xts->Value = ' '.$line;
                $xts->DelTail();
                if (!empty($xts->Value)) {
                    $ar = $xts->Xplode();
                    $arRaw['type'] = $ar[0];
                    $arRaw['date'] = $ar[1];
                    $arRaw['amt'] = $ar[2];

                }
                }
            }
            }
            $arArgs['idx.val'] = $intLine;
            $out .= $this->HandleRegisterLine($arRaw,$arArgs);

            $out .= '</table>';
            $txtEntry = Pluralize($intLine,'This Entry','These Entries');
            $out .= '<input type=hidden name=pgkey value="'.$htPg.'">';
            $out .= '<input type=hidden name=pgst value="'.$htSt.'">';
            $out .= '<input type=hidden name=pgfi value="'.$intLine.'">';
            $out .= '<input type=submit name=btnAdd value="Add '.$txtEntry.'">';
            $out .= '</form>';
        } else {
            if ($wgRequest->getBool('btnAdd')) {
            $arForm = $wgRequest->GetArray('data');
            if (is_array($arForm)) {
                $qRows = count($arForm);
                $idxRowFi = $wgRequest->GetIntOrNull('pgfi');
                $out .= 'Adding <b>'.$qRows.'</b> transaction'.Pluralize($qRows);
                $txtEv = 'adding '.$qRows.' transaction'.Pluralize($qRows);
                if (($idxRowSt != 1) || ($idxRowFi != 1))  {
                $txtSnip = ' (line'.Pluralize($qRows).' '.$idxRowSt;
                $out .= $txtSnip;
                $txtEv .= $txtSnip;
                if ($idxRowFi != $idxRowSt) {
                    $txtSnip = ' to '.$idxRowFi;
                    $out .= $txtSnip;
                    $txtEv .= $txtSnip;
                }
                $txtSnip = ') ';
                $out .= $txtSnip;
                $txtEv .= $txtSnip;
                }
                $out .= ' from register page <b>'.$strPg.'</b>:';
                $txtEv .= 'from register page '.$strPg;
                $out .= '<table>';
                $out .= clsTxact::ShowHeader();
                $arEvent = array(
                  'descr'	=> SQLValue($txtEv),
                  'where'	=> __METHOD__,
                  'code'	=> 'ADD',
                  'error'	=> FALSE,
                  'severe'	=> FALSE
                  );
                $idEv = $this->StartEvent($arEvent);
                foreach ($arForm as $idx=>$arRow) {
                $arIns['ID_Srce'] = $this->ID;
                foreach ($arRow as $fname=>$fval) {
                    $sqlName = $this->Engine()->SafeParam($fname);
                    $sqlVal =  SQLValue($this->Engine()->SafeParam($fval));
                    $arIns[$sqlName] = $sqlVal;
                    // is this a security hole? malicious user could inject arbitrary field names,
                    //	but SafeParam() should prevent any other code from executing...
                }
                $arIns['WhenEntered'] = 'NOW()';
                $this->Engine()->Txacts()->Insert($arIns);	// create the transaction record
                $idTx = $this->Engine()->NewID();
                $objTx = $this->Engine()->Txacts()->GetItem($idTx);
                // later we'll have a flag to mark the event as complete right away...
                $objTx->StartEvent($arEvent);	// log row-specific event
                $out .= $objTx->ShowRegRow(FALSE);
                $arDone[$idTx] = $arIns;
                }
                $this->FinishEvent($idEv,array('id'=>$idTx));
                $out .= '</table>';
            }
            }
            $htPg = NULL;
            $htTrx = NULL;
            $htSt = NULL;
            //$out = NULL;
        }
        $out .= <<<__END__
<form method=POST>
  Register page key: <input type=text name=pgkey value="$htPg" size=10>
    starting index: <input type=text name=pgst value="$htSt" size=3>
  <br>Enter transactions in the format "<u>type</u> <u>date</u> <u>amount</u>".
  <br>Additional parameters on following lines, starting with a space: <b>ck from/to memo conf/ref</b>
  <br><textarea name=trxlist rows=30 cols=80>$htTrx</textarea>
  <br><input type=submit name=btnParse value="Parse">
</form>';
__END__;
        return $out;
    }
    /*
      ACTION: Handles parsed data from the text lines which together will create a single register entry.
        Some of the data comes from the initial line; the rest comes from the indented lines.
      HISTORY:
        2018-05-09 This will need updating in order to work.
    */
    private function HandleRegisterLine(array $iData,array $iarArgs) {
        if (is_array($iData)) {
            $arUpd['WhenEntered'] = 'NOW()';
            $out = '';

            // generate stuff from current line index
            $intSt = $iarArgs['idx.val'];
            $lenSt = $iarArgs['idx.len'];
            $this->strPageSfx = sprintf('%0'.$lenSt.'u',$intSt);
            $strLineName = $strLineName = $this->strPagePfx.'-'.$this->strPageSfx;
            $htFieldTplt = "data[$intSt][%s]";

            foreach($iData as $key=>$val) {
            $htName = NULL;
            $htVal = htmlspecialchars($val);
            switch ($key) {
              case 'ck':
                $htName = 'CheckNum';
                break;
              case 'conf':
              case 'ref':
                $htName = 'ConfNum';
                break;
              case 'memo':
                $htName = 'Descr';
                break;
              case 'from':
                $htName = 'TextDest';
                $isDebit = FALSE;
                $strDest = $val;
                $objDest = $this->Table()->Search($val);
                break;
              case 'to':
                $htName = 'TextDest';
                $isDebit = TRUE;
                $strDest = $val;
                $objDest = $this->Table()->Search($val);
                break;
              case 'type':
                $strType = strtoupper($val);
                $objTypeTbl = $this->Engine()->TxTypes();
                $objTypeRows = $objTypeTbl->FindCode($strType);
                $qTypes = $objTypeRows->RowCount();
                $htFieldName = sprintf($htFieldTplt,'ID_Type');
                if ($qTypes > 1) {
                $htType = '(q='.$qTypes.') '.$objTypeRows->DropDown();
                } elseif ($qTypes == 1) {
                $objTypeRows->FirstRow();
                $strType = $objTypeRows->Row['Code'];
                $htType = $strType.'<input type=hidden name="'.$htFieldName.'" value="'.$strType.'">';
                } else {
                $htType = $this->DropDown_TrxType(array('form.name'=>$htFieldName));
                }
                break;
              case 'date':
                if (empty($val)) {
                $htDate = NULL;
                } else {
                $objDate = new xtTime($val);
                $objDate->AssumeYear(0);
                //$arDate = $objDate->PartsArray();
                $htVal = $objDate->FormatSQL();
                $htDate = $objDate->FormatSortable();
                $htName = 'DateAction';
                // 2010-04-28 this line duplicates the functionality invoked by setting $htName
                //$htDate .= '<input type=hidden name=DateAction value="'.$htDate.'">';
                }
              case 'amt':
                //$dlrAmt = $iData['amt'];
                $dlrAmt = $val;
                break;
            }
            if (!is_null($htName)) {
                $htFieldName = sprintf($htFieldTplt,$htName);
                $out .= '<input type=hidden name="'.$htFieldName.'" value="'.$htVal.'">';
            }
            }
            //$htType = $iData['type'];

    // this has to be done after 'type' is determined
    /* This doesn't seem to be necessary
            if ($isDebit) {
            $dlrAmt = -$dlrAmt;
            }
    */
            $htAmt = '$'.sprintf('%0.2f',$dlrAmt);
            $htFieldName = sprintf($htFieldTplt,'Amount');
            $out .= '<input type=hidden name="'.$htFieldName.'" value="'.$dlrAmt.'">';

            if (isset($objDest)) {
            $qRows = $objDest->RowCount();
            if ($qRows > 1) {
                $htDestName = $objDest->DropDown().'&larr;'.$strDest;
            } elseif ($qRows == 1) {
                $objDest->FirstRow();
                $htDestNameVal = $objDest->AdminLink($objDest->Name);
                $htFieldName = sprintf($htFieldTplt,'ID_Dest');
                $htDestName = $htDestNameVal
                  .'<input type=hidden name="'.$htFieldName.'" value='.$objDest->ID.'>';
            } else {
                $htDestNameVal = htmlspecialchars($strDest);
                $htDestName = '<b>??</b>'.$htDestNameVal;	// LATER: edit box
            }
            if ($isDebit) {
                $htDestPfx = '<b>To</b>:';
            } else {
                $htDestPfx = '<b>From</b>:';
            }
            $htDest = $htDestPfx.$htDestName;
            }

            $htMemo = nz($iData['memo']);
            // display
            $out .= "<tr>"
              ."<td>$strLineName</td>"
              ."<td>$htType</td>"
              ."<td>$htDate</td>"
              ."<td align=right>$htAmt</td>"
              ."<td>$htDest</td>"
              ."<td>$htMemo</td>"
              ."</tr>";
            // data to save
            $htSortEntry = htmlspecialchars($strLineName);
            $htFieldName = sprintf($htFieldTplt,'SortEntry');
            $out .= '<input type=hidden name="'.$htFieldName.'" value="'.$htSortEntry.'">';
            return $out;
        } else {
            return NULL;
        }
    }

    // -- WEB UI: PAGES -- //
}

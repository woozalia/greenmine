<?php namespace greenmine\FinanceFerret;
/*
  PURPOSE: Transaction Type classes for FinanceFerret
  HISTORY:
    2015-05-08 split off from FinanceFerret.php
    2018-05-06 namespace; revamping finally
*/

class ctTxTypes extends \fcTable_keyed_single_standard implements \fiLinkableTable {
    use \ftLinkableTable;
//    private $idAcctType;

    // ++ SETUP ++ //
    
    public function GetKeyName() {
	return 'Code';
    }
    protected function TableName() {
	return 'Trx Types';
    }
    protected function SingularName() {
	return __NAMESPACE__.'\\crTxType';
    }
    public function GetActionKey() {
	return KS_ACTION_FINFER_TXTY;
    }

    // -- SETUP -- //
    // ++ LOOKUP ++ //

    /*-----
      ACTION: Find the transaction type whose code matches $iCode
      RETURNS: data for the matching record(s)
      INPUT:
	$iAllowPart: TRUE = allow partial matches, e.g. "AD" would find "ADJ"
    */
    public function FindCode($iCode, $iAllowPart=FALSE) {
	throw new \exception('2018-05-12 If anything is using this, it will need rewriting.');
	if ($iAllowPart) {
	    $sqlFilt = 'Code LIKE "'.$iCode.'%"';
	} else {
	    $sqlFilt = 'Code="'.$iCode.'"';
	}
	$objRows = $this->GetData($sqlFilt);
	return $objRows;
    }

    // -- LOOKUP -- //
    // ++ RECORDSETS ++ //

    public function DataSet_forDropDown() {
	throw new \exception('2018-05-06 Call GetRecords_forDropDown() instead.');
    }
    // 2018-05-12 renamed from GetRecordset_forDropDown() to GetRecords_forDropDown()
    public function GetRecords_forDropDown() {
//	$sql = 'SELECT * FROM '.$this->NameSQL();	// first two columns are ID and text
//	$rs = $this->DataSQL($sql);
	$rs = $this->SelectRecords();			// 2018-05-06 maybe refine later
	return $rs;
    }

    // -- RECORDSETS -- //

    /*-----
      DEPRECATED - use clsAcctType->MakeList()
    */
    public function MakeList($iType) {
	if (!isset($this->objXAccts) || ($iType != $this->idAcctType)) {
	    $this->idAcctType = $iType;
	    $this->FigureList();
	}
    }
    public function FigureList() {
        throw new \exception('2019-02-14 Who calls this?'); // $sort is never used
	if (!isset($this->objXAccts)) {
	    $this->objXAccts = $this->objDB->DataSet(KSQL_TxTypes_x_AcctType);
	}
	if (!isset($this->arData)) {
	    $this->objXAccts->StartRows();
	    while ($this->objXAccts->NextRow()) {
		$code = $this->objXAccts->Code;
		if (!isset($this->arType[$code])) {
		    $this->arType[$code] = '';
		}

		if (isset($this->arData[$code])) {
		    $this->arData[$code]->AddType($this->objXAccts->ID_AcctType);
		} else {
		    $objRow = new clsTxType();
		    $objRow->Init($this->objXAccts);
		    $this->arData[$code] = $objRow;
		}
	    }
	    foreach ($this->arData as $code=>$objRow) {
		$isAvail = $objRow->HasType($this->idAcctType);
		$sort = $isAvail?'0':'1';
		$sort .= $code;
	    }
	}
    }
    public function ComboBox($iName,$iCur) {
	throw new exception('Does anyone still call this?');

	$out = '<select name="'.$iName.'">'."\n";
	foreach ($this->arData as $code => $objRow) {
	    $htSelect = "";
	    if ($code == $iCur) {
		    $htSelect = " SELECTED";
	    }
	    if ($objRow->isEquity) {
		$strSign = '';
	    } else {
		$strSign = $objRow->isDebit?'-':'+';
	    }
	    $strDescr = $code.$strSign;
	    $out .= '<option'.$htSelect.' value="'.$code.'">'.$strDescr.'</option>'."\n";
	}
	$out .= '</select>'."\n";
	return $out;
    }
}
class crTxType extends \fcRecord_keyed_single_integer implements \fiLinkableRecord {
    use \ftLinkableRecord;
/*
    public $isAvail;
    public $Code;
    public $AcctTypes;
    public $Descr;
    public $isDebit;
    public $isEquity;
*/
    // ++ SETUP ++ //

    public function Init($iDataSet) {
	throw new exception('Is anyone still calling this?');
	$this->Code		= $iDataSet->Code;
	$this->Descr		= $iDataSet->Descr;
	$this->isDebit		= $iDataSet->IsDebit;
	$this->isEquity		= $iDataSet->IsEquity;
	$this->AcctTypes	= ' '.$iDataSet->ID_AcctType;
    }
    public function Load() {
	throw new exception('Is anyone still calling this?');
	$this->isDebit		= $this->IsDebit;
	$this->isEquity		= $this->IsEquity;
    }

    // -- SETUP -- //
    // ++ BOILERPLATE HELPERS ++ //

    public function AdminLink_name() {
	return $this->SelfLink($this->Code());
    }

    // -- BOILERPLATE HELPERS -- //
    // ++ ACTION ++ //

    public function AddType($iType) {
	$this->AcctTypes .= ' '.$iType;
    }

    // -- ACTION -- //
    // ++ DATA FIELD ACCESS ++ //

    protected function Code() {
	return $this->GetFieldValue('Code');
    }
    protected function Descr() {
	return $this->GetFieldValue('Descr');
    }
    protected function isDebit() {
	return $this->GetFieldValue('IsDebit');
    }
    protected function isEquity() {
	return $this->GetFieldValue('IsEquity');
    }
    public function HasType($iType) {
	$pos = strpos($this->AcctTypes, ' '.$iType);
	return ($pos !== FALSE);
    }

    // -- DATA FIELD ACCESS -- //
    // ++ CALLBACKS ++ // (see fcFormControl_HTML_DropDown::RenderValue() and ::RenderEditor())

    public function ListItem_Value() {
	return $this->KeyValue();
    }
    public function ListItem_Text() {
	$sSign = $this->isDebit()?'-':'+';
	return $this->Code().$sSign.' '.$this->Descr();
    }
    public function ListItem_Link() {
	return $this->AdminLink_name();
    }

    // -- CALLBACKS -- //
    // ++ DATA FIELD CALCULATIONS ++ //

    public function BalanceChange($iAmount=1) {
	throw new exception('2018-05-06 Call FigureBalanceChange() instead.');
    }
    public function FigureBalanceChange($iAmount=1) {
	if ($this->isEquity()) {
	    $ret = 0;
	} else {
	    if ($this->isDebit()) {
		$ret = $iAmount * (-1.0);
	    } else {
		$ret = $iAmount;
	    }
	}
	return $ret;
    }

    // -- DATA FIELD CALCULATIONS -- //

    public function DropDown(array $iarArgs=NULL) {
	throw new exception('Is anyone still calling this?');

	$strName = nz($iarArgs['form.name'],'type');
	$strDef = nz($iarArgs['default']);
	$strEmtpy = nz($iarArgs['empty'],'N/A');

	$objRow = $this;
	if ($objRow->hasRows()) {
	    $out = '<select name="'.$strName.'">'."\n";
	    while ($objRow->NextRow()) {
		$strCode = $objRows->Code;
		$htSelect = "";
		if ($strCode == $strDef) {
			$htSelect = " SELECTED";
		}
		if ($objRow->isEquity) {
		    $strSign = '';
		} else {
		    $strSign = $objRow->isDebit?'-':'+';
		}
		$strDescr = $strCode.$strSign;
		$out .= '<option'.$htSelect.' value="'.$strCode.'">'.$strDescr.'</option>'."\n";
	    }
	    $out .= '</select>'."\n";
	} else {
	    $out = $strEmpty;
	}
	return $out;
    }
}

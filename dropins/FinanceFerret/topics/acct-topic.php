<?php
/*
  PART OF: FinanceFerret
  PURPOSE: classes for handling title-topic assignments
  HISTORY:
    2013-11-06 split off from SpecialVbzAdmin.main.php
    2018-05-24 adapting from VbzCart (formerly VbzAdmin)
*/
class vctTitlesTopics_admin extends vctTitlesTopics {
    // ++ CLASS NAMES ++ //

    protected function TitlesClass() {
	return KS_CLASS_CATALOG_TITLES;
    }
    protected function TopicsClass() {
	return KS_CLASS_CATALOG_TOPICS;
    }

    // -- CLASS NAMES -- //
}

<?php namespace greenmine\FinanceFerret;
/*
  PURPOSE: Account Type classes for FinanceFerret
  HISTORY:
    2015-05-08 split off from FinanceFerret.php
*/

class ctAcctTypes extends \ferreteria\data\cStandardTableSingleKey implements \fiLinkableTable {
    use \ftLinkableTable;

    // ++ SETUP ++ //
    
    /*
    public function __construct($iDB) {
	parent::__construct($iDB);
	  $this->Name('Acct Types');
	  $this->KeyName('ID');
	  $this->ActionKey(KS_ACTION_ACCT_TYPE);
	  $this->ClassSng('clsAcctType');
    }*/
    protected function SingleRowClass() : string { return __NAMESPACE__.'\\ctAcctType'; }
    protected function GetTableName() : string { return 'Acct Types'; }
    public function GetActionKey() : string { return KS_ACTION_FINFER_ACTY; }
    
    // -- SETUP -- //
    // ++ RECORDS ++ //
    
    public function GetRecords_forDropDown() {
        $rs = $this->SelectRecords();			// 2018-05-06 maybe refine later
        return $rs;
    }
    /*----
      RETURNS: recordset of all account types, with extended information
    */
    public function GetData_ext() {
        throw new \exception('2018-05-12 If this is still being used, it will need updates.');
        $sql = 'SELECT at.ID, at.Name, at.Descr, COUNT(ac.ID) AS cntAccts'
          .' FROM `'.$this->Name().'` AS at'
          .' LEFT JOIN Accounts AS ac ON ac.ID_Type=at.ID'
          .' GROUP BY at.ID, at.Name, at.Descr'
          .' ORDER BY at.ID';
        $rs = $this->DataSQL($sql);
        return $rs;
    }
    public function DataSet_DropDown() {
	throw new \exception('2018-05-12 If this is still being used, it will need updates.');
	return $this->Engine()->DataSet('SELECT * FROM qryCbx_AcctTypes;');
    }
   
    // -- RECORDS -- //
    // ++ WEB UI ++ //
    
      //++elements++//

    public function ComboBox($idCur=NULL) {
        $objRows = $this->GetData();
        $out = '<select name="acct-type">'."\n";
        while ($objRows->NextRow()) {
            $htSelect = "";
            if ($idCur == $objRows->ID) {
                $htSelect = " SELECTED";
            }
            $strDescr = $objRows->Name;
            if (!is_null($objRows->Descr)) {
                $strDescr .= ': '.$objRows->Descr;
            }
            $out .= '<option'.$htSelect.' value="'.$objRows->ID.'">'.$strDescr.'</option>'."\n";
        }
        $out .= '</select>'."\n";
        return $out;
    }

      //++pages++//
    
    public function AdminPage() {
	throw new \exception('2018-05-12 If this is still being used, it will need updates.');
	$vgPage = clsApp::Me()->Page();
	//$vgPage->UseHTML();

	$arMenu = array(
	  // 			array $iarData,	$iLinkKey,	$iGroupKey,$iDispOff,$iDispOn,$iDescr
	  new fcSectionMenuItem('add','add','add a new Account Type'),
	  );
	$out = fcSectionHeader_MW::Render($arMenu,$this->AdminURL(),'Account Types',2);


	/* older code from WorkFerret
	$objPage = new clsWikiFormatter($vgPage);
	$objSection = new clsWikiSection_std_page($objPage,'Account Types',2);
	$objLink = $objSection->AddLink_local(new clsWikiSectionLink_option(array(),'edit','do','edit','view'));
	$out = $objSection->Render();
	*/

	$rsTypes = $this->GetData_ext();
	if ($rsTypes->HasRows()) {
	    $out .= '<table><tr><th>ID</th><th>Actions</th><th>Name</th><th>Description</th><th># accts</th></tr>';
	    while ($rsTypes->NextRow()) {
		$htID = $rsTypes->AdminLink();
		$row = $rsTypes->Values();
		$strName = $row['Name'];
		$strNameLC = strtolower($strName);
		$strDescr = $row['Descr'];
		$cntAccts = $row['cntAccts'];

		//$htEdit = $rsTypes->AdminLink('edit','edit the "'.$strNameLC.'" account type',array('do'=>'edit'));
		//$htView = $rsTypes->AdminLink('A','view '.$strNameLC.' accounts',array('do'=>'view'));
		//$htView = $rsTypes->AdminLink('accts','view "'.$strNameLC.'" accounts',array('do'=>'accts'));
		$htView = $rsTypes->AdminLink('view','view "'.$strNameLC.'" accounts');

		$out .= '<tr>'
		  ."<td>$htID</td>"
		  //."<td align=center>[$htEdit] [$htView]</td>"
		  ."<td align=center>[$htView]</td>"
		  ."<td>$strName</td>"
		  ."<td>$strDescr</td>"
		  ."<td align=right>$cntAccts</td>"
		  ."</tr>"
		  ;
	    }
	    $out .= '</table>';
	} else {
	    $out = 'No account types are defined.';
	}
	return $out;
    }
    
    // -- WEB UI -- //

}
class ctAcctType extends \ferreteria\data\cRecordDeluxe implements \fiLinkableRecord {
    use \ftLinkableRecord;
    // object cache
    //private $dsTrxTypes;	// dataset of TrxTypes for this AcctType

    // ++ FIELD VALUES ++ //

    public function GetNameString() : string { return $this->GetFieldValue('Name'); }

    // -- FIELD VALUES -- //
    // ++ FIELD CALCULATIONS ++ //

    public function ListItem_Link() : string { return $this->SelfLink($this->ListItem_Text()); }
    public function ListItem_Text() : string { return $this->GetNameString(); }

    // -- FIELD CALCULATIONS -- //
    // ++ RECORDS ++ //

    /*-----
      RETURNS: a recordset of all accounts of this type
    */
    public function Accts() {
	throw new exception('Accts() is deprecated; call AccountRecords() instead.');
    }
    protected function AccountRecords() {
        if ($this->IsNew()) {
            return NULL;
        } else {
            return $this->AccountTable()->GetData('ID_Type='.$this->KeyValue());
        }
    }
    /*-----
      RETURNS: a recordset of all transaction types for this account type
    */
    public function TxTypes() {
	throw new exception('TxTypes() is deprecated; call TxTypeRecords() instead.');
    }
    protected function TxTypeRecords() {
        if (!isset($this->dsTrxTypes)) {
            $sql = sprintf(KSQL_TxTypes_for_AcctType,$this->KeyValue());
            $this->dsTrxTypes = $this->Engine()->DataSet($sql);
        }
        return $this->dsTrxTypes;
    }

    // -- RECORDS -- //
    // ++ WEB UI FORMS ++ //

    /*----
      HISTORY:
	2010-11-06 adapted from VbzStockBin for VbzAdminTitle
	2011-10-01 adapted from VbzAdminTitle for VbzAdminSupplier
	2011-10-16 adapted from VbzAdminSupplier to clsAcctType
    */
    private $oForm;
    private function PageForm() {
        if (empty($this->oForm)) {
            $oForm = new fcForm_DB($this->Table()->ActionKey(),$this);

              $oField = new fcFormField_Text($oForm,'Name');
            $oCtrl = new fcFormControl_HTML($oForm,$oField,array('size'=>10));

              $oField = new fcFormField_Text($oForm,'Descr');
            $oCtrl = new fcFormControl_HTML($oForm,$oField,array('size'=>40));

              $oField = new fcFormField_Bit($oForm,'doIndirect');
            $oCtrl = new fcFormControl_HTML_CheckBox($oForm,$oField,array());

              $oField = new fcFormField_Bit($oForm,'hasTrxacts');
            $oCtrl = new fcFormControl_HTML_CheckBox($oForm,$oField,array());

            $this->oForm = $oForm;
        }
        return $this->oForm;
    }
    private $tpPage;
    protected function PageTemplate() {
        if (empty($this->tpPage)) {
            $sTplt = <<<__END__
<table>
  <tr><td align=right><b>ID</b>:</td><td>{{ID}}</td></tr>
  <tr><td align=right><b>Name</b>:</td><td>{{Name}}</td></tr>
  <tr><td align=right><b>Description</b>:</td><td>{{Descr}}</td></tr>
  <tr><td align=right><b>Use Indirection</b>:</td><td>{{doIndirect}}</td></tr>
  <tr><td align=right><b>Use Transactions</b>:</td><td>{{hasTrxacts}}</td></tr>
</table>
__END__;
            $this->tpPage = new fcTemplate_array('{{','}}',$sTplt);
        }
        return $this->tpPage;
    }

    // -- WEB UI FORMS -- //
    // ++ WEB UI DISPLAY ++ //

    public function AdminAccts() {
        global $wgOut;

        $db = $this->Engine();

        $tbl = $db->Accts();
        $id = $this->KeyValue();
        $rs = $tbl->GetData('(ID_Type='.$id.') AND (ID_Alias IS NULL)');

    //	$out = "<h3>Accounts</h3>\n";
        $arMenu = array(
          // 			array $iarData,	$iLinkKey,	$iGroupKey,$iDispOff,$iDispOn,$iDescr
          new fcSectionMenuItem('add','add','add a new account type'),
          );
        $out = fcSectionHeader_MW::Render($arMenu,$this->AdminURL(),"Accounts",3);

        $out .= $rs->AdminList();
        return $out;
    }
    public function AdminPage() {
    throw new \exception('2020-11-12 This code is still targeting MediaWiki instead of Ferreteria. To be rewritten.');
	global $wgRequest;

	$vgPage = clsApp::Me()->MWSpecialPage();
	$vgPage->UseHTML();

	$sDo = $vgPage->Arg('do');
	$doEdit = ($sDo == 'edit');
	//$doAccts = ($sDo == 'accts');
	$doSave = $wgRequest->GetBool('btnSave');

	$sPageName = $this->Name();
	$arMenu = array(
	  // 			$sKey,$sShow,$sLong
	  new fcSectionMenuItem('edit','edit',"edit the $sPageName definition"),
	  );
	// 			array $iarData,	$iLinkKey,	$iGroupKey,$iDispOff,$iDispOn,$iDescr
	$out = fcSectionHeader_MW::Render($arMenu,$this->AdminURL(),"'$sPageName' account type",2);

	// save edits before showing events
	$ftSaveStatus = NULL;
	if ($doEdit || $doSave) {
	    if ($doSave) {
		$ftSaveStatus = $this->AdminSave();
	    }
	}

	$frmEdit = $this->PageForm();
	if ($this->IsNew()) {
	    $frmEdit->ClearValues();
	} else {
	    $frmEdit->LoadRecord();
	}
	$oTplt = $this->PageTemplate();
	$arCtrls = $frmEdit->RenderControls($doEdit);
	  // custom vars
	  $arCtrls['ID'] = $this->AdminLink();

	// render the form
	$oTplt->VariableValues($arCtrls);
	$htForm = $oTplt->Render();

	if ($doEdit) {
	    $out .= <<<__END__
<form method=post>
  $htForm
  <b>Edit notes</b>: <input type=text name="EvNotes" size=40><br>
  <input type=submit name="btnSave" value="Save">
  <input type=submit name="btnCancel" value="Cancel">
  <input type=reset value="Reset">
</form>
__END__;
	} else {
	    $out .= $htForm;
	}

	$out .= $this->AdminAccts();

	return $out;
    }

    // -- WEB UI DISPLAY -- //
    // ++ WEB UI ACTIONS ++ //

    /*----
      ACTION: Save user changes to the record
      HISTORY:
	2010-11-06 copied from VbzStockBin to VbzAdminItem
	2011-01-26 copied from VbzAdminItem to clsAdminTopic
	2011-10-02 copied from clsAdminTopic to VbzAdminSupplier
	2011-10-16 copied from VbzAdminSupplier to clsAcctType
    */
    public function AdminSave() {
    throw new \exception('2020-11-12 This code will need updating.');
	global $vgOut;

	$out = $this->objForm->Save();
	$vgOut->AddText($out);
    }

    // -- WEB UI ACTIONS -- //

}

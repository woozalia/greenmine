<?php
/*
  PURPOSE: define locations for libraries using modloader.php
  FILE SET: FinanceFerret libraries
  HISTORY:
    2015-05-08 created
    2017-02-07 adapting for Greenmine
*/
// ACTIONS

define('KS_ACTION_FINFER_ACCT','ffac');	// bank account
define('KS_ACTION_FINFER_ACTY','ffact');	// bank account type
define('KS_ACTION_FINFER_TRXT','fftr');	// transaction
define('KS_ACTION_FINFER_TRXL','fftrl');	// transaction list
define('KS_ACTION_FINFER_TXTY','fftrt');	// transaction type
define('KS_ACTION_FINFER_OWNER','ffown');	// account owner

// CLASS NAMES

define('KS_NSPACE_FINFER','greenmine\\FinanceFerret');
define('KS_CLASS_FINFER_ACCOUNTS',KS_NSPACE_FINFER.'\\ctAccts');
define('KS_CLASS_FINFER_ACCT_TYPES',KS_NSPACE_FINFER.'\\ctAcctTypes');
define('KS_CLASS_FINFER_TRANSACTS',KS_NSPACE_FINFER.'\\ctTrxts');
define('KS_CLASS_FINFER_TRANSACT_LISTS',KS_NSPACE_FINFER.'\\ctTrxLists');
define('KS_CLASS_FINFER_TRANSACT_LIST_ITEMS',KS_NSPACE_FINFER.'\\ctTrxListItems');
define('KS_CLASS_FINFER_TRANSACT_TYPES',KS_NSPACE_FINFER.'\\ctTxTypes');
define('KS_CLASS_FINFER_OWNERS',KS_NSPACE_FINFER.'\\ctOwners');

// PERMITS
//define('KS_PERMIT_FINFER_MANAGE_ACCOUNTS','finance.acct.manage');

$arActions = array(
  KS_ACTION_FINFER_ACCT		=> array(
    'class'	=> KS_CLASS_FINFER_ACCOUNTS,
    'text'	=> 'Accounts',
    'summary'	=> 'money accounts',
    #'perms'	=> NULL
    ),
  KS_ACTION_FINFER_TRXT		=> array(
    'class'	=> KS_CLASS_FINFER_TRANSACTS,
    'text'	=> 'Transactions',
    'summary'	=> 'money transactions'
    ),
  KS_ACTION_FINFER_TRXL		=> array(
    'class'	=> KS_CLASS_FINFER_TRANSACT_LISTS,
    'text'	=> 'Transaction Lists',
    'summary'	=> 'lists of transactions'
    ),
  KS_ACTION_FINFER_TXTY		=> array(
    'class'	=> KS_CLASS_FINFER_TRANSACT_TYPES,
    'text'	=> 'Trx Types',
    'summary'	=> 'transaction types'
    ),
  KS_ACTION_FINFER_OWNER		=> array(
    'class'	=> KS_CLASS_FINFER_OWNERS,
    'text'	=> 'Owners',
    'summary'	=> 'account owners'
    ),
);

/* 2020-03-10 old format
$om = $oRoot->SetNode(new fcMenuFolder('FinanceFerret','FinanceFerret','money management'));

  $om->SetDatabaseSpec(KS_DB_FINFER);

  $omi = $om->SetNode(
    new fcDropinLink(
      KS_ACTION_FINFER_ACCT,
      KS_CLASS_FINFER_ACCOUNTS,
      'Accounts','banking accounts'
      )
    );

  $omi = $om->SetNode(
    new fcDropinLink(
      KS_ACTION_FINFER_TRXT,
      KS_CLASS_FINFER_TRANSACTS,
      'Transactions','money transactions'
      )
    );

  $omi = $om->SetNode(
    new fcDropinLink(
      KS_ACTION_FINFER_TRXL,
      KS_CLASS_FINFER_TRANSACT_LISTS,
      'Transaction Lists','lists of transactions'
      )
    );

  $omi = $om->SetNode(
    new fcDropinLink(
      KS_ACTION_FINFER_TXTY,
      KS_CLASS_FINFER_TRANSACT_TYPES,
      'Trx Types','transaction types'
      )
    );

  $omi = $om->SetNode(
    new fcDropinLink(
      KS_ACTION_FINFER_OWNER,
      KS_CLASS_FINFER_OWNERS,
      'Owners','account owners'
      )
    );
*/

// MODULE SPEC ARRAY

$arDropin = array(
  'name'	=> 'FinanceFerret',
  'descr'	=> 'money management',
  'version'	=> '0.1',
  'date'	=> '2020-03-10',
  'URL'		=> 'https://htyp.org/FinanceFerret',
  'section'	=> array(
    'title'		=> 'FinanceFerret',
    'actions'		=> $arActions,
    ),
  'classes'	=> array(	// list of files and the classes they contain
    'ff-acct.php'	          => KS_CLASS_FINFER_ACCOUNTS,
    'ff-acct-type.php'            => KS_CLASS_FINFER_ACCT_TYPES,
    'ff-txact.php'                => KS_CLASS_FINFER_TRANSACTS,
    'trx-lists/trx-list.php'      => KS_CLASS_FINFER_TRANSACT_LISTS,
    'trx-lists/trx-list-item.php' => KS_CLASS_FINFER_TRANSACT_LIST_ITEMS,
    'ff-txact-type.php'           => KS_CLASS_FINFER_TRANSACT_TYPES,
    'ff-owner.php'                => KS_CLASS_FINFER_OWNERS,
     ),
  );

<?php
/*
  PURPOSE: Form/Control classes for FinanceFerret
    These should probably be replaced by Ferreteria forms...
  HISTORY:
    2015-05-08 split off from FinanceFerret.php
*/
abstract class FormCtrl  {
    protected $strName;
    protected $vVal;
    public $ID;
    public $Format;
    public $phpInCode;	// code for translating user input to data
    public $phpOuCode;	// code for translating data to user-readable output

    public function __construct($iName) {
	$this->strName = $iName;
	$this->ID = NULL;
    }
    public function Val($iData=NULL) {
	if (func_num_args() > 0) {
	    $this->vVal = $iData;
	}
	return $this->vVal;
    }
    protected function GetTextPlain() {
	return $this->vVal;
    }
    protected function GetTextCoded() {
	$in = $this->vVal;
	eval($this->phpOuCode);
	return $ou;
    }
    protected function GetText() {
	if (is_null($this->phpOuCode)) {
	    $out = $this->GetTextPlain();
	} else {
	    $out = $this->GetTextCoded();
	}
	return $out;
    }
    protected function PutText($iText) {
	if (is_null($this->phpInCode)) {
	    $this->vVal = $iText;
	} else {
	    $in = $iText;
	    eval($this->phpInCode);
	    $this->vVal = $ou;
	}
	return $iText;
    }
    public function Text($iData=NULL) {
	if (func_num_args() == 0) {
	    // no input value, so return output:
	    $out = $this->GetText();
	} else {
	    // setting value of variable from input text:
	    $out = $this->PutText($iData);
	}
	if (isset($this->Format)) {
	    return sprintf($this->Format,$out);
	} else {
	    return $out;
	}
    }

    public function ShowView() {
	return $this->Text();
    }
    abstract protected function ShowCtrl($iKeySfx=NULL,$iDoVal);
    public function ShowEdit() {
	return $this->ShowCtrl($this->ID,TRUE);
    }
    public function ShowNew() {
	return $this->ShowCtrl(KS_NEW_REC,FALSE);
    }
}

class FormCtrl_Line extends FormCtrl {
    public $Width;

    protected function ShowCtrl($iKeySfx=NULL,$iDoVal) {
	$out = ' <input type=text';

	if (isset($this->Width)) {
	    $out .= ' size='.$this->Width;
	}

	$out .= ' name="'.$this->strName;
	if (!is_null($iKeySfx)) {
	    $out .= '-'.$iKeySfx;
	}
	$out .= '"';

	if ($iDoVal && isset($this->vVal)) {
	    $out .= ' value="'.htmlspecialchars($this->Text(),ENT_NOQUOTES).'"';
	}

	$out .= '>';
	return $out;
    }
}
class FormCtrl_Money extends FormCtrl_Line {
    public function __construct($iName) {
	parent::__construct($iName);
	$this->Format = '%01.2f';
	$this->Width = 6;
    }
    public function ShowView() {
	if (is_null($this->vVal)) {
	    return NULL;
	} else {
	    return '$'.parent::ShowView();
	}
    }
}
class FormCtrl_Date extends FormCtrl_Line {
    public function __construct($iName) {
	parent::__construct($iName);
	$this->Width = 6;
    }

    protected function GetText() {
	$out = parent::GetText();
	return FmtDate($out);
    }
}
class FormCtrl_CheckBox extends FormCtrl {
    public $strTitle;
    private $arDisplay;

    public function SetDisplay($iTrue,$iFalse=NULL) {
	$this->arDisplay[TRUE] = $iTrue;
	$this->arDisplay[FALSE] = $iFalse;
    }
    public function IsTrue() {
	$out = FALSE;
	if (isset($this->vVal)) {
	    if ($this->vVal) {
		$out .= TRUE;
	    }
	}
	return $out;
    }
    public function ShowView() {
	return $this->arDisplay[$this->IsTrue()];
    }
    protected function ShowCtrl($iKeySfx=NULL,$iDoVal) {
	$out = ' <input type=checkbox';

	if (isset($this->strTitle)) {
	    $out .= ' title="'.$this->strTitle.'"';
	}

	if ($iDoVal && $this->IsTrue()) {
	    $out .= ' checked';
	}

	$out .= ' name="'.$this->strName;
	if (!is_null($iKeySfx)) {
	    $out .= '-'.$iKeySfx;
	}
	$out .= '"';

	$out .= '>';
	return $out;
    }
}
class FormCtrl_DropDown extends FormCtrl {
    public $objList;

    public function FillList($iDataSet,$iValName,$iTextName) {
	$this->objList = new ListChoice();

	// some debugging
	$ok = FALSE;
	if (property_exists ($iDataSet,$iValName)) {
	    if (property_exists ($iDataSet,$iTextName)) {
		$ok = TRUE;
	    }
	}
	if (!$ok) {
	    echo 'Class <b>'.get_class($iDataSet).'</b> is missing either property <b>'.$iValName.'</b> or property <b>'.$iTextName.'</b> (or both).<br>';
	    throw new exception('Missing property!');
	}

	if ($iDataSet->StartRows()) {
	    $this->objList->Clear();
	    while ($iDataSet->NextRow()) {
		$val = $iDataSet->$iValName;
		$text = $iDataSet->$iTextName;
		$this->objList->Add($val,$text);
	    }
	} else {
	    $this->objList = NULL;
	}
    }

    public function ShowCtrl($iKeySfx=NULL,$iDoVal) {
	if ($iDoVal) {
	    $this->objList->Choice = $this->vVal;
	} else {
	    $this->objList->Choice = NULL;
	}
	return $this->objList->ComboBox($this->strName);
    }
    protected function GetTextPlain() {
	if (is_null($this->vVal)) {
	    return NULL;
	} else {
	    return $this->objList->List[$this->vVal];
	}
    }
}
/*==========================*\
- DATA-SPECIFIC FORM CLASSES
\*==========================*/
class FormCtrl_DropDown_Acct extends FormCtrl_DropDown {
    public function Accts($iObj) {
	$this->objAccts = $iObj;
	$this->FillList($iObj->DataSet_DropDown(),'ID','Name');
    }
    protected function GetTextPlain() {
	if (is_null($this->vVal)) {
	    $out = NULL;
	} else {
	    $obj = $this->objAccts->GetItem($this->vVal);
	    $out = $obj->ShowAsHTML(1);
	}
	return $out;
    }
}

class FormCtrl_DropDown_AcctType extends FormCtrl_DropDown {
    public function Types($iObj) {
	$this->objTypes = $iObj;
	$this->FillList($iObj->DataSet_DropDown(),'ID','Name');
    }
}
/*================*\
- UTILITY CLASSES
\*================*/

/*
 CLASS: ListChoice
 PURPOSE: A list of displayable values (each value has associated display-text) plus a chosen value within the list
*/
class ListChoice {
    public $List;
    public $Choice;

    public function Clear() {
	unset($this->List);
    }
    public function Add($iVal,$iText) {
	$this->List[$iVal] = $iText;
    }

    public function ComboBox($iName) {
	$out = '<select name="'.$iName.'">'."\n";
	foreach ($this->List as $val => $text) {
	    $htSelect = "";
	    if ($val == $this->Choice) {
		    $htSelect = " SELECTED";
	    }
	    $out .= '<option'.$htSelect.' value="'.$val.'">'.$text.'</option>'."\n";
	}
	$out .= '</select>'."\n";
	return $out;
    }
}

/*==================*\
- UTILITY FUNCTIONS
\*==================*/
function FmtMoney($iCurr,$iPfx='$') {

    if (is_null($iCurr)) {
	$out = '';
    } else {
	$out = $iPfx.sprintf("%01.2f",$iCurr);
    }
    return $out;
}
function FmtDate($iDate) {
    if (is_string($iDate)) {
      $objDate = new DateTime($iDate);
      $out = $objDate->format('Y-m-d');
    } else {
      $out = '';
    }
  return $out;
}
function CkBox($iName,$iVal) {
    $htCk = $iVal?' checked':'';
    return '<input type=checkbox name="'.$iName.'" '.$htCk.'>';
}
function CkBoxRow($iName,$iVal,$iID) {
    return CkBox($iName.'-'.$iID,$iVal);
}

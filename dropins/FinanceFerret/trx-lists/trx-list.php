<?php namespace greenmine\FinanceFerret;

class ctTrxLists extends \fcTable_keyed_single_standard implements \fiLinkableTable, \fiEventAware, \fiInsertableTable {
    use \ftLinkableTable;
    use \ftExecutableTwig;
    
    // ++ SETUP ++ //

    protected function SingularName() {
	return __NAMESPACE__.'\\crTrxList';
    }
    protected function TableName() {
	return 'Trx_Lists';
    }
    public function GetActionKey() {
	return KS_ACTION_FINFER_TRXL;
    }

    // -- SETUP -- //
    // ++ EVENTS ++ //

    protected function OnCreateElements() { }
    protected function OnRunCalculations() {
	$oPage = \fcApp::Me()->GetPageObject();
	$oPage->SetPageTitle('Transaction Lists'); // Probably never used?
	//$oPage->SetBrowserTitle('(browser)');
	//$oPage->SetContentTitle('(content)');
    }
    public function Render() {
        return 'Feature not implemented, probably not necessary.';
    }

    // -- EVENTS -- //
    // ++ TABLE ++ //
    
    // PUBLIC so Record can use it
    public function GetItemsTable() {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_FINFER_TRANSACT_LIST_ITEMS);
    }
    
    // -- TABLE -- //
    // ++ DATA WRITE ++ //
    
    public function InsertList(\fcDataRecord $rsTrx, $sDescr) {
        // create master record for new list
        $sqlDescr = $this->GetDatabase()->SanitizeValue($sDescr);
        $ar = array(
          'ID_User'       => \fcApp::Me()->GetUserID(),
          'WhenCreated'   => 'NOW()',
          'WhenViewed'    => 'NULL',
          'Title'         => $sqlDescr
        );
        $idList = $this->Insert($ar);
        $idx = 0;
        if ($idList !== FALSE) {
            $tItems = $this->GetItemsTable();
        
            $rsTrx->RewindRows();
	    while ($rsTrx->NextRow()) {
                $idTrx = $rsTrx->GetFieldValue('ID');
                $idx++;
                $tItems->AddItem($idList,$idTrx,$idx);
            }
        }
        return $idList;
    }

    // -- DATA WRITE -- //
}

class crTrxList extends \fcRecord_keyed_single_integer implements \fiLinkableRecord, \fiEventAware, \fiEditableRecord {
    use \ftLinkableRecord;
    use \ftExecutableTwig;
    use \ftSaveableRecord;
    
    // ++ EVENTS ++ //
    
    protected function OnCreateElements() { }
    protected function OnRunCalculations() {
        $sTitle = $this->GetTitle();
    
	$oPage = \fcApp::Me()->GetPageObject();
	$oPage->SetPageTitle($sTitle);
	//$oPage->SetBrowserTitle('(browser)');
	//$oPage->SetContentTitle('(content)');
    }
    public function Render() {
        $tItems = $this->GetTableWrapper()->GetItemsTable();
        $rs = $tItems->GetListTransactions($this->GetKeyValue());
        return $rs->ShowRegister($this->SelfArray());
    }

    // -- EVENTS -- //
    // ++ FIELD VALUES ++ //
    
    protected function GetTitle() {
        return $this->GetFieldValue('Title');
    }
    
    // -- FIELD VALUES -- //
}

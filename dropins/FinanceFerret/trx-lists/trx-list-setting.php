<?php namespace greenmine\FinanceFerret;

class ctTrxListSettings extends \fcTable_wSource_wRecords implements \fiTable_wRecords {
    use \ftName_forTable;
    use \ftWriteableTable;	// Insert()
    // ++ SETUP ++ //
    
    protected function TableName() {
	return 'Trx_List_Settings';
    }
}

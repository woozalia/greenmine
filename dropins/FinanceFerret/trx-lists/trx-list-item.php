<?php namespace greenmine\FinanceFerret;

class ctTrxListItems extends \fcTable_wSource_wRecords implements \fiTable_wRecords {
    use \ftName_forTable;
    use \ftWriteableTable;	// Insert()
    //use \ftRecords_forTable;

    // ++ SETUP ++ //
    
    protected function TableName() {
	return 'Trx_List_Items';
    }
    protected function SingularName() {
	return __NAMESPACE__.'\\crTrxt';
    }

    // -- SETUP -- //
    // ++ TABLES ++ //
    
    protected function GetTransactionsTable() {
	return $this->GetDatabase()->MakeTableWrapper(KS_CLASS_FINFER_TRANSACTS);
    }
    
    // -- TABLES -- //
    // ++ DATA WRITE ++ //
    
    public function AddItem($idList,$idTrx,$nSort) {
        $ar = array(
          'ID_List' => $idList,
          'ID_Trx' => $idTrx,
          'Sort' => $nSort
          );
        $this->Insert($ar);
    }

    // -- DATA WRITE -- //
    // ++ DATA READ ++ //
    
    public function GetListTransactions($idList) {
        $sqlItemsTable = $this->TableName_Cooked();
        $tTrx = $this->GetTransactionsTable();
        $sqlTrxTable = $tTrx->TableName_Cooked();
        $sql = "SELECT t.* from $sqlItemsTable AS i LEFT JOIN $sqlTrxTable AS t ON i.ID_Trx=t.ID WHERE i.ID_List=$idList ORDER BY i.Sort";
        $rs = $this->FetchRecords($sql);
        $rs->SetTableWrapper($tTrx);
        return $rs;
    }
    
    // -- DATA READ -- //
}

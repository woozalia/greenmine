<?php namespace greenmine\FinanceFerret;
/*
  PURPOSE: classes for managing Owners table
  HISTORY:
    2018-05-10 started
*/

class ctOwners extends \fcTable_keyed_single_standard implements \fiLinkableTable {
    use \ftLinkableTable;
    
    // ++ SETUP ++ //

    protected function SingularName() {
	return __NAMESPACE__.'\\crOwner';
    }
    protected function TableName() {
	return 'Owners';
    }
    public function GetActionKey() {
	return KS_ACTION_FINFER_OWNER;
    }
    
    // -- SETUP -- //
    // ++ RECORDS ++ //
    
    public function GetRecords_forDropDown() {
	$rs = $this->SelectRecords();			// 2018-05-06 maybe refine later
	return $rs;
    }

    // -- RECORDS -- //

}
class crOwner extends \fcRecord_keyed_single_integer implements \fiLinkableRecord, \fiEventAware {
    use \ftLinkableRecord;
    use \ftExecutableTwig;

    // ++ EVENTS ++ //

    protected function OnCreateElements() {}
    /*----
      NOTE: Most of this stuff doesn't actually *have* to be here, but the title calculations do, and it just
	made sense to put it all together here.
    */
    protected function OnRunCalculations() {
	$sName = $this->GetNameString();
	$id = $this->GetKeyValue();
    
	$oPage = \fcApp::Me()->GetPageObject();
	$oPage->SetBrowserTitle('own'.$id.' '.$sName);
	$oPage->SetContentTitle("Owner id$id: $sName");
    }
    public function Render() {
	return $this->AdminPage();
    }
    
    // -- EVENTS -- //
    // ++ FIELD VALUES ++ //
    
    protected function GetNameString() {
	return $this->GetFieldValue('Name');
    }

    // -- FIELD VALUES -- //
    // ++ FIELD CALCULATIONS ++ //

    public function ListItem_Link() {
	return $this->SelfLink($this->ListItem_Text());
    }
    public function ListItem_Text() {
	return $this->GetNameString();
    }

    // -- FIELD CALCULATIONS -- //

}
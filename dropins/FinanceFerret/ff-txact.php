<?php namespace greenmine\FinanceFerret;
/*
  PURPOSE: Transaction classes for FinanceFerret
  HISTORY:
    2015-05-08 split off from FinanceFerret.php
    2018-04-02 major updating to work with Ferreteria
*/

/*
  NOTE:
    for setting default start-date to one month ago:
	$odt = new \DateTime();
	$odt->sub(new \DateInterval('P1M'));	// one month ago
	$sDateMinDef = $odt->format('Y-m-d');
*/

class cTrxFilterForm extends \fcDisplayForm {

    // ++ SETUP ++ //

    const KS_NAME_DATE_MIN = 'dtMin';
    const KS_NAME_DATE_MAX = 'dtMax';
    const KS_NAME_ROWS_QTY = 'nRowsQty';
    const KS_NAME_BTN_FILT = 'btnFilt';

    protected function OnSetup() {
        $this->AddControl(new \fcFilterInput_HTML(self::KS_NAME_DATE_MIN));
        $this->AddControl(new \fcFilterInput_HTML(self::KS_NAME_DATE_MAX));
        $this->AddControl(new \fcFilterInput_HTML(self::KS_NAME_ROWS_QTY));
    }

    // -- SETUP -- //
    // ++ INPUT ++ //

    protected function GetControl_MinDate() {
        return $this->GetControl(self::KS_NAME_DATE_MIN);
    }
    public function HasMinDate() {  
        return $this->GetControl_MinDate()->WasSpecified();
    }
    public function GetMinDate_forSQL() {
        return $this->GetControl_MinDate()->Get_forSQL(\fcApp::Me()->GetDatabase());
    }
    
    protected function GetControl_MaxDate() {
        return $this->GetControl(self::KS_NAME_DATE_MAX);
    }
    public function HasMaxDate() {  
        return $this->GetControl_MaxDate()->WasSpecified();
    }
    public function GetMaxDate_forSQL() {
        return $this->GetControl_MaxDate()->Get_forSQL(\fcApp::Me()->GetDatabase());
    }

    protected function GetControl_RowsQty() {
        return $this->GetControl(self::KS_NAME_ROWS_QTY);
    }
    public function HasRowsQty() {  
        return $this->GetControl_RowsQty()->WasSpecified();
    }
    public function GetRowsQty_forSQL() {
        return $this->GetControl_RowsQty()->Get_forSQL(\fcApp::Me()->GetDatabase());
    }

    // -- INPUT -- //
    // ++ OUTPUT ++ //

    // RETURN: single-line description of the current filter values
    public function GetDescription() {
        $out = NULL;
        if ($this->HasRowsQty()) {
            $n = $this->GetControl_RowsQty()->Get_forDisplay();
            $sPlur = \fcString::Pluralize($n);
            $out .= " max $n row$sPlur";
        }
        if ($this->HasMinDate()) {
            $sDate = $this->GetControl_MinDate()->Get_forDisplay();
            if (!is_null($out)) {
                $out .= ' ';
            }
            $out .= " from $sDate";
        }
        if ($this->HasMaxDate()) {
            $sDate = $this->GetControl_MaxDate()->Get_forDisplay();
            if (!is_null($out)) {
                $out .= ' ';
            }
            $out .= " to $sDate";
        }
        return $out;
    }
    /*----
      HISTORY:
	2018-05-07 created
	2019-02-14 adapted for ffcTrxFilterForm
      TODO: radio button for choosing which date field to filter on
    */
    public function Render() {
	$htDateMin = $this->GetControl_MinDate()->Render();
	$htDateMax = $this->GetControl_MaxDate()->Render();
	$htRowsQty = $this->GetControl_RowsQty()->Render();
	$ksButton = self::KS_NAME_BTN_FILT;
	$out = <<<__END__
<form method=get>
<table class=form-block>
<tr><th>Filtering</th></tr>
<tr>
  <td align=right>
    Earliest date: $htDateMin<br>
    Latest date: $htDateMax<br>
    Max # of rows: $htRowsQty
  </td>
</tr>
<tr><td><input type=submit name=$ksButton value="Apply"></td></tr>
</table>
</form>
__END__;

	return $out;
    }

    // -- OUTPUT -- //
}
class ctTrxts extends \fcTable_keyed_single_standard
    implements
      \fiLinkableTable,
      \fiEventAware,
      \fiInsertableTable
      {
    use \ftLinkableTable;
    use \ftExecutableTwig;

    // ++ SETUP ++ //
    
    protected function SingularName() {
	return __NAMESPACE__.'\\crTrxt';
    }
    protected function TableName() {
	return 'Transactions';
    }
    public function GetActionKey() {
	return KS_ACTION_FINFER_TRXT;
    }

    // -- SETUP -- //
    // ++ EVENTS ++ //
  
    protected function OnCreateElements() { }
    protected function OnRunCalculations() {
	$oPage = \fcApp::Me()->GetPageObject();
	$oPage->SetPageTitle('Transactions');
	//$oPage->SetBrowserTitle('(browser)');
	//$oPage->SetContentTitle('(content)');
    }
    public function Render() {
	return $this->AdminPage();	// 2018-04-02 to be written -- probably a search function
    }

    // -- EVENTS -- //
    // ++ TABLES ++ //
    
    protected function GetAccountTable() {
        return $this->GetDatabase()->MakeTableWrapper(KS_CLASS_FINFER_ACCOUNTS);
    }
    protected function GetTrxListTable() {
	return $this->GetDatabase()->MakeTableWrapper(KS_CLASS_FINFER_TRANSACT_LISTS);
    }

    // -- TABLES -- //
    // ++ RECORDS ++ //
    
    protected function GetSQL_forIDs_forAccount($idAcct) {
	$sqlSort = 'NOT Accounted,DateEffective IS NOT NULL,DateEffective DESC,SortInst DESC,DateAction DESC,SortEntry DESC';
	$sqlFilt = 'ID_Acct='.$idAcct;
	$oForm = $this->GetFilterForm();
	if ($oForm->HasMinDate()) {
	    $sqlDate = $oForm->GetMinDate_forSQL();
	    $sqlFilt .= " AND (IFNULL(DateAction,DateEffective) >= $sqlDate)";
	}
	if ($oForm->HasMaxDate()) {
	    $sqlDate = $oForm->GetMaxDate_forSQL();
	    $sqlFilt .= " AND (IFNULL(DateAction,DateEffective) <= $sqlDate)";
	}
	$sqlOther = NULL;
	if ($oForm->HasRowsQty()) {
            $sqlOther = ' LIMIT '.$oForm->GetRowsQty_forSQL();
	}
	$sql = $this->FigureSelectSQL($sqlFilt,$sqlSort,$sqlOther,'ID');
	return $sql;
    }
    /* 2019-02-10 I *think* we don't need this anymore
    protected function SelectRecords_forAccount($idAcct) {
        $sql = $this->GetSQL_forAccount($idAcct);
	$rs = $this->SelectRecords($sqlFilt,$sqlSort,$sqlOther);
	$rs->SetAccountID($idAcct);  // so "new transaction" line knows what Account to use
	return $rs;
    }*/
    // PUBLIC so Account object can call it
    public function FigureStats_forAccount($idAcct) {
	$sqlName = $this->TableName_Cooked();
	$sql = <<<__END__
SELECT
  COUNT(ID) AS NumRows,
  SUM(NOT Voided) AS NumActive,
  MIN(IFNULL(DateAction,DateEffective)) AS DateFirst,
  MAX(IFNULL(DateAction,DateEffective)) AS DateFinal
FROM $sqlName
WHERE ID_Acct=$idAcct
GROUP BY ID_Acct
__END__;
	$rs = $this->FetchRecords($sql);
	$rs->NextRow();	// move to first (only) row
	return $rs->GetFieldValues();
    }
    
    // -- RECORDS -- //
    // ++ FIELD CALCULATIONS ++ //
    
    protected function GetFilterDescription_forAcct($idAcct,$nRows) {
        $oForm = $this->GetFilterForm();
        $sFilter = $oForm->GetDescription();
        $sPlur = \fcString::Pluralize($nRows);
        $sAcctName = $this->GetAccountTable()->GetAccountName($idAcct);
        $sAcct = "$sAcctName ($idAcct)";
        $out = $nRows." transaction$sPlur for $sAcct $sFilter";
        return $out;
    }
    
    // -- FIELD CALCULATIONS -- //
    // ++ WEB UI: INPUT ++ //

    const KS_NAME_ACCT_ID = 'idAcct';
    const KS_NAME_BTN_SAVE = 'btnSave';
  
    private $idTrx = NULL;
    // PUBLIC so Record can access it
    public function SetTrxactFocus($idTrx) {
        $this->idTrx = $idTrx;
    }
    // PUBLIC so Record can access it
    public function GetTrxactFocusStatus() {
        $hasTF = !is_null($this->idTrx);
        $oRes = new cffFieldStatus($hasTF);
        if ($hasTF) {
            $oRes->value = $this->idTrx;
        }
        return $oRes;
    }
    
    // RETURNS: TRUE if the user has submitted new filter parameters
    protected function IsFilterRequested() {
    	$oFormIn = \fcHTTP::Request();
    	return $oFormIn->GetBool(cTrxFilterForm::KS_NAME_BTN_FILT);
    }
    protected function IsSaveRequested() {
    	$oFormIn = \fcHTTP::Request();
    	return $oFormIn->GetBool(self::KS_NAME_BTN_SAVE);
    }

    // -- WEB UI: INPUT -- //
    // ++ WEB UI: FORMS ++ //

    private $oFilterForm = NULL;
    protected function GetFilterForm() {
        if (is_null($this->oFilterForm)) {
            $this->oFilterForm = new cTrxFilterForm();
        }
        return $this->oFilterForm;
    }
    public function GetInputLinkArray() {
        return $this->GetFilterForm()->GetInputLinkArray();
    }

    // PUBLIC so Account record can call it
    public function ShowRegister_forAccount($idAcct) {
        $oForm = $this->GetFilterForm();
        if ($this->IsFilterRequested()) {
            // new filter applied, so fetch new transaction list:
            $oForm->ReadFromForm(); // get filtering parameters from the entry form
            $sql = $this->GetSQL_forIDs_forAccount($idAcct);
            $rs = $this->FetchRecords($sql);
            $nRows = $rs->RowCount();
            
            $t = $this->GetTrxListTable();
            $idList = $t->InsertList($rs,$this->GetFilterDescription_forAcct($idAcct,$nRows));
            $rcList = $t->GetRecord_forKey($idList);
            $rcList->SelfRedirect();
            die();
        } else {
            $oForm->ReadFromPath();
        }
	$out = $oForm->Render();
        /* WORKING HERE
        $this->CheckForSave();
	$out .= $rs->ShowRegister(array(self::KS_NAME_ACCT_ID=>$idAcct));
	*/
	return $out;
    }
    
    protected function CheckForSave() {
        if ($this->IsSaveRequested()) {
            $oFormIn = \fcHTTP::Request();
    //        echo 'FORM INPUT:'.$oFormIn->DumpHTML();
            $arForm = $oFormIn->GetSubArray(KS_ACTION_FINFER_TRXT);
            // anything found here is form data to be saved
            foreach ($arForm as $idTrx=>$arFields) {
                $rc = $this->SpawnRecordset();
                $rc->SaveFormInput($idTrx, $arFields);
            }
        }
    }
    
    // -- WEB UI: FORMS -- //
}
class cffFieldStatus {
    public $isFound;
    public $value;
    
    public function __construct($isFound,$value=NULL) {
        $this->isFound = $isFound;
        $this->value = $value;
    }
}
class cffRecordStatus {
    public $id;
    public $isNull;
    public $isFound;
    public $record;

    public function __construct($id) {
        $this->id = $id;
        $this->isNull = is_null($id);
    }
}
/*::::
  HISTORY:
    2018-05-06 I considered trying to use ftShowableRecord, but given that the rows are actually in a double-row,
      it's probably easier (at least short-term) to not try to automate.
*/
class crTrxt extends \fcRecord_keyed_single_integer
    implements
      \fiLinkableRecord,
      \fiEventAware,
      \fiEditableRecord,
      \fiInsertableTable
      {
    use \ftLinkableRecord;
    use \ftExecutableTwig;
    use \ftSaveableRecord;

    // ++ EVENTS ++ //

    protected function OnCreateElements() {}
    /*----
      NOTE: Most of this stuff doesn't actually *have* to be here, but the title calculations do, and it just
	made sense to put it all together here.
    */
    protected function OnRunCalculations() {
	$sSumm = $this->GetSummaryString();
	$id = $this->GetKeyValue();
    
	$oPage = \fcApp::Me()->GetPageObject();
	$oPage->SetBrowserTitle('tx'.$id.' '.$sSumm);
	$oPage->SetContentTitle("Transaction id$id: $sSumm");
    }
    public function Render() {
	return $this->AdminPage();
    }
    
    // -- EVENTS -- //
    // ++ INPUT ++ //
    
    private $arLink = array();  // additional context for self-links
    protected function SetLocalContext(array $ar) {
        $this->arLink = $ar;
    }
    protected function GetLocalContext() : array {
        return $this->arLink;
    }
    protected function GetFullContext() : array {
        $arTable = $this->GetTableWrapper()->GetInputLinkArray();
        $arLocal = $this->GetLocalContext();
        return array_merge($arTable,$arLocal);
    }
    protected function GetAccountContextStatus() {
        $isFnd = array_key_exists(ctTrxts::KS_NAME_ACCT_ID,$this->arLink);
        $oRes = new cffFieldStatus($isFnd);
        if ($isFnd) {
            $oRes->value = $this->arLink[ctTrxts::KS_NAME_ACCT_ID];
        }
        return $oRes;
    }
    // ACTION: if account ID is set explicitly in the URL, add it to the link array
    protected function ReadAccountContext() {
        $oPathIn = \fcApp::Me()->GetKioskObject()->GetInputObject();
	$sDateMaxDef = NULL; // 2019-02-04 I think eventually this should be a per-account option
    	$idAcct = $oPathIn->GetString(ctTrxts::KS_NAME_ACCT_ID);
        $this->arLink[ctTrxts::KS_NAME_ACCT_ID] = $idAcct;
    }

    // -- INPUT -- //
    // ++ INTERNAL STATES ++ //

    private $isOddRow=FALSE;
    protected function ToggleOdd() {
	$this->isOddRow = !$this->isOddRow;
	return $this->isOddRow;
    }

    // -- INTERNAL STATES -- //
    // ++ FIELD VALUES ++ //

    public function SetAccountID($id) {
        $this->SetFieldValue('ID_Acct',$id);
    }
    protected function GetAccountID() {
        return $this->GetFieldValue('ID_Acct');
    }
    protected function TypeID() {
	return $this->GetFieldValue('ID_Type');
    }
    protected function doReuse() {
	return $this->GetFieldValue('doReuse');
    }
    protected function isVoided() {
	return $this->GetFieldValue('Voided');
    }
    protected function Amount() {
	return $this->GetFieldValue('Amount');
    }
    protected function GetBalanceCalculated() {
	return $this->GetFieldValue('CalcBalance');
    }
    protected function GetBalanceCalculatedNz($def='') {
        if ($this->FieldIsSet('CalcBalance')) {
            return $this->GetFieldValue('CalcBalance');
        } else {
            return $def;
        }
    }
    protected function BalanceCalculated($crBal=NULL) {
	throw new \exception('2018-05-06 Call GetBalanceCalculated() instead.');
	return round($this->Value('CalcBalance',$crBal),2);
    }
    protected function GetBalanceStated() {
	return $this->GetFieldValue('BalanceStated');
    }
    protected function useBalanceEffective() {
	return $this->GetFieldValue('UseBalanceEffective');
    }

    // -- FIELD VALUES -- //
    // ++ FIELD LOOKUPS ++ //
    
    // RETURNS: name string for source account
    protected function GetSourceName($sDefault = '<i>none</i>',$sNotFound = '<i>id %n missing!</i>') {
        $oRes = $this->GetSourceStatus();
        if ($oRes->isFound) {
            $id = $oRes->value;
            $rcAcct = $this->AccountTable($id);
            if ($rcAcct->HasRows()) {
                $sAcct = $rcAcct->GetNameString();
            } else {
                $sAcct = sprintf($sNotFound,$id);
            }
        } else {
            $sAcct = $sDefault;
        }
        return $sAcct;
    }
    // RETURNS: name string for target account
    protected function GetTargetName($sDefault = '<i>not set</i>',$sNotFound = '<i>id %n missing!</i>') {
        $oRes = $this->GetTargetStatus();
        if ($oRes->isFound) {
            $id = $oRes->value;
            $rcAcct = $this->AccountTable($id);
            if ($rcAcct->HasRows()) {
                $sAcct = $rcAcct->GetNameString();
            } else {
                $sAcct = sprintf($sNotFound,$id);
            }
        } else {
            $sAcct = $sDefault;
        }
        return $sAcct;
    }
    
    // -- FIELD LOOKUPS -- //
    // ++ FIELD CALCULATIONS ++ //

    protected function GetSourceStatus() {
        $idAcct = $this->GetFieldValue('ID_Acct');
        $oRes = new cffFieldStatus(!is_null($idAcct),$idAcct);
        return $oRes;
    }
    protected function GetTargetStatus() {
        $idAcct = $this->GetFieldValue('ID_Dest');
        $oRes = new cffFieldStatus(!is_null($idAcct),$idAcct);
        return $oRes;
    }
    /*----
      HISTORY:
        2019-01-22 Apparently this used to exist, but I couldn't find it; rewrote best-guess first draft.
    */
    protected function GetSummaryString() {
        $sSrce = $this->GetSourceName();
        $sTarg = $this->GetTargetName();
        $out =
          $this->Amount()
          .' '
          .$this->TypeID()
          .' '
          ."$sSrce &rarr; $sTarg"
          ;
        return $out;
    }
    protected function GetBalanceEffective() {
	if ($this->useBalanceEffective()) {
	    $crBalCalc = $this->GetBalanceStated();
	} else {
	    $crBalCalc = $this->GetBalanceCalculatedNz(0) + $this->GetBalanceChange();
	}
	return round($crBalCalc,2);
    }
    /*----
    ACTION: Returns the amount by which the current transaction affects the balance
    */
    public function GetBalanceChange() {
	if ($this->IsVoided()) {
	    $ret = 0;
	} else {
            $oTxTypeStatus = $this->TxTypeRecordStatus();
	    if ($oTxTypeStatus->isFound) {
                $rcType = $oTxTypeStatus->record;
		$ret = $rcType->FigureBalanceChange($this->Amount());
	    } else {
		$ret = 0;
	    }
	}
	return $ret;
    }

    // -- FIELD CALCULATIONS -- //
    // ++ TABLES ++ //
    
    protected function AccountTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_FINFER_ACCOUNTS,$id);
    }
    protected function TransactionTypeTable($id=NULL) {
	return $this->GetConnection()->MakeTableWrapper(KS_CLASS_FINFER_TRANSACT_TYPES,$id);
    }
    
    // -- TABLES -- //
    // ++ RECORDS ++ //

    /*----
      RETURNS: recordset of all Accounts, for drop-down lists
    */
    protected function AccountRecords_forDropDown() {
	return $this->AccountTable()->GetRecords_forDropDown();
    }
    /*----
      RETURNS: recordset of all Transaction Types, for drop-down lists
    */
    protected function TypeRecords_forDropDown() {
	return $this->TransactionTypeTable()->GetRecords_forDropDown();
    }
    /*
      RETURNS: a record-status object for the current transaction type
    */
    public function TxTypeRecordStatus() : cffRecordStatus {
	$idType = $this->TypeID();
	$oRes = new cffRecordStatus($idType);
	if ($oRes->isNull) {
	    $oRes->isFound = FALSE;
	} else {
	    $oRes->record = $this->TransactionTypeTable($oRes->id);
	    $oRes->isFound = $oRes->record->HasRows();
	}
	return $oRes;
    }

    // -- RECORDS -- //
    // ++ WEB UI: FORMS ++ //

    // PUBLIC so Table object can call it
    public function SaveFormInput($idTrx, array $arFields) {
        $this->SetKeyValue($idTrx);
        $this->ChangeFieldValues($arFields);
        $this->LineForm()->Save();
    }
    
    // NOTE: This kind of cheats by creating *another* recordset, when in a Register context
    public function AdminPage() {
        $this->ReadAccountContext();
        $oRes = $this->GetAccountContextStatus();
        if ($oRes->isFound) {
            $idAcct = $oRes->value;
            $t = $this->GetTableWrapper();
            $t->SetTrxactFocus($this->GetKeyValue());
            $out = $t->ShowRegister_forAccount($idAcct);
        } else {
            $out = "WARNING: single-transaction display has not yet been implemented.";
        }
        return $out;
    }
    
    private $frmLine=NULL;
    protected function LineForm() {
	if (is_null($this->frmLine)) {
	    $oForm = new \fcFormSavable($this);
              $oField = new \fcFormField_Text($oForm,'ID_Acct');
		$oCtrl = new \fcFormControl_HTML_Hidden($oField,array());
		
	      $oField = new \fcFormField_Text($oForm,'CheckNum');
		$oCtrl = new \fcFormControl_HTML($oField,array('size'=>2));
		
	      $oField = new \fcFormField_Text($oForm,'Descr');
		$oCtrl = new \fcFormControl_HTML_TextArea($oField,array('rows'=>1,'cols'=>20));

	      $oField = new \fcFormField_Text($oForm,'SortEntry');
		$oCtrl = new \fcFormControl_HTML($oField,array('size'=>8,'headers'=>'entSort'));

	      $oField = new \fcFormField_Text($oForm,'SortInst');
		$oCtrl = new \fcFormControl_HTML($oField,array('size'=>1,'style'=>'width: 25px'));

	      // TODO: need a Control or Field type for money
	      $oField = new \fcFormField_Num($oForm,'Amount');
		$oCtrl = new \fcFormControl_HTML($oField,array('size'=>3));

	      $oField = new \fcFormField_Time($oForm,'DateAction');
		$oCtrl = new \fcFormControl_HTML_Timestamp($oField,array('size'=>6,'type'=>'date'));
		  $oCtrl->Format('Y-m-d');

	      $oField = new \fcFormField_Time($oForm,'DateEffective');
		$oCtrl = new \fcFormControl_HTML_Timestamp($oField,array('size'=>6,'type'=>'date','headers'=>'dtEff'));
		  $oCtrl->Format('Y-m-d');

	      // TODO: this should be a date field
	      $oField = new \fcFormField_Bit($oForm,'Voided');
		$oCtrl = new \fcFormControl_HTML_CheckBox($oField,array('title'=>'void?'));
		$oCtrl->DisplayStrings('<b>V</b>','-');

	      $oField = new \fcFormField_Bit($oForm,'doReuse');
		$oCtrl = new \fcFormControl_HTML_CheckBox($oField,array('title'=>'recycle?'));
		$oCtrl->DisplayStrings('<b>R</b>','-');

	      $oField = new \fcFormField_Bit($oForm,'UseBalanceEffective');
		$oCtrl = new \fcFormControl_HTML_CheckBox($oField,array('title'=>'use effective balance?'));
		$oCtrl->DisplayStrings('<b>E</b>','-');

	      $oField = new \fcFormField_Num($oForm,'BalanceImmed');
		$oCtrl = new \fcFormControl_HTML($oField,array('size'=>8));

	      $oField = new \fcFormField_Num($oForm,'BalanceStated');
		$oCtrl = new \fcFormControl_HTML($oField,array('size'=>8));

	      $oField = new \fcFormField_Num($oForm,'ID_Dest');
		$oCtrl = new \fcFormControl_HTML_DropDown($oField,array('headers'=>'toFrom'));
		$oCtrl->SetRecords($this->AccountRecords_forDropDown());

	      $oField = new \fcFormField_Text($oForm,'ID_Type'); // ID_Type uses a short code-string as an ID
		$oCtrl = new \fcFormControl_HTML_DropDown($oField,array());
		$oCtrl->SetRecords($this->TypeRecords_forDropDown());

	    $this->frmLine = $oForm;
	}
	return $this->frmLine;
    }
    // INPUT: $htNew = HTML for the "enter new row" control
    static public function ShowHeader($htNew) {
//	$htStyle = 'font-size: 8pt; background: #ccffcc;';

	$htStyle = 'background: #464;';
	$out = <<<__END__
<tr style="$htStyle">
  <th title="ID # / voided? / recycle?">ID/V/R</th>
  <th>ck#</th>
  <th>type</th>
  <th>amount</th>
  <th>action date</th>
  <th>immed. bal</th>
  <th>stated bal</th>
  <th>E!</th>
  <th>balance</th>
</tr>
<tr style="$htStyle">
  <th>$htNew</th>
  <th id=entSort>entry sort</th>
  <th id=toFrom colspan=2>to/from</th>
  <th id=dtEff title="date effective">eff date</th>
  <th id=order>order</th>
  <th id=notes title="notes" colspan=3>notes</th>
</tr>
__END__;
	return $out;
    }
    private $tpLine=NULL;
    protected function LineTemplate() {
	if (is_null($this->tpLine)) {
	    $sTplt = <<<__END__
<tr style="{{!cssStyle}}">
  <td align=right>
    <small>{{ID}}{{ID_Acct}}</small>
    {{Voided}}
    {{doReuse}}
    </td>
  <td>{{CheckNum}}</td>
  <td>{{ID_Type}}</td>
  <td align=right>{{Amount}}</td>
  <td>{{DateAction}}</td>
  <td align=right>{{BalanceImmed}}</td>
  <td align=right>{{BalanceStated}}</td>
  <td>{{UseBalanceEffective}}</td>
  <td align=right>{{CalcBalance}}</td>
</tr>
<tr style="{{!cssStyle}}">
  <td bgcolor=black>{{!btnSave}}</td>
  <td>{{SortEntry}}</td>
  <td colspan=2>{{ID_Dest}}</td>
  <td>{{DateEffective}}</td>
  <td align=right>{{SortInst}}</td>
  <td colspan=3>{{Descr}}</td>
</tr>
__END__;
	    $this->tpLine = new \fcTemplate_array('{{','}}',$sTplt);
	}
	return $this->tpLine;
    }
    /*-----
      ACTION: Show a register-like table consisting of all the currently loaded transaction data
      PUBLIC so Table can call it
      INPUT:
        $arLink: any additional data that should be included in transaction self-links
      NOTE:
        (2019-01-24) I decided this should only process transactions that the current filter would reveal, so as to prevent
          modification without visible consequences. This is probably an unnecessary precaution, but it resolved the question
          of how to do the form-checking.
      HISTORY:
        2019-02-04 adding $arLink argument so transaction links can include necessary context for summoning Register page
    */
    public function ShowRegister(array $arLink) {
    
        $this->SetLocalContext($arLink);
    
        // always show the new-record form
        $oKiosk = \fcApp::Me()->GetKioskObject();
        $oPathIn = $oKiosk->GetInputObject();
        //$wpPage = $oKiosk->GetPagePath();
        $out = 
          //"<form method=post target='$wpPage'>"
          "<form method=post>"
          .'<table id=register class=listing border=1 style="border: thin solid black;">'
          .$this->ShowRegRowTop()
          ;
        $cntTrx = 0;
	if ($this->HasRows()) {
            $oRes = $this->GetTableWrapper()->GetTrxactFocusStatus();
	    while ($this->NextRow()) {
		$cntTrx++;
                if ($oRes->isFound) {
                    $doEdit = ($this->GetKeyValue() == $oRes->value);
                } else {
                    $doEdit = $this->doReuse();
                }
		$out .= $this->ShowRegRow($doEdit);
	    }

	} else {
	    $out .=
	      "<span class=content>No transactions found."
	      ."<br><b>SQL</b>:<br>{$this->sql}</span>"
	      ;
	}
        $out .=
          "\n</table>"
          //.'<p><center><input type=submit name="btn-save" value="Save Changes"></center></p>'
          ."\n</form>"
          .$cntTrx.' transaction'.\fcString::Pluralize($cntTrx).' found.'
          ;
	return $out;
    }
    /*----
      NOTE: For now, we assume this is being called before any rows are displayed, so it will be blank.
        If this changes in the future, it will need to be explicitly cleared.
    */
    protected function ShowRegRowNew() {

	// 2018-05-06 something to replace this may be necessary:
	//$this->InitNew();	// set all fields to new-record values
	$out = $this->ShowRegRow(TRUE);

	return $out;
    }
    /*----
      PURPOSE: Displays what should go at the top of the register
        This curently displays the header and, if appropriate, a blank/new row.
        "appropriate" currently just means "if no existing row has the focus",
        but later it might reflect a user preference about whether tne new-row line
        should be at the top or bottom.
    */
    protected function ShowRegRowTop() {
        $arLink = $this->GetFullContext();
        $htNew = $this->SelfLink('new',NULL,$arLink); // ($sText,$sPopup,array $arAdd,array $arAttr)
        $out = self::ShowHeader($htNew);
        
        $oRes = $this->GetTableWrapper()->GetTrxactFocusStatus();
        if (!$oRes->isFound) {
            $out .= $this->ShowRegRowNew();
        }
        return $out;
    }
    /*----
      ACTION: Show a single row in register format
      USED BY: this class; clsAcct::EnterRegister()
    */
    protected function ShowRegRow($doEdit) {

// formatting
	$isOdd = $this->ToggleOdd();
	$cssStyle = 'border: thin solid black ! important;';
	$cssStyle .= ' '.($isOdd?'background:#ffffff;':'background:#cccccc;');

	// prepare the form

	$frmEdit = $this->LineForm();
	if ($this->IsNew()) {
	    $frmEdit->ClearValues();
	    $crBalCalc = NULL;
	} else {
	    $frmEdit->LoadRecord();
	    if ($this->isVoided()) {
		$cssStyle .= ' color: grey;';
	    }
	    // 2018-05-06 question: maybe calculations below should be different for voided lines?
	    
// balance calculations
	    $crBalSaved = $this->GetBalanceCalculatedNz('<i>n/a</i>');
	    $crBalCalc = $this->GetBalanceEffective();
	    //$this->BalanceCalculated($crBalCalc);	// if balance not updating, uncomment this
	    $isBalChgd = ($crBalSaved != $crBalCalc);
	    if ($isBalChgd) {
		$htBalCalc = '<i>'.\fcMoney::Format_withSymbol($crBalCalc).'</i> ('.($crBalSaved).')';
	    } else {
		$htBalCalc = \fcMoney::Format_withSymbol($crBalCalc);
	    }
	    //$this->crCalcBal = $crBalCalc;
	}
	$oTplt = $this->LineTemplate();
	if ($doEdit) {
            $htBtnName = ctTrxts::KS_NAME_BTN_SAVE;
            $sBtnSave = 
              "<input type=submit name='$htBtnName' value='Save'>"
              ;
        } else {
            $sBtnSave = NULL;
        }
	$arCtrls = $frmEdit->RenderControls($doEdit);
	  // custom vars
	  $arLink = $this->GetFullContext();
	  $arCtrls['ID'] = $this->SelfLink(NULL,NULL,$arLink); // ($sText,$sPopup,array $arAdd,array $arAttr)
	  $arCtrls['CalcBalance'] = $crBalCalc;
	  $arCtrls['!cssStyle'] = $cssStyle;
	  $arCtrls['!btnSave'] = $sBtnSave;

	// render the form
	$oTplt->SetVariableValues($arCtrls);
	$out = $oTplt->Render();

	return $out;
    }

    // -- WEB UI: FORMS -- //

}

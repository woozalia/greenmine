<?php namespace greenmine;
/*
  FILE: greennmine/app.php
  PURPOSE: Greenmine application framework classes
  HISTORY:
    2013-11-13 Extracted clsVbzApp from vbz-page.php
    2016-06-13 Adapting for Greenmine
    2017-01-24 rewriting/updating for Ferreteria revisions (wasn't working before anyway)
    2017-04-17 Removing gtAdminObject because the only content was SystemEventsClass() and I'm removing that too.
      We're going to the App object to retrieve the event log now.
    2020-03-08 some tidying and updating
*/

class cMenuKiosk extends \fcMenuKiosk_admin {
    public function GetBasePath() : string { return cGlobals::GetWebPath_forAppBase(); }
}

/*::::
  PURPOSE: main application framework class for Greenmine
*/
class cApp extends \fcAppStandardAdmin {
    private $oPage;
    private $oData;

    // ++ SETUP ++ //

    public function Go() {
        $this->SetStartTime();	// get the starting time, for benchmarking
        parent::Go();
    }

    // -- SETUP -- //
    // ++ CLASSES ++ //

    protected function GetPageClass() : string { return 'gcGreenminePage'; }
    protected function GetKioskClass() : string { return __NAMESPACE__.'\\cMenuKiosk'; }

    // -- CLASSES -- //
    // ++ OBJECTS ++ //

    private $db = NULL;
    // CEMENT
    public function GetDatabase() : \ferreteria\data\cDatabase {
        if (is_null($this->db)) {
            $this->db = \ferreteria\data\cDatabase::Instantiate(cGlobals::Me()->GetGreenmineDBSpec(), FALSE);
        }
        return $this->db;
    }

    // -- OBJECTS -- //
    
}

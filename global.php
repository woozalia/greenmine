<?php namespace greenmine;
/*
  PURPOSE: moving to global class as a way of handling configuration
  HISTORY:
    2020-10-23 created
*/
abstract class cGlobalsBase extends \fcGlobals {
    abstract public function GetGreenmineDBSpec() : \ferreteria\data\cDatabaseSpec;
    abstract public function GetGreenMoneyDBSpec() : \ferreteria\data\cDatabaseSpec;
}

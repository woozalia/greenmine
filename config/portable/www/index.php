<?php declare(strict_types=1); namespace greenmine;
/*
 PROJECT: Greenmine
 PURPOSE: main page -- handles all requests
 HISTORY:
  2020-11-08 significant rearranging of file organization
*/

//* 2017-02-03 Not needed here; php.ini modified, and not sure what 2nd argument should be.
$fErrLevel = E_ALL | E_STRICT;
error_reporting($fErrLevel);
if (!ini_get('display_errors')) {
    ini_set('display_errors', 'TRUE');
}
//*/

// CONSTANTS
// -- KF = boolean flag
// -- KS = string
// -- KWP = web path (URL including protocol)
// -- KFP = file path
// -- KRP = relative path

$kfpHome = dirname($_SERVER['SCRIPT_FILENAME'],2);
/*
require($kfpHome.'/site/config/greenmine/portable/defaults.php');	// Greenmine default config values
require($kfpHome.'/site/config/greenmine/local/globals.php');       // Greenmine local config values
*/
require($kfpHome.'/site/git/greenmine/start.php');	// Greenmine start-file
new cGlobalsLocal($kfpHome);

$oApp = new cApp();
$oApp->SetBasePath('/');    // TODO: rename this SetWebBasePath()
$oApp->Go();
#$kfpConfigGreenmine = $kfpAcctRoot.'/site/config/greenmine';

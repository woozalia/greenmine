<?php namespace greenmine;
/*
  PURPOSE: Greenmine site default global settings
  REQUIRES:
    $kfpAcctRoot - define this in index.php
  USAGE: If you use exactly the same folder-layout as the canonical model, this file can be used as is
    (In which case, you can make upgrading easier by linking to it instead of copying it.)
  FOLDER LAYOUT: see https://wooz.dev/Greenmine/folders
  HISTORY:
    2017-01-24 created
    2018-02-28 modifying for cloud5
    2020-03-08 removing defines now defined in ferreteria/const.php
    2020-03-09 getting locations straightened out; more complete mapping of folder-structure in comments above
    2020-11-08 more restructuring
*/
#require($kfpConfigGreenmine.'/local/globals.php');	// includes db credentials
#$kfpSitePrivate = $kfpAcctRoot.'/site'; // non-web files area

//$kfpSite = dirname(__DIR__);  // 2020-03-09 I don't think this works now...
#$kfpConfigFerreteria = $kfpSitePrivate.'/config/ferreteria';

//require('/var/www/lib/php/server.php');
#require($kfpSite.'/git/ferreteria/ClassLoader.php');

#require($kfpSitePrivate.'/git/ferreteria/start.php');
#require($kfpSitePrivate.'/git/greenmine/start.php');

#require($kfpSite.'/git/greenmine/@libs.php');

//new fcCodeLibrary('ferreteria',$fsFerreteria);
//new fcCodeLibrary('greenmine',$fsGreenmine);
//fcCodeLibrary::Load_byName('ferreteria');
//fcGlobals::SetFilePath_forLibraries($fpLib);

#date_default_timezone_set('America/New_York');	// KLUGE - TODO: make this a user config option

abstract class cGlobals extends cGlobalsBase {

    public function __construct(string $fpHome) {
        $this->SetHomePath($fpHome);
        parent::__construct();
    }

    // various strings
    
    public function GetAppKeyString() : string { return 'gmine'; }
    abstract public function GetText_SiteName() : string;
    abstract public function GetText_SiteName_short() : string;
    abstract public function GetText_EmailDomain() : string;
    
    // file paths
    
    private $fpHome;
    protected function SetHomePath(string $fp) { $this->fpHome = $fp; }
    protected function GetHomePath() : string { return $this->fpHome; }
    
    // base path to all private files
    protected function GetPrivatePath() : string { return $this->GetHomePath().'/site'; }
    
    // base path to all configuration files
    protected function GetConfigPath() : string { return $this->GetPrivatePath().'/config'; }
    
    // web paths
    
    // -- folders
    
    protected function GetWebPath_forAppBase() : string { return '/'; }
    protected function GetWebPath_forStaticFiles() : string { return $this->GetWebPath_forAppBase().'static/'; }
      protected function GetWebPath_forIcons() : string { return $this->GetWebPath_forStaticFiles().'icons/'; }
      public function GetWebPath_forStyleSheets() : string { return $this->GetWebPath_forStaticFiles().'css/'; }
    
    // -- files
    
    public function GetWebSpec_forSuccessIcon() : string { return $this->GetWebPath_forIcons().'button-check-20px.png'; }
    public function GetWebSpec_forWarningIcon() : string { return $this->GetWebPath_forIcons().'button-red-X.20px.png'; }
    // TODO: need a yellow warning icon
    public function GetWebSpec_forErrorIcon() : string { return $this->GetWebPath_forIcons().'button-red-X.20px.png'; }
    
    // data stuff
    
    private $oTableNames = NULL;
    public function TableNames() : cTableNames {
        if (is_null($this->oTableNames)) {
            $sClass = $this->GetTableNamesClass();
            $this->oTableNames = new $sClass();
        }
        return $this->oTableNames;
    }
    protected function GetTableNamesClass() : string { return __NAMESPACE__.'\\cTableNames'; }
    public function GroupID_forUsers_exists() : int { return TRUE; }
    public function GroupID_forUsers() : int { return 1; }
}
class cTableNames {
    use \ftSingletonAuto;

    public function UserAccount() : string { return 'user_account'; }
    public function UserGroup() : string { return 'user_group'; }
    public function UserPermit() : string { return 'user_permit'; }
    public function UserAccountXGroup() : string { return 'uacct_x_ugroup'; }
    public function UserGroupXPermit() : string { return 'ugroup_x_upermit'; }
    
}

// 2020-11-10 Deprecating global constants, and gradually converting them to global-object values.

#define('KS_SITE_SHORT','Greenmine');
#define('KS_SITE_NAME','Hypertwins Greenmine');
#define('KS_APP_KEY','gmine');
#define('KS_EMAIL_DOMAIN',		'hypertwins.net');
#define('KS_TPLT_EMAIL_ADDR_ADMIN',	KS_SITE_NAME.' admin system <admin-{{tag}}@'.KS_EMAIL_DOMAIN.'>');

define('KWP_APP_BASE','/');	// no links should point above/outside this area, and pathinfo is relative to it
#define('KFN_DROPIN_INDEX','index.dropin.php');		// filename for drop-in index file

define('KWP_STYLE_SHEETS','/static/css/');		// web folder where CSS files are kept
define('KWP_ICONS','/static/icons');
/* 2017-03-28 configuration works differently now...
define('KWP_ICON_ALERT'		,KWP_ICONS.'/button-red-X.20px.png');
define('KWP_ICON_WARN'		,KWP_ICONS.'/button-red-X.20px.png');	// TODO: separate warning icon
define('KWP_ICON_OKAY'		,KWP_ICONS.'/button-check-20px.png');
*/

// file locations
define('FP_TEMPORARY_STORAGE','/home/htnet/tmp');
define('FP_FILE_REPOSITORY','/home/htnet/site/var/files');
define('FN_INVOICE_TEMPLATE','invoice-template.odt');

// user permissions
// uncomment this when setting up user permissions for the first time
define('ID_USER_ROOT',1);	// user with this ID has all permissions
define('ID_GROUP_USERS',1);	// group to which all users automatically belong (not sure if includes anonymous)
define('KB_USE_ANONYMOUS_ROOT',FALSE);	// set to TRUE if nonexistent permissions are preventing login
define('KS_PERM_FE_ROOT','*');	// "must be root" - not counted as a "needed" permission

// -- permission names (values can be whatever you want; they just need to match the values in the permits table)
define('KS_PERM_RAW_DATA_EDIT','fe.data.admin');		// can edit fields that might wreck data integrity
define('KS_PERM_EVENTS_VIEW','fe.event.view');		// can use event data viewing tools
define('KS_PERM_SITE_VIEW_CONFIG','fe.site.view.config');	// can view site configuration details
define('KS_PERM_USER_CONN_DATA','fe.user.conn.view');		// can view user connection data (browser ID, IP address)
#define('KS_PERM_SEC_USER_VIEW','fe.user.acct.view');		// can view all user accounts
define('KS_USER_SEC_PERM_VIEW','fe.user.perm.view');		// can view available permits
define('KS_USER_SEC_GROUP_VIEW','fe.user.group.view');		// can view user groups

// dropin features
define('KS_FEATURE_USER_SECURITY','user.security');
define('KS_FEATURE_USER_ACCOUNT_ADMIN','user.admin.acct');
define('KS_FEATURE_USER_SECURITY_ADMIN','user.security.admin');
define('KS_FEATURE_USER_SESSION_ADMIN','user.admin.sess');

// aesthetic preferences
#define('KS_CHAR_URL_ASSIGN',':');	// pathArgs intra-item separator
	// ^ this can be changed - but don't use '-' or '.', because some names use those
#define('KS_CHAR_PATH_SEP','/');		// pathArgs inter-item separator
#define('KS_NEW_REC','new');		// pathArgs string to use for new entries/records
#define('KS_PATHARG_NAME_TABLE','page');
#define('KS_PATHARG_NAME_INDEX','id');
// -- markup we will be using for templates
#define('KS_TPLT_OPEN','{{');
#define('KS_TPLT_SHUT','}}');

// technical stuff

/* 2020-03-08 commenting these out as well
#define('KS_CHARACTER_ENCODING','ISO-8859-15');		// This seems to be necessary for either latin1 or UTF8.
//define('KS_CHARACTER_ENCODING','UTF-8');		// This seems to makes text disappear.
define('KS_USER_SESSION_KEY',KS_APP_KEY.'-session');	// name of cookie for storing session key
// TABLE NAMES
// -- ferreteria
define('KS_TABLE_USER_ACCOUNT',		'user_account');
define('KS_TABLE_USER_GROUP',		'user_group');
define('KS_TABLE_USER_PERMISSION',	'user_permit');
define('KS_TABLE_UACCT_X_UGROUP',	'uacct_x_ugroup');
define('KS_TABLE_UGROUP_X_UPERM',	'ugroup_x_upermit');

// blocks of text (eventually we'll come up with a better way to do this)
define('KS_TEXT_EMAIL_SUBJ_FOR_NEW_ACCOUNT',KS_SITE_NAME.' password reset authorization');
define('KS_TPLT_EMAIL_TEXT_FOR_NEW_ACCOUNT',<<<__END__
Someone (hopefully you) has requested authorization to create a new account on {{site}}.

If you would like to do this, please go to this link:

	  {{url}}

You will be able to enter your account information there.
__END__
);
define('KS_TEXT_EMAIL_SUBJ_FOR_PASS_CHANGE',KS_SITE_NAME.' password reset authorization');
define('KS_TPLT_EMAIL_TEXT_FOR_PASS_CHANGE',<<<__END__
Someone (hopefully you) has made a request to change the password on {{site}}
for user "{{user}}" at this email address ({{addr}}).

If you would like to do this, please go to this link:

	  {{url}}
	  
You will be able to enter your new password there.
__END__
);
define('KS_TPLT_AUTH_EMAIL_TO_SHOW',<<<__END__
A link has been emailed to you at <b>{{addr}}</b>.<br>
Clicking the link will {{action}}.
__END__
);

*/

<?php
/*
  PURPOSE: access credentials for Greenmine site - EXAMPLE FILE
  HISTORY:
    2017-01-24 created
    2017-02-04 updated for cloud4
    2018-02-28 updated for cloud5
    2020-03-09 converted to example
*/
$ksDBserver	= "localhost";

$ksDBuser	= "greenminer-dev";
$ksDBpass	= 'your-password-here';	// TODO: need to make it possible for pw to contain "@", ":", or "/"
$ksDBschema	= 'greenmine-db';
define('KS_DB_GREENMINE','mysql:'.$ksDBuser.':'.$ksDBpass.'@'.$ksDBserver.'/'.$ksDBschema);

$ksDBuser	= "greenmoney";
$ksDBpass	= 'your-password-here';	// TODO: need to make it possible for pw to contain "@", ":", or "/"
$ksDBschema	= 'financeferret-db';
define('KS_DB_FINFER','mysql:'.$ksDBuser.':'.$ksDBpass.'@'.$ksDBserver.'/'.$ksDBschema);

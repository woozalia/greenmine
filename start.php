<?php namespace ferreteria\classloader;
/*
  REQUIRES:
    $kfpHome - base directory for everything specific to this site
  HISTORY:
    2020-03-08 Created -- trying to make Greenmine more easily deployable.
  TODO: This isn't consistent with what's in VbzCat's start.php
*/
$kfpConfigFerreteria = $kfpHome.'/site/config/ferreteria';
require($kfpHome.'/site/git/ferreteria/start.php');	// Ferreteria start-file

cLoader::SetBasePath(__DIR__);
new cLibraryExec('greenmine','@libs.php');  // register the greenmine library
cLoader::LoadLibrary('greenmine');          // load its class list
cLoader::LoadLibrary('ferreteria.data');
cLoader::LoadLibrary('ferreteria.login');
cLoader::LoadLibrary('ferreteria.node');
cLoader::LoadLibrary('ferreteria.forms');

/* 2020-11-08 old code
require($kfpConfigGreenmine.'/portable/defaults.php');  // default global values
require($kfpConfigGreenmine.'/local/const.php');	     // local global values
*/
require($kfpHome.'/site/config/greenmine/portable/defaults.php');	// Greenmine default config values
require($kfpHome.'/site/config/greenmine/local/globals.php');       // Greenmine local config values

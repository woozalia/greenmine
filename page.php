<?php
/*
  PURPOSE: page type for Greenmine (probably won't have any others)
  HISTORY:
    2016-06-13 started
    2017-01-24 gutted; rewriting
*/

class gcGreenminePage extends fcPage_login {

    // ++ EVENTS ++ //

    // CEMENT
    protected function OnRunCalculations() {
        // 2017-01-25 These may create nodes -- shouldn't they be in OnCreateElements()?
        $this->UseStyleSheet('basic');
        $sSite = greenmine\cGlobals::Me()->GetText_SiteName_short();
        $this->SetPageTitle($sSite.' Control Panel');	// default page title

        $oKiosk = fcApp::Me()->GetKioskObject();
        $oReq = $oKiosk->GetInputObject();
    }
    // -- EVENTS -- //
    // ++ CLASSES ++ //

    protected function Class_forTagHTML() : string { return 'gcTag_html'; }

    // -- CLASSES -- //
    // ++ ELEMENTS ++ //

    public function GetElement_PageHeader() { return $this->GetTagNode_body()->GetElement_PageHeader(); }

    // -- ELEMENTS -- //

}
class gcTag_html extends fcTag_html_standard {

    // ++ CEMENTING ++ //

    protected function Class_forTag_body() : string { return 'gcTag_body'; }

    // -- CEMENTING -- //
}
class gcTag_body extends fcTag_body_login {
    use ftLoginContainer;

    // ++ EVENTS ++ //
    
    // OVERRIDE: Navbar needs to render before header and content.
    protected function OnCreateElements() {
    	$this->GetElement_PageNavigation();
        $this->GetElement_PageHeader();
        $this->GetElement_LoginWidget();
        $this->GetElement_PageContent();
    }
    protected function OnRunCalculations() {}
  
    // -- EVENTS -- //
    // ++ CLASSES ++ //

    // CEMENT
    protected function Class_forPageHeader() : string { return 'gcPageHeader'; }
    // CEMENT
    protected function Class_forPageNavigation() : string { return 'gcNavbar'; }
    // CEMENT
    protected function Class_forPageContent() : string { return 'gcPageContent'; }
    // OVERRIDE
    protected function Class_forLoginWidget() : string { return 'fcpeLoginWidget_block'; }
    
    // -- CLASSES -- //

}
class gcPageHeader extends fcContentHeader_login {}
class gcNavbar extends fcMenuFolder {

    // ++ CEMENTING ++ //
    
    protected function OnCreateElements() {
        $fpDropins = greenmine\cGlobals::Me()->GetFilePath_forDropins();
        fcDropInManager::ScanDropins($fpDropins,$this);	// add all the dropins as subnodes
    }
    protected function RenderSelf() { return NULL; }
    protected function RenderNodesBlock() {
        return "\n<ul class=dropin-menu>"
          .$this->RenderNodes()
          ."\n</ul>"
          ;
    }

    // -- CEMENTING -- //
    
}
class gcPageContent extends fcPageContent {
    use ftContentMessages;

    // ++ EVENTS ++ //
    
    // ACTION: add any elements that can be defined at construction time
    protected function OnCreateElements() {}	// Nothing to do here; <body> creates header and navbar
    protected function OnRunCalculations() {}	// Nothing to do here; Page object does some calcs, menu executes requests

    // -- EVENTS -- //
    // ++ FRAMEWORK ++ //

    protected function GetDatabase() { return fcApp::Me()->GetDatabase(); }
    
    // -- FRAMEWORK -- //
    // ++ NEW PAGE ELEMENTS ++ //
    
}
class gcpeMessage_error extends fcpeSimple {
    public function Render() : string {
        $s = $this->GetValue();
        return '<center>'
          .'<table class="error-message">'
          .'<tr><td valign=middle><img src="'.KWP_ICON_ALERT.'" alt="icon: error" /></td>'
          ."<td valign=middle>$s</td>"
          .'</tr></table>'
          .'</center>'
          ;
    }
}
class gcpeMessage_warning extends fcpeSimple {
    public function Render() : string {
        $s = $this->GetValue();
        return '<center>'
          .'<table class="warning-message">'
          .'<tr><td valign=middle><img src="'.KWP_ICON_WARN.'" alt="icon: warning" /></td>'
          ."<td valign=middle>$s</td>"
          .'</tr></table>'
          .'</center>'
          ;
    }
}
class gcpeMessage_success extends fcpeSimple {
    public function Render() {
	$s = $this->GetValue();
	return '<center>'
	  .'<table style="border: 1px solid green; background: #eeffee;">'
	  .'<tr><td valign=middle><img src="'.KWP_ICON_OKAY.'" alt="icon: okay" /></td>'
	  ."<td valign=middle>$s</td>"
	  .'</tr></table>'
	  .'</center>';
    }
}
